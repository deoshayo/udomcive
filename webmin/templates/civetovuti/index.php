<?php
// no direct access
if (!defined('WEB_ROOT')) {
	exit;
}
?>
<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>CIVE  INTRANET</title>

    <!-- Bootstrap Core CSS -->
    <link href="<?php echo WEB_ROOT;?>templates/civetovuti/bower_components/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">
	
    <link href="<?php echo WEB_ROOT;?>templates/civetovuti/bootstrap/css/style.css" rel="stylesheet">

    <link href="<?php echo WEB_ROOT;?>templates/civetovuti/bower_components/datatables/media/css/jquery.dataTables.min.css" rel="stylesheet">

    <link href="<?php echo WEB_ROOT;?>templates/civetovuti/bower_components/datatables/media/css/jquery.dataTables.css" rel="stylesheet">

    
    <!-- MetisMenu CSS -->
    <link href="<?php echo WEB_ROOT;?>templates/civetovuti/bower_components/metisMenu/dist/metisMenu.min.css" rel="stylesheet">

    <!-- Custom CSS -->
    <link href="<?php echo WEB_ROOT;?>templates/civetovuti/dist/css/sb-admin-2.css" rel="stylesheet">

    <!-- Custom Fonts -->
    <link href="<?php echo WEB_ROOT;?>templates/civetovuti/bower_components/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->

    	<!-- UN USED LIBRARY
        <script language="JavaScript" type="text/javascript" src="<?php echo WEB_ROOT; ?>templates/civetovuti/js/menu.js"></script>
	<script type="text/javascript" src="<?php echo WEB_ROOT;?>templates/civetovuti/js/jquery-1.2.2.pack.js"></script>
	<script type="text/javascript" src="<?php echo WEB_ROOT;?>templates/civetovuti/js/ddaccordion.js"></script>
        
        -->
        

        
        
          <!-- CSS WIZARD -->
   
        <link href="<?php echo WEB_ROOT;?>templates/civetovuti/bootstrap/css/bootstrap.min.css" rel="stylesheet">
	<link href="<?php echo WEB_ROOT;?>templates/civetovuti/bootstrap/css/prettify.css" rel="stylesheet">
        
</head>

<body>

    <div id="wrapper">

        <!-- Navigation -->
        <nav class="navbar navbar-default navbar-static-top" role="navigation" style="margin-bottom: 0">
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand" href="index.html">CIVE  INTRANET</a>
            </div>
            <!-- /.navbar-header -->

            <ul class="nav navbar-top-links navbar-right">
                <li class="dropdown">
                    <a class="dropdown-toggle" data-toggle="dropdown" href="#">
                        <i class="fa fa-user fa-fw"></i>  <i class="fa fa-caret-down"></i>
                    </a>
                    <ul class="dropdown-menu dropdown-user">
                        
			<?php
                        
			$self2="index.php?q=user_account&fetch=fetch";
			?>
			
			<li><a href="<?php echo $self2; ?>">
                        <i class="fa fa-gear fa-fw"></i> Change Password</a>
                        </li>
			<li class="divider"></li>
                        <li><a href="index.php?q=logout">
                        <i class="fa fa-sign-out fa-fw"></i> Logout</a>
                        </li>
                        
                    </ul>
                    <!-- /.dropdown-user -->
                </li>
                <!-- /.dropdown -->
            </ul>
            <!-- /.navbar-top-links -->

            <div class="navbar-default sidebar" role="navigation">
                <div class="sidebar-nav navbar-collapse">
                    <ul class="nav" id="side-menu">
                                <?php
				//Calling menus
				
				left_menu();
				
				
				?>
                    </ul>
                </div>
                <!-- /.sidebar-collapse -->
            </div>
            <!-- /.navbar-static-side -->
        </nav>

        <!-- Page Content -->
        <div id="page-wrapper">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-lg-12">
                         <h1 class="page-header"><?php echo $label;?>
                         <!--
                         <img   id="loader" src="<? echo WEB_ROOT;?>img/ajax-loader.gif" border="0"/>
                         -->
                         </h1>
                
                
                    
                    <div class="panel-body">
                        <?php
                            include_once $content;
                        ?>
                    </div>
                    </div>
                    <!-- /.col-lg-12 -->
                </div>
                <!-- /.row -->
            </div>
            <!-- /.container-fluid -->
        </div>
        <!-- /#page-wrapper -->

    </div>
    <!-- /#wrapper -->

    <!-- jQuery -->
    <script src="<?php echo WEB_ROOT;?>library/civescripts.js"></script>
    <script src="<?php echo WEB_ROOT;?>library/stickynote.js"></script>
	
	<!-- jQuery -->
    <script src="<?php echo WEB_ROOT;?>templates/civetovuti/bower_components/jquery/dist/jquery.min.js"></script>

    <!-- Bootstrap Core JavaScript -->
    <script src="<?php echo WEB_ROOT;?>templates/civetovuti/bower_components/bootstrap/dist/js/bootstrap.min.js"></script>

    <!-- Metis Menu Plugin JavaScript -->
    <script src="<?php echo WEB_ROOT;?>templates/civetovuti/bower_components/metisMenu/dist/metisMenu.min.js"></script>

    <!-- Custom Theme JavaScript -->
    <script src="<?php echo WEB_ROOT;?>templates/civetovuti/dist/js/sb-admin-2.js"></script>
    
    <script src="<?php echo WEB_ROOT;?>templates/civetovuti/bower_components/datatables/media/js/jquery.dataTables.min.js"></script>

    <script src="<?php echo WEB_ROOT;?>templates/civetovuti/bower_components/datatables/media/js/jquery.dataTables.js"></script>

    <script>
		$(document).ready(function() {
			$('#dataTables-example').dataTable();
			scrollY: 400;
		});
	
	<?php
	for($i=1; $i<9; $i++){
	?>
		
		$(document).ready(function() {
			$('#dataTables-example<?php echo $i; ?>').dataTable();
			scrollY: 400;
		});
		
	<?php
	}
	?>
	</script>
	</script>
            <script>
          $(document).ready(function() {
  	$('#rootwizard').bootstrapWizard();
});
       
       </script>
            
            

	<script src="<?php echo WEB_ROOT;?>templates/civetovuti/bootstrap/js/jquery.bootstrap.wizard.js"></script>
	<script src="<?php echo WEB_ROOT;?>templates/civetovuti/bootstrap/js/prettify.js"></script>
	<script>
	$(document).ready(function() {
	  	$('#rootwizard').bootstrapWizard({onNext: function(tab, navigation, index) {
				if(index==2) {
					// Make sure we entered the name
					if(!$('#name').val()) {
						alert('You must enter your name');
						$('#name').focus();
						return false;
					}
				}

				// Set the name for the next tab
				$('#tab3').html('Hello, ' + $('#name').val());

			}, onTabShow: function(tab, navigation, index) {
				var $total = navigation.find('li').length;
				var $current = index+1;
				var $percent = ($current/$total) * 100;
				$('#rootwizard .progress-bar').css({width:$percent+'%'});
			}});
		window.prettyPrint && prettyPrint()
	});
	</script>
            
          
<script src="<?php echo WEB_ROOT;?>templates/civetovuti/bower_components/tinymce/tinymce.min.js"></script>
<script src="<?php echo WEB_ROOT;?>templates/civetovuti/bower_components/tinymce/jquery.tinymce.min.js"></script>
<script>tinymce.init({selector:'textarea'});</script>


      
</body>

</html>
