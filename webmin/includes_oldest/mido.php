 <?php
$user_id=$_SESSION['cive_user_id'];
require("almanac/config.php");
require("almanac/lang/lang." . LANGUAGE_CODE . ".php");
require("almanac/functions.php");

javaScript();
?>
	<link rel="stylesheet" type="text/css" href="almanac/css/default.css">
          
<div>
<?php




# testing whether var set necessary to suppress notices when E_NOTICES on
$month = 
	(isset($_GET['month'])) ? (int) $_GET['month'] : null;
$year =
	(isset($_GET['year'])) ? (int) $_GET['year'] : null;

# set month and year to present if month 
# and year not received from query string
$m = (!$month) ? date("n") : $month;
$y = (!$year)  ? date("Y") : $year;

$scrollarrows = scrollArrows($m, $y);
$auth 		  = auth();

//require("./templates/" . TEMPLATE_NAME . ".php");
?>

<table cellpadding="0" cellspacing="0" border="0" align="center">
<tr>
	<td>
		<?php echo $scrollarrows ?>
		<span class="date_header">
		&nbsp;<?php echo $lang['months'][$m-1] ?>&nbsp;<?php echo $y ?></span>
	</td>

	<!-- form tags must be outside of <td> tags -->
	<form name="monthYear">
	<td align="right">
	<?php monthPullDown($m, $lang['months']); yearPullDown($y); ?>
	<input type="button" value="GO" onClick="submitMonthYear()">
	</td>
	</form>

</tr>

<tr>
	<td colspan="2" bgcolor="#000000">
	<?php echo writeCalendar($m, $y); ?></td>
</tr>

<tr>
	<td colspan="2" align="center">
	<?php echo footprint($auth, $m, $y) ?></td>
</tr>
</table>




</div>
<?php
backup_ifo();
?>

