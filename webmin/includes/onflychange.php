<?php
	
/**
 *	Onfly change
 *	Author: Steven Edward 
 *	Date: 20-04-2015	
 *	
**/

//load configurations for win
require_once ('../library/config.php');

//load global functions
require_once ('../library/functions.php');

//get target file
$target = $_GET['target']; 

switch ($target) {
	case 'loadDepartmentStaff':
	    loadDepartmentStaff();
		break;
	
	case 'viewAvailablePrivilege':
	    viewAvailablePrivilege();
		break;
		
	case 'assignPrivelege':
	    assignPrivelege();
		break;
		
	case 'registerNewGroup':
	    registerNewGroup();
		break;
		
	case 'courseSelection':
	    courseSelection();
		break;
}

//onfly function definition
function loadDepartmentStaff(){
	$dep      = $_GET['dep'];
	$schoolID = $_GET['id'];
	if($schoolID != ''){
		$ct=mysql_query("SELECT * FROM tbl_staff WHERE DepartmentID='$dep'") or die (mysql_error());
		$num=mysql_num_rows($ct);
		echo "<select name='StaffID' id='StaffID'><option>--Select Staff--</option>";
		for($c=1;$c<=$num;$c++)
		{
		$ctr=mysql_fetch_array($ct);
		echo "<option value='$ctr[staffId]'>$ctr[firstName] $ctr[lastName]</option>";
		}
		echo "</select>";
	}else{
	?>
	<select name='StaffID'>
		<option value=''>:Select School First:</option>
	</select>
	<?php 
	}
}
	
function viewAvailablePrivilege(){
	
	$userId = $_GET['id'];
			
	if($userId != '') {
	$w=0;
	$privilegeArray = array();
	$exist = '';
	$resultPrivilege = dbQuery("SELECT ModuleID FROM tbl_privilege WHERE priorityId = $userId");
	if( dbNumRows($resultPrivilege) > 0){
		$od = 0;
		while($rows = dbFetchAssoc($resultPrivilege)){
		$privilegeArray[$od] = $rows['ModuleID'];
		$od++;
		}
	} else $privilegeArray = array();
	
	$result = dbQuery("SELECT * FROM  tbl_module")or die(mysql_error());
	
	?>
	<table border=0 class="table table-striped table-bordered table-hover" id="dataTables-example" cellspacing='0'>
	<tr height="10" class="head">
		<th width="5" class='content'>S/N</th>
		<th  class='content'>Module Description</th>
		<th class='content'>Select <input type="hidden" name="userId" id="userId" value="<?php echo $userId;?>"/></th>
	</tr>
	<?php
	$k = 0;
	while($r = dbFetchAssoc($result))
	{
	$prId = $r['ID'];
	$resultPrivilege = dbQuery("SELECT Position FROM tbl_privilege WHERE priorityId = $userId AND ModuleID = $prId ");
	if(dbNumRows($resultPrivilege) > 0) $exist = 'true'; else $exist = '';
	if($k%2==0)
	{
		$bg="row1";
	}else{
		$bg="row2";
	}
	 $checkName    = "check_" . $k;
	 $checkNameDiv = "divCheck_" . $k;
	 $checkHidden  = "action_" . $k;
	
	?>
	<tr height="10"  class = "<?php echo $bg; ?>" >
		<td width="10%" align="left"><?php echo($w+1);?></td>
		<td width="50%" align="left"><?php echo $r['ModuleDescription']; ?></td>
		<?php
		if($exist == 'true'){
		?>
		<td  align='left'>
			<input type='checkbox' name="<?php echo $checkName; ?>" value="<?php echo $prId; ?>" checked='checked' 
			onClick="assignPrivilege(document.frmUserPrivilege,'assignPrivelege','<?php echo $checkNameDiv; ?>','<?php echo $prId; ?>','<?php echo $checkHidden; ?>');"><input type='hidden' name="<?php echo $checkHidden;?>" id="<?php echo $checkHidden;?>" value=0 />
		&nbsp;&nbsp;<span id="<?php echo $checkNameDiv; ?>">&nbsp;</span>
		</td>
		<?php
		} else { ?>
		<td  align='left'><input type='checkbox' name="<?php echo $checkName; ?>" value="<?php echo $prId; ?>" onClick="assignPrivilege(document.frmUserPrivilege,'assignPrivelege','<?php echo $checkNameDiv; ?>','<?php echo $prId; ?>','<?php echo $checkHidden; ?>');" >
		<input type='hidden' name="<?php echo $checkHidden;?>" id="<?php echo $checkHidden;?>" value=1 />
		&nbsp;&nbsp;<span id="<?php echo $checkNameDiv; ?>">&nbsp;</span>
		</td>
		<?php 
		}
		?>
	</tr>
	<?php
	$w++;
	$k++;
	}
	?>
	</table>
	<p>&nbsp;</p>
	<?php
	}else{
		echo "Select the username you want to assign his/her privilege";
	}

}	

function assignPrivelege()
{
	$groupId = $_GET['groupId'];
	$modId   = $_GET['modId'];
	$action  = $_GET['action'];
	if($action == 0)
		$result = dbQuery("DELETE FROM tbl_privilege WHERE priorityId = '$groupId' AND moduleID='$modId'");
	else{
		//avoid duplicate entry
		//$resultTest = dbQuery("SELECT position FROM tbl_privilege WHERE priorityId = '$groupId' AND moduleID='$modId'");
		//if(dbNumRows($resultTest)==0){
		$result = dbQuery("INSERT INTO tbl_privilege (priorityId,moduleID,position) VALUES('$groupId','$modId','Normal')");
		//}
	}
}
//course selection
function courseSelection()
{
	$courseId     = $_GET['courseId'];
	$academic     = $_GET['academic'];
	$staffId      = $_GET['staffId'];
	$isChief      = $_GET['isChief'];
	$action       = $_GET['action'];
	$checkNameDiv = $_GET['checkDiv'];
	$checkName    = $_GET['checkName'];
	$checkHidden  = $_GET['checkHidden'];
	
	if($action == 0){
		$result = dbQuery("DELETE FROM tbl_course_selection WHERE courseID = '$courseId' AND status='$isChief' AND academicID='$academic'");
		$instructor = isCourseSelected($courseId,$academic,$isChief);
	}else{
		//check if the instructor can choose a course
		if(checkIfCanChooseACourse($staffId,$academic,$isChief)){
			$result = dbQuery("INSERT INTO tbl_course_selection (courseID,academicID,staffID,status) VALUES('$courseId','$academic','$staffId','$isChief')");
			$instructor = isCourseSelected($courseId,$academic,$isChief);
		}else{
			$instructor = "<span style='color:red'>Overloaded</span>";
		}
	}
	
	if(is_array($instructor)){
	?>
	<span id="<?php echo $checkNameDiv; ?>">
		<input type='checkbox' name="<?php echo $checkName; ?>" id="<?php echo $checkName; ?>" value="0" checked='checked'
		onClick="courseSelection(document.frmSelection,'courseSelection','<?php echo $checkNameDiv; ?>','<?php echo $courseId; ?>','<?php echo $academic ?>','<?php echo $_SESSION['cive_user_id']?>','1','<?php echo $checkHidden; ?>','<?php echo $checkName; ?>');">
		<input type='hidden' name="<?php echo $checkHidden;?>" id="<?php echo $checkHidden;?>" value=0  />
		&nbsp;<?php echo $instructor[1]; ?>
	</span>
	<?php
		//echo $instructor[1];
	}else{
		//echo $instructor;
		?>
		<span id="<?php echo $checkNameDiv; ?>">
			<input type='checkbox' name="<?php echo $checkName; ?>" id="<?php echo $checkName; ?>" value="1" 
			onClick="courseSelection(document.frmSelection,'courseSelection','<?php echo $checkNameDiv; ?>','<?php echo $courseId; ?>','<?php echo $academic ?>','<?php echo $_SESSION['cive_user_id']?>','1','<?php echo $checkHidden; ?>','<?php echo $checkName; ?>');">
			<input type='hidden' name="<?php echo $checkHidden;?>" id="<?php echo $checkHidden;?>" value=1 />
			&nbsp;<span id="stickynote2"><?php echo $instructor; ?></span>
		</span>
		<?php
	}
}

/*
	Function to register new group
*/

function registerNewGroup()
{
	?>
	<form name='frmApplication' action="index.php?q=user_privilege_rw" method='POST' >
	<table  class='dtable' width='100%' border='0'>
	<tr>
		<td colspan='2' class='content' align='center'><h4>Register New Group</h4></td>
	</tr>
	<tr>
		<td class='content' width='20%' valign='top'>Group name:</td>
		<td class='content'><input type='text' name='group' id='group' size='25'></td>
	</tr>
	<tr><td colspan='2'>&nbsp;</td></tr>
	<tr><td class='content' valign='top'>Description:</td><td class='content'><textarea name='desc' cols=40 rows=8></textarea></td></tr>
	<tr>
		<td colspan=2 align='center'>
			<input type='submit' name='newGroup' value='  Register  ' class='btn btn-info'>&nbsp;&nbsp;&nbsp;&nbsp;
			<input name="btnCancel" type="button" id="btnCancel" value="    Cancel    " onClick="window.location.href='index.php';" class="btn">
		</td>
	</tr>
	</table>
	</form>
	<?php
}
?>