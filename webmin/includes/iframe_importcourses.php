<?php
//Self Reference
require_once '../library/config.php';
require_once '../library/functions.php';

require_once("excel_reader2.php");
//$self="index.php?q=". MD5('importEmployee');
$self="index.php?q=upload_curiculum";
//include 'db.php';

$token = (isset($_GET['token']) && $_GET['token'] != '') ? $_GET['token'] : '';

//messages
if($token == 'okay'){
	//echo "<p class='message'>File records Import Finished Successfully</p>";
	upload_file_sucess();
} else if($token == 'not'){
	//echo "<p class='message'>File records Import Finished UnSuccessfully  $_GET[err] record(s) rejected</p>";
	uploadError("File records Import Finished UnSuccessfully  $_GET[err] record(s) rejected");
} else if ( $token == 'fail'){
	echo "<p class='message'>There is an internal error only $_GET[err] row(s) imported</p>";
}

//import translations details
if(!empty($_FILES['userfile']))
{
	# delete old files
	//@unlink("temp/ResultFile.txt");
	#constants
	$fileavailable=0;
	#validate the file
	$file_name = addslashes(@$_POST['userfile']);
	$overwrite = addslashes(@$_POST['radiobutton']);
	#check file extension
	$file = @$_FILES['userfile']['name'];
	//The original name of the file on the client machine. 
	$filetype = @$_FILES['userfile']['type'];
	//The mime type of the file, if the browser provided this information. An example would be "image/gif". 
	$filesize = @$_FILES['userfile']['size'];
	//The size, in bytes, of the uploaded file. 
	$filetmp = @$_FILES['userfile']['tmp_name'];
	//The temporary filename of the file in which the uploaded file was stored on the server. 
	$filetype_error = @$_FILES['userfile']['error'];
	$filename=time().@$_FILES['userfile']['name'];
	// In PHP earlier then 4.1.0, $HTTP_POST_FILES  should be used instead of $_FILES.
	if (is_uploaded_file($filetmp))
	{
		$filename=time().$file;
		copy($filetmp, "temp/$filename");
		move_uploaded_file($filetmp,"temp/$filename");
		$str = $filename;
		$i = strrpos($str,".");
		if (!$i) 
		{
			return "";
		}
		$l = strlen($str) - $i;
		$ext = substr($str,$i+1,$l);
		$pext = strtolower($ext);
		if ($pext != "xls")
		{
			print "<p class='message'>File Extension Unknown.<br>";
			unlink("temp/$filename");
		} else {
			#unzip the file first
			if ($pext == "xls") 
			{
				$fileavailable=1;
			}
			#STEP 2.0 exceute sql scripts
			if ($fileavailable==1)
			{
				$data = new Spreadsheet_Excel_Reader("temp/$filename");
				$rows = $data->rowcount(0);
				$error=0;
				$sucess=0;
				$title = 0;
				$fcontents = file ("temp/$filename"); 
				# expects the csv file to be in the same dir as this script
				for($i=2; $i<= $rows; $i++) 
				{ 
					
					$Name         = $data->val($i,2);
					$Description  = $data->val($i,3);
					$Unit         = $data->val($i,4);
					$Content      = $data->val($i,5);
					$Description=ucwords($Description);
					if(!empty($Name) and !empty($Description)){
						if ($overwrite==1)
						{
							
							//check if exists
							$sqlcheck = "SELECT * FROM tbl_course WHERE 
										Name='$Name' AND 
										Description='$Description' AND 
										Unit='$Unit' AND 
										Content='$Content'";
							$result = mysql_query($sqlcheck);
							if(mysql_num_rows($result)>0){
								$data2=1;
							}else{
								$sql = "REPLACE INTO tbl_course(Name,Description,Unit,Content)
									VALUES ('$Name','$Description','$Unit','$Content')";
								$data2=mysql_query($sql) or die (mysql_error());
							}
						}
						else
						{
							$sql = "INSERT INTO tbl_course(Name,Description,Unit,Content)
							VALUES ('$Name','$Description','$Unit','$Content')";
							//check if the record exists
							$sqlcheck = "SELECT * FROM tbl_course WHERE 
										Name='$Name' AND 
										Description='$Description' AND 
										Unit='$Unit' AND 
										Content='$Content'";
							$result = mysql_query($sqlcheck);
							if(mysql_num_rows($result)>0){
								$data2=0;
							}else{
								$sql = "INSERT INTO tbl_course(Name,Description,Unit,Content)
										VALUES ('$Name','$Description','$Unit','$Content')";
								$data2=mysql_query($sql) or die (mysql_error());
							}
						}
						
						if($data2) 
						{
							$sucess++;
						}
						else
						{
							$error++;
							$errorArray[] = array(
										'Name'        => $Name,		  
										'Description' => $Description,
										'Comment'	  => 'Course exist'
										);
						}
					}
				}
			}
			
			if($error == 0)
			{
				mafanikio("<p class='message'>Data Import Finished Successfully</p>");
				
			}else if($sucess != 0){
				noma("Data Import Finished UnSuccessfully " . $error. " record(s) rejected. Click <a href='" .errorExcelFileCourse($errorArray). "' target='_blank'> here</a> to view rejected records");
				
			}else
			{
				//echo "<p class='message'>Data Import Finished UnSuccessfully</p>";
				noma("File records Import Finished UnSuccessfully  $error record(s) rejected. Click <a href='" .errorExcelFileCourse($errorArray). "' target='_blank'> here</a> to view rejected records");
			}
	 	}
	}
	else 
	{
		echo "<p class='message'>File not uploaded, see the error: ".$filetype_error."</p>";
	}

}
?>