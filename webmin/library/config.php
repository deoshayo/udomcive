<?php
ini_set('display_errors', 'On');

ini_set('max_execution_time',360);  //Maximum execution time of each script, in seconds (I CHANGED THIS VALUE)
ini_set('max_input_time',120); //Maximum amount of time each script may spend parsing request data
ini_set('max_input_nesting_level',64); //Maximum input variable nesting level
ini_set('memory_limit', '128M');   //Maximum amount of memory a script may consume (128MB by default)
//ob_start("ob_gzhandler");
error_reporting(E_ALL);

// start the session
@session_start();

// database connection config
$dbHost     = 'localhost';
$dbUser     = 'root';
$dbPass     = '';
$dbName     = 'udomcive';
$siteDomain = 'localhost';
$kannelport = 13013;
$kannelUser = 'deo';
$kannelPass = '123456';

// setting up the web root and server root for
$thisFile = str_replace('\\', '/', __FILE__);
$docRoot = $_SERVER['DOCUMENT_ROOT'];

$webRoot  = str_replace(array($docRoot, 'library/config.php'), '', $thisFile);
$srvRoot  = str_replace('library/config.php', '', $thisFile);

define('WEB_ROOT', $webRoot);
define('SRV_ROOT', $srvRoot);
define('DB_HOST', $dbHost);
define('DB_USER', $dbUser);
define('DB_PASS', $dbPass);
define('DB_NAME', $dbName);

//for kannel config
define('CONFIG_KANNEL_HOST', $siteDomain);
define('CONFIG_KANNEL_PORT', $kannelport);
define('CONFIG_KANNEL_USER_NAME', $kannelUser);
define('CONFIG_KANNEL_PASSWORD', $kannelPass);

//session timeout 1800
define('MAXSECONDS', 300000);    //1800maximum session time in seconds

//time b4 pop-up for unread sms
define('TIME_B4_POPUP', 15); //15

//maximum days before the next backup
define('DAYS_BEFORE_NEXT_BACKUP',14); // two weeks

//bgcolor for core course
define('BGCOLORCORE','#e1e3e5'); 

//bgcolor for core course
define('BGCOLORELECTIVE','#eef4f9');

if (!get_magic_quotes_gpc()) {
	if (isset($_POST)) {
		foreach ($_POST as $key => $value) {
			$_POST[$key] =  trim(addslashes($value));
		}
	}

	if (isset($_GET)) {
		foreach ($_GET as $key => $value) {
			$_GET[$key] = trim(addslashes($value));
		}
	}
}

// since all page will require a database access
// and the common library is also used by all
// it's logical to load these library here
require_once 'database.php';
require_once 'common.php';
require_once 'taasisi.php';
$User_id="";

?>
