<?php
require_once 'config.php';

$dbConn = mysql_connect ($dbHost, $dbUser, $dbPass) or die ('MySQL connect failed. ' . mysql_error());
@mysql_select_db($dbName) or die('Cannot select database. ' . mysql_error());

function dbQuery($sql)
{
	$result = @mysql_query($sql) or die(@mysql_error());
	
	return $result;
}

function dbClose($sql)
{
	$result = @mysql_close($sql) or die(@mysql_error());
	
	return $result;
}

function dbAffectedRows()
{
	global $dbConn;
	
	return @mysql_affected_rows($dbConn);
}

function dbFetchArray($result, $resultType = MYSQL_NUM) {
	return @mysql_fetch_array($result, $resultType);
}

function dbFetchAssoc($result)
{
	return @mysql_fetch_assoc($result);
}

function dbFetchRow($result) 
{
	return @mysql_fetch_row($result);
}

function dbFreeResult($result)
{
	return @mysql_free_result($result);
}

function dbNumRows($result)
{
	return @mysql_num_rows($result);
}

function dbSelect($dbName)
{
	return @mysql_select_db($dbName);
}

function dbInsertId()
{
	return @mysql_insert_id();
}

 /*
 // automatic backup
 */
 function automaticbackup()
 {
	$resultautobackup = dbQuery(" SELECT backupDate FROM tbl_database_autobackup ORDER BY backupId DESC");
	$rowautobackup	= dbFetchAssoc($resultautobackup);
	$autochangedate = array();
	$autobackdt     = $rowautobackup['backupDate'];
	$autochangedate = explode('-',$autobackdt);
	$lastbackdate =mktime(0,0,0,$autochangedate[1],$autochangedate[2],$autochangedate[0]);
	$todaydate = date( 'Y-m-d');
	$todaydatearray = explode('-',$todaydate);
	$todaybackdate =mktime(0,0,0,$todaydatearray[1],$todaydatearray[2],$todaydatearray[0]);
	$backupdiffer = $todaybackdate - $lastbackdate;
	$backupdiffer = $backupdiffer/(60*60*24);
	if($backupdiffer > DAYS_BEFORE_NEXT_BACKUP)
	@autobackup_tables($dbHost,$dbUser,$dbPass,$dbName);
 }
?>