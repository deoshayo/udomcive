<?php
require_once 'config.php';

function checkUser()
{
	// if the session id is not set, redirect to login page
	if (!isset($_SESSION['cive_user_id'])) {
		@header('Location: ' . WEB_ROOT . 'login.php');
		exit;
	}
	
	// the user want to logout
	if (isset($_GET['logout'])) {
		doLogout();
		$User_id="";
	}
}

/*
	Check if user id exist in order view popup
*/
function checkUserPopUp()
{
	if (!isset($_SESSION['cive_user_id'])) {
	?>
	<script type="text/javascript">
	window.close();
	</script>
	<?php
	exit;
	}
}

/*
	login function for webmin
*/

function doLogin2()
{
	// if we found an error save the error message in this variable
	$errorMessage = '';
	
	$userName = addslashes(trim($_POST['txtUserName']));
	$password = addslashes(trim($_POST['txtPassword']));
	//$language = $_POST['language'];
	
	//home url
	//$url = 'https://' . CONFIG_KANNEL_HOST . '/ams/index.php';
	$url = 'index.php';
	
	// first, make sure the username & password are not empty
	if ($userName == '' or $password == '') {
		$errorMessage = 'You must enter your username and password';
	} else {
		// check the database and see if the username and password combo do match
		$sql = "SELECT *
		        FROM tbl_staff 
				WHERE Email = '$userName' AND password = MD5('$password')";
		$result = dbQuery($sql);

		if (dbNumRows($result) == 1) {		
			//check from db if ip is blocked 
			$_SESSION['user_ip'] = $_SERVER['REMOTE_ADDR'];
			$_SESSION ['user_login_iteration'] = 1;
			$resultcheckip = dbQuery("SELECT * FROM tbl_blocked_ip WHERE ip = '{$_SESSION['user_ip']}' AND status = 1");
			if(dbNumRows($resultcheckip) > 0){
			$errorMessage = 'HTTP/1.1 403 Forbidden, Your Account has been banned. Contact Your System Administrator';
			return $errorMessage;
			}
			$row = dbFetchAssoc($result);
			
			//check if user have modules
			$activeUser = dbQuery("SELECT * FROM tbl_privilege WHERE priorityId ='{$row['priorityId']}' ");
			if(dbNumRows($activeUser) > 0){
			$_SESSION['cive_user_id']      = $row['staffId'];
			$_SESSION['sis_user_id']      = $row['staffId'];
			$_SESSION['cive_username']     = $row[ 'firstName'];
			$_SESSION['user_category']     = $row[ 'priorityId'];
			$_SESSION['staff_position_id'] = $row[ 'Position'];
			//$_SESSION['sms_language']    = $language;
			$_SESSION['last_use']    = time();
			$_SESSION['login_time']  = time();
			
			define('USER_ID',$_SESSION['cive_user_id']);
			//define('USER_ID',$_SESSION['sis_user_id']);
			
			// log the time when the user last login
			$sql = "UPDATE tbl_staff
			        SET user_last_login = NOW()
					WHERE staffId = '{$row['staffId']}'";
			dbQuery($sql);
			
			// automatic backup after two weeks
			automaticbackup();
			
			$pageNumber = 'login';
			
			//upadate the loghistory
			traceUsers($pageNumber);
			
			// now that the user is verified we move on to the next page
            // if the user had been in the admin pages before we move to
			// the last page visited
				if(isset($_SESSION ['login_return_url'])){
					//check if user have previlege to view the page
					if($_SESSION['last_user_id'] == $_SESSION['ams_user_id'] ){
					header('Location: ' . $_SESSION ['login_return_url']);
					exit;
					} else {
					
					header('Location:' . $url);
					exit;
					}
				}
				@header('Location:' . $url);
				//header('Location: index.php');
				exit;
			} else $errorMessage = 'Your Account is locked contact your Administrator';
			
		} else {
		    // for wrong username or password
			$ip      = $_SERVER['REMOTE_ADDR'];
			if(!isset($_SESSION ['user_ip'])){
				$_SESSION ['user_ip'] = $ip;
				$_SESSION ['user_login_iteration'] = 1; //check number of iterations for user login
				$errorMessage = 'Wrong username or password';
			} else {
				if($_SESSION ['user_login_iteration'] > 10){
					$errorMessage = 'Brute force attack. Please you are not required to access this system again';
					$resultifipexist = dbQuery("SELECT * FROM tbl_blocked_ip WHERE ip = '{$_SESSION['user_ip']}'");
					if(dbNumRows($resultifipexist) > 0){
						dbQuery("UPDATE tbl_blocked_ip SET status = '1', date = NOW() WHERE ip = '{$_SESSION['user_ip']}'");
					} else {
						dbQuery("INSERT INTO tbl_blocked_ip (ip,status) VALUES('$ip','1')");
					}
					
					$_SESSION ['user_login_iteration'] += 1;
				} else {
					$errorMessage = 'Wrong username or password';
					$_SESSION ['user_login_iteration'] += 1;
				}
			}
			
		
			
		}		
			
	}
	
	return $errorMessage;
	//return $sql;
}




function doLogin()
{
	// if we found an error save the error message in this variable
	$errorMessage = '';
	
	$userName = addslashes(trim($_POST['txtUserName']));
	$password = addslashes(trim($_POST['txtPassword']));
	//$language = $_POST['language'];
	
	//home url
	//$url = 'https://' . CONFIG_KANNEL_HOST . '/ams/index.php';
	$url = 'index.php';
	
	// first, make sure the username & password are not empty
	if ($userName == '' or $password == '') {
		$errorMessage = 'You must enter your username and password';
	} else {
		//check the database and see if the username and password combo do match
		$sql = "SELECT staffId, firstName
		        FROM tbl_staff 
				WHERE Email = '$userName' AND password = MD5('$password')";
		$result = dbQuery($sql);
	
		if (dbNumRows($result) == 1) {		
			//check from db if ip is blocked 
			$_SESSION['user_ip'] = $_SERVER['REMOTE_ADDR'];
			$_SESSION ['user_login_iteration'] = 1;
			$resultcheckip = dbQuery("SELECT * FROM tbl_blocked_ip WHERE ip = '{$_SESSION['user_ip']}' AND status = 1");
			if(dbNumRows($resultcheckip) > 0){
				$errorMessage = 'HTTP/1.1 403 Forbidden, Your Account has been banned. Contact Your System Administrator';
				return $errorMessage;
			}
			
			$row = dbFetchAssoc($result);
			
			//check if user have modules
			$activeUser = dbQuery("SELECT Position FROM tbl_privilege WHERE user_id ='{$row['staffId']}' ");
			if(dbNumRows($activeUser) > 0){
			$_SESSION['cive_user_id'] = $row['staffId'];
			$_SESSION['sis_username'] = $row[ 'fname'];
			//$_SESSION['sms_language']    = $language;
			$_SESSION['last_use']    = time();
			$_SESSION['login_time']  = time();
			
			define('USER_ID',$_SESSION['cive_user_id']);
			
			// log the time when the user last login
			$sql = "UPDATE tbl_user 
			        SET user_last_login = NOW()
					WHERE user_id = '{$row['user_id']}'";
			dbQuery($sql);
			
			// automatic backup after two weeks
			automaticbackup();
			
			$pageNumber = 'login';
			
			//upadate the loghistory
			traceUsers($pageNumber);
			
			// now that the user is verified we move on to the next page
            // if the user had been in the admin pages before we move to
			// the last page visited
			if(isset($_SESSION ['login_return_url'])){
			    //check if user have previlege to view the page
				if($_SESSION['last_user_id'] == $_SESSION['ams_user_id'] ){
				header('Location: ' . $_SESSION ['login_return_url']);
				exit;
				} else {
				
				header('Location:' . $url);
				exit;
				}
			}
			@header('Location:' . $url);
			//header('Location: index.php');
			exit;
			} else $errorMessage = 'Your Account is locked contact your Administrator';
			
		} else {
		    // for wrong username or password
			$ip      = $_SERVER['REMOTE_ADDR'];
			if(!isset($_SESSION ['user_ip'])){
				$_SESSION ['user_ip'] = $ip;
				$_SESSION ['user_login_iteration'] = 1; //check number of iterations for user login
				$errorMessage = 'Wrong username or password';
			} else {
				if($_SESSION ['user_login_iteration'] > 10){
					$errorMessage = 'Brute force attack. Please you are not required to access this system again';
					$resultifipexist = dbQuery("SELECT * FROM tbl_blocked_ip WHERE ip = '{$_SESSION['user_ip']}'");
					if(dbNumRows($resultifipexist) > 0){
						dbQuery("UPDATE tbl_blocked_ip SET status = '1', date = NOW() WHERE ip = '{$_SESSION['user_ip']}'");
					} else {
						dbQuery("INSERT INTO tbl_blocked_ip (ip,status) VALUES('$ip','1')");
					}
					
					$_SESSION ['user_login_iteration'] += 1;
				} else {
					$errorMessage = 'Wrong username or password';
					$_SESSION ['user_login_iteration'] += 1;
				}
			}
			
		
			
		}		
			
	}
	
	//return $errorMessage;
	return $sql;
}

/*
	Logout a user
*/
function doLogout()
{
	if (isset($_SESSION['cive_user_id'])) {
		$pageNumber = 'logout';
		traceUsers($pageNumber);
		unset($_SESSION['cive_user_id']);
		unset($_SESSION['sms_category']);
		unset($_SESSION['sis_username']);
		session_cache_limiter('nocache');
		$_SESSION = array();
		session_unset(); 
		session_destroy();
		
	}
	?>	
	<meta http-equiv="refresh" content="0; url=index.php">
	<?php
	exit;
}

/*
Load template
*/

function loadtemplate()
{
    $sql = "SELECT template 
            FROM tbl_template
		    WHERE status = 1";
			
	$result = dbQuery($sql);
	$rowtmp = dbFetchAssoc($result);
	//$rowtmp['template']="civetovuti";
	$temp = 'templates/' . $rowtmp['template'] . '/index.php'; 
	return $temp;

}

/*
check session
*/

 function checksession ()
{
	
	if (isset($_SESSION['cive_user_id']) && isset($_SESSION['last_use']))
	{
	//Destroy if session older than maxSeconds (default 600 seconds)
		if (($_SESSION['last_use']+ MAXSECONDS ) < time())
		{
		//Session has expired: Destroy
		$_SESSION['last_user_id'] = $_SESSION['cive_user_id'];
		
		unset($_SESSION['cive_user_id']);
		unset($_SESSION['user_category']);
		unset($_SESSION['username']);
   
		//Get the current request URI
		$_SESSION ['login_return_url'] = $_SERVER['REQUEST_URI'];
   
		//Reload the current page in case it has been cached
		//Header ("Location: $page");
		header('Location: ' . WEB_ROOT . 'login.php');
		exit;
		}  else
		  {
		   //Session still active and valid.
		   // - Update / Refresh session
		   $_SESSION['last_use'] = time();
		  }
 }
 else
 {
  //Required session variables not set.  Destroy
  session_destroy();
 }
 }
 
 /**/
 function checkPopUp ()
 {
	if(isset($_SESSION['SHOW_SMS_POP_UP'])){
		if (($_SESSION['login_time']+ TIME_B4_POPUP ) < time())
		{
		$_SESSION['sms_alert'] = 1; 
		}
	}
 }
 
 /* backup the db OR just a table */
function backup_tables($host,$user,$pass,$name,$tables = '*')
{
	
	//$link = @mysql_connect($host,$user,$pass);
	//@mysql_select_db($name,$link);
	
	//get all of the tables
	if($tables == '*')
	{
		$tables = array();
		$result = @mysql_query('SHOW TABLES');
		while($row = @mysql_fetch_row($result))
		{
			$tables[] = $row[0];
		}
	}
	else
	{
		$tables = is_array($tables) ? $tables : explode(',',$tables);
	}
	
	//cycle through
	$nowArray = getdate(time());
	$return="
			---------------------------------------
			-- NRC Mysql database Backup 
			-- version 1.0
			
			--
			-- Host: " . $host . " 
			-- Generation Time: " . date( 'd-m-Y') . " at " . $nowArray['hours'] . ":" . $nowArray['minutes'] . " 
			-- Server version: 5.0.51
			-- PHP Version: 5.2.5 
			-- Database: " . $name ."
			---------------------------------------\n\n";
	foreach($tables as $table)
	{
		$result = mysql_query('SELECT * FROM '.$table);
		$num_fields = mysql_num_fields($result);
		
		$return.= 'DROP TABLE IF EXISTS '.$table.';';
		$row2 = mysql_fetch_row(mysql_query('SHOW CREATE TABLE '.$table));
		$return.= "\n\n".$row2[1].";\n\n";
		
		for ($i = 0; $i < $num_fields; $i++) 
		{
			while($row = mysql_fetch_row($result))
			{
				$return.= 'INSERT INTO '.$table.' VALUES(';
				for($j=0; $j<$num_fields; $j++) 
				{
					$row[$j] = addslashes($row[$j]);
					$row[$j] = ereg_replace("\n","\\n",$row[$j]);
					if (isset($row[$j])) { $return.= '"'.$row[$j].'"' ; } else { $return.= '""'; }
					if ($j<($num_fields-1)) { $return.= ','; }
				}
				$return.= ");\n";
			}
		}
		$return.="\n\n\n";
	}
	
	//save file
	$bkdbname = 'db-backup-'.time().'-'.(md5(implode(',',$tables))).'.sql';
	$handle = fopen('./backups/' . $bkdbname ,'w+');
	 
	if(fwrite($handle,$return)){
	dbQuery("INSERT INTO tbl_database_backup (backupDate,dbName) value(NOW(),'$bkdbname')");
	fclose($handle);
	?>	
		<meta http-equiv="refresh" content="0; url=index.php?q=<?php echo MD5('backup')?>&msg=<?php echo MD5('okay')?>">
	<?php
	} else {
	echo "Error";
	fclose($handle);
	}
	
}


 
 function user()
 {
 echo"<select name='user_id'><option value=''>::Select User::</option>";
 $user = dbQuery("select * from  tbl_user");
 while($name =dbFetchAssoc($user))
 {
 $user_id = $name['user_id'];
 $username = $name['username'];
 echo"<option value='$user_id'>$username</option>";
 }
 echo"</select>";
 }
 

 
 /*
 // automatic backup fuction
 */
 function autobackup_tables($host,$user,$pass,$name,$tables = '*')
{
	
	//$link = mysql_connect($host,$user,$pass);
	//mysql_select_db($name,$link);
	
	//get all of the tables
	if($tables == '*')
	{
		$tables = array();
		$result = @mysql_query('SHOW TABLES');
		while($row = @mysql_fetch_row($result))
		{
			$tables[] = $row[0];
		}
	}
	else
	{
		$tables = is_array($tables) ? $tables : explode(',',$tables);
	}
	
	//cycle through
	$nowArray = getdate(time());
	$return="
			---------------------------------------
			-- NRC  Mysql database Backup 
			-- version 1.0
			
			--
			-- Host: " . $host . " 
			-- Generation Time: " . date( 'd-m-Y') . " at " . $nowArray['hours'] . ":" . $nowArray['minutes'] . " 
			-- Server version: 5.0.51
			-- PHP Version: 5.2.5 
			-- Database: " . $name ."
			---------------------------------------\n\n";
	foreach($tables as $table)
	{
		$result = mysql_query('SELECT * FROM '.$table);
		$num_fields = mysql_num_fields($result);
		
		$return.= 'DROP TABLE IF EXISTS '.$table.';';
		$row2 = mysql_fetch_row(mysql_query('SHOW CREATE TABLE '.$table));
		$return.= "\n\n".$row2[1].";\n\n";
		
		for ($i = 0; $i < $num_fields; $i++) 
		{
			while($row = mysql_fetch_row($result))
			{
				$return.= 'INSERT INTO '.$table.' VALUES(';
				for($j=0; $j<$num_fields; $j++) 
				{
					$row[$j] = addslashes($row[$j]);
					$row[$j] = ereg_replace("\n","\\n",$row[$j]);
					if (isset($row[$j])) { $return.= '"'.$row[$j].'"' ; } else { $return.= '""'; }
					if ($j<($num_fields-1)) { $return.= ','; }
				}
				$return.= ");\n";
			}
		}
		$return.="\n\n\n";
	}
	
	//save file
	$bkdbname = 'db-backup-'.time().'-'.(md5(implode(',',$tables))).'.sql';
	$handle = fopen('./autobackups/' . $bkdbname ,'w+');
	 
	if(fwrite($handle,$return))
	dbQuery("INSERT INTO tbl_database_autobackup (backupDate,dbName) value(NOW(),'$bkdbname')");
	dbQuery("TRUNCATE TABLE tbl_loghistory");
	fclose($handle);
		
}
 
 /*
 / for process page
 */
 
 function updateprivilege()
{
	$userId = $_POST['userId'];
	$q  	= @$_POST[q];
	
	//delete user privilege
	dbQuery("DELETE FROM tbl_privilege WHERE user_id = $userId");
	$a = count($q);
	for($b = 0; $b < $a; $b++){
	dbQuery("INSERT INTO tbl_privilege (user_id,ModuleID,Position) VALUES('$userId','$q[$b]','Normal')");
	}
	if($userId == 1){
	dbQuery("INSERT INTO tbl_privilege (user_id,ModuleID,Position) VALUES('1','16','Normal')");
	}
	?>	
	<meta http-equiv="refresh" content="0; url=index.php?q=<?php echo MD5('privilege')?>&oaf45=<?php echo $userId; ?>&pG59u=<?php echo MD5('okay');?>">
	<?php
}
/*** TRACE VISITED BY USER PAGE ***/
  function traceUsers($page) 
 {
	$ip      = $_SERVER['REMOTE_ADDR'];
	
	if (strpos($_SERVER['HTTP_USER_AGENT'], 'MSIE') !== FALSE) { 
		$browser = 'Internet Explorer'; 
	} else if(strpos($_SERVER['HTTP_USER_AGENT'], 'Chrome') !== FALSE){
		$browser = 'Google Chrome';
	} else if(strpos($_SERVER['HTTP_USER_AGENT'], 'Firefox') !== FALSE){
		$browser = 'Mozilla Firefox';
	} else $browser = 'Unkown Browser';

	$Page	 = $page;//$pageTitle;
	$User_id = @$_SESSION['cive_user_id'];
	$query="INSERT INTO tbl_loghistory (IpAddress,User_id, browser,Page) values('$ip','$User_id','$browser','$Page')";
	mysql_query($query)or die(mysql_error());
}



///**************** NEW FUNCTIONS FOR EDMS

function employee($EmployeeID)
{
$data=mysql_query("select * from tbl_staff");
$num=mysql_num_rows($data);
echo"<select name='EmployeeID'>";
echo"<option value=''>::Select employee::</option>";
for($k=0;$k<=$num;$k++)
{
$d=mysql_fetch_array($data);
if($EmployeeID==$d['EmployeeID'])
{
echo"<option value='$d[EmployeeID]' selected>$d[EmployeeID]</option>"; 
}else
{
echo"<option value='$d[EmployeeID]'>$d[EmployeeID]</option>"; 
}
}
echo "</select>";
}





function taasisi($t)
{
echo"<select name='InstitutionID'><option value=''>::Select Institution ::</option>";
$scale=mysql_query("select * from tbl_institution") or die (mysql_error());
while($r=mysql_fetch_array($scale))
{
if($r['Id']==$t)
{
echo "<option value='$r[Id]' selected>$r[Name]</option>";
}else
{
echo "<option value='$r[Id]'>$r[Name]</option>";
}
}
echo"</select>";
}


function wizara($wz)
{
echo"<select name='MinistryID'><option value=''>::Select Ministry::</option>";
$scale=mysql_query("select * from tbl_ministry");
while($r=mysql_fetch_array($scale))
{
if($wz==$r['Id'])
{
echo "<option value='$r[Id]' selected>$r[Name]</option>";
}else
{
echo "<option value='$r[Id]'>$r[Name]</option>";
}
}
echo"</select>";
}


function vote($wz)
{
echo"<select name='VoteID'>
<option value=''>::Select Vote::</option>";
$scale=mysql_query("select * from tbl_vote");
while($r=mysql_fetch_array($scale))
{
if($wz==$r['VoteID'])
{
echo "<option value='$r[VoteID]' selected>$r[VoteID] - $r[VoteName]</option>";
}else
{
echo "<option value='$r[VoteID]'>$r[VoteID] - $r[VoteName]</option>";
}
}
echo"</select>";
}



function filecategory($wz)
{
echo"<select name='CategoryID'><option value=''>::Select File Category::</option>";
$scale=mysql_query("select * from tbl_fileCategory");
while($r=mysql_fetch_array($scale))
{
if($wz==$r['Id'])
{
echo "<option value='$r[Id]' selected>$r[Name]</option>";
}else
{
echo "<option value='$r[Id]'>$r[Name]</option>";
}
}
echo"</select>";
}


function file_action($wz)
{
echo"<select name='FileAction'><option value='Normal'>::File Action::</option>";
$scale=mysql_query("select * from tbl_file where Id='$Id'");
$r=mysql_fetch_array($scale);
if($r['FileAction']=='Upgrade')
{
echo "<option value='Upgrade' selected>Upgrade</option>";
echo "<option value='Downgrade'>Downgrade</option>";
}elseif($r['FileAction']=='Downgrade')
{
echo "<option value='Downgrade' selected>Downgrade</option>";
echo "<option value='Upgrade'>Upgrade</option>";
}else{
echo "<option value='Upgrade'>Upgrade</option>";
echo "<option value='Downgrade'>Downgrade</option>";		
}
echo"</select>";
}






function module_list()
{
echo"<select name='ModuleID'>
<option value=''>::Select Module::</option>";
$scale=mysql_query("select * from tbl_module");
while($r=mysql_fetch_array($scale))
{
echo "<option value='$r[ID]'>$r[ModuleName]</option>";
}
echo"</select>";
}

function location($wz)
{
	
$scale=mysql_query("select * from tbl_file where Id='$wz'");
$r= mysql_fetch_array ($scale);
$n=mysql_num_rows($scale);
if($n==1)
{
	$BlockNo=$r['BlockNo'];
	$ShelfNo = $r['Shelf'];
	$ColumnNo= $r['ColumnNo'];
	$PartNo = $r['PartNo'];
	$RoomNo = $r['RoomNo'];
	
}else
{
	$BlockNo="";
	$ShelfNo = "";
	$ColumnNo= "";
	$PartNo = "";
	$RoomNo = "";
	
}

?>
<table class="praise"> <tr>
<td> Block <br> <?php block($BlockNo); ?></td>
<td> Room <br> <?php room ($RoomNo); ?></td>
<td> Shelf <br> <input type='text' name='ShelfNo' value="<?php echo $ShelfNo; ?>"></td>
<td> Column <br ><input type='text' name='ColumnNo' value="<?php echo $ColumnNo; ?>"> </td>
<td> Part <input type='text' name='PartNo' value="<?php echo $PartNo; ?>"> </td>
</tr> </table>
<?php
}



function file_from_to_date($Id)
{

$scale=mysql_query("select * from tbl_file where Id='$Id' or FileNo='$Id'");
$r= mysql_fetch_array ($scale);
$n=mysql_num_rows($scale);
if($n==1)
{
	$FromDate=$r['FromDate'];
	$ToDate = $r['ToDate'];
	
}else
{
	$FromDate= " ";
	$ToDate = " ";	
	
}

?>
<table class='file_date'>

<tr>
<td>From</br>
<input type="text" class="span2" name="FromDate"  id="dp1" value="<?php echo $FromDate; ?>" >
</td>
<td>To </br>
<input type="text" class="span2" name="ToDate"  id="dp2"  value="<?php echo $ToDate; ?>">
</td>
</tr>
</table>
<?php

}


function block($wz)
{
echo"<select name='BlockID' >
<option value=''>::Block::</option>";
$scale=mysql_query("select * from tbl_block");
while($r=mysql_fetch_array($scale))
{
if($wz==$r['Name'])
{
echo "<option value='$r[Name]' selected>$r[Name]</option>";
}else
{
echo "<option value='$r[Name]'>$r[Name]</option>";
}
}
echo"</select>";
}


function room($wz)
{
echo"<select name='RoomNo'>
<option value=''>::Room::</option>";
$scale=mysql_query("select DISTINCT Name from tbl_room order by Name");
while($r=mysql_fetch_array($scale))
{
if($wz==$r['Name'])
{
echo "<option value='$r[Name]' selected>$r[Name]</option>";
}else
{
echo "<option value='$r[Name]'>$r[Name]</option>";
}
}
echo"</select>";
}
function file_info($wz,$i)
{
echo"<select name='FileNo'>
<option value=''>::Select File Reference No::</option>";
$scale=mysql_query("select * from tbl_file where InstitutionID='$i'");
while($r=mysql_fetch_array($scale))
{
if($wz==$r['FileNo'])
{
echo "<option value='$r[FileNo]' selected>$r[FileNo]-$r[FileTitle]</option>";
}else
{
echo "<option value='$r[FileNo]'>$r[FileNo]-$r[FileTitle]</option>";
}
}
echo"</select>";
}



function gender($Gender)
{
echo "<select name='Sex'>";
echo "<option value=''>---- Select Sex ----</option>";
if($Gender=='Male')
{
echo "<option value='Male' selected>Male</option>";
}else
{
echo "<option value='Male'>Male</option>";
}
if($Gender=='Female')
{
echo "<option value='Female' selected>Female</option>";
}else
{
echo "<option value='Female'>Female</option>";
}
echo "</select>";
}


function action_category($G)
{
echo "<select name='ActionCategory'>";
echo "<option value=''>---- Select Action ----</option>";
if($G=='Preservation')
{
echo "<option value='Preservation' selected>Preservation</option>";
}else
{
echo "<option value='Preservation'>Preservation</option>";
}
if($G=='Destruction')
{
echo "<option value='Destruction' selected>Destruction</option>";
}else
{
echo "<option value='Destruction'>Destruction</option>";
}
echo "</select>";
}




function keygen()
{
$p=date('Y');
$y=$p."-"."STAFF"."-";
$f=rand(0,9);
$s=rand(0,9);
$t=rand(0,9);
$ft=rand(1,9);
$id="$y"."$f"."$s"."$t"."$ft";
return $id;
}


function rOrder($InstitutionID)
{
$p="NRC";
$y=$p."-".$InstitutionID."-";
$f=rand(0,9);
$s=rand(0,9);
$t=rand(0,9);
$ft=rand(1,9);
$id="$y"."$f"."$s"."$t"."$ft";
return $id;
}


//GPA CALCULATIONS

###################################################
/**************************
	Paging Functions
	
	this java script is required
	function pagingOption(self,strGet)
	{
	with (window.document.frmPaging) {
		if (cboPanging.options[cboPanging.selectedIndex].value == 1) {
			window.location.href = self + "&" + strGet;
		} else {
			window.location.href = self + "&" + "page=" + cboPanging.options[cboPanging.selectedIndex].value + "&" + strGet;
		}
	}
	}
***************************/

function getPagingQuery($sql, $itemPerPage = 10)
{
	if (isset($_GET['page']) && (int)$_GET['page'] > 0) {
		$page = (int)$_GET['page'];
	} else {
		$page = 1;
	}
	
	// start fetching from this row number
	$offset = ($page - 1) * $itemPerPage;
	
	return $sql . " LIMIT $offset, $itemPerPage";
}

/*
	Get the links to navigate between one result page to another.
	Supply a value for $strGet if the page url already contain some
	GET values for example if the original page url is like this :
	
	http://www.udom.ac.tz/CIVE/index.php?q=content&c=12
	
	use "c=12" as the value for $strGet and "index.php?q=content" as $selfO. But if the url is like this :
	
	http://www.udom.ac.tz/CIVE/index.php
	
	then there's no need to set a value for $strGet
	
	
	
*/

function getPagingLink($sql, $itemPerPage = 10,$selfO, $strGet = '')
{
	$result        = dbQuery($sql);
	$pagingLink    = '';
	$totalResults  = dbNumRows($result);
	$totalPages    = ceil($totalResults / $itemPerPage);
	
	// how many link pages to show
	$numLinks      = $totalPages;

		
	// create the paging links only if we have more than one page of results
	if ($totalPages > 1) {
	
		$self = $selfO ;
		

		if (isset($_GET['page']) && (int)$_GET['page'] > 0) {
			$pageNumber = (int)$_GET['page'];
		} else {
			$pageNumber = 1;
		}
		
		// print 'previous' link only if we're not
		// on page one
		if ($pageNumber > 1) {
			$page = $pageNumber - 1;
			if ($page > 1) {
				$prev = "<li> <a href=\"$self&page=$page&$strGet\">Previous</a></li> ";
			} else {
				$prev = " <li class='disabled'><a href=\"$self&$strGet\">Previous</a></li> ";
			}	
				
			$first = "<li> <a href=\"$self&$strGet\">First</a></li> ";
		} else {
			$prev  = "<li  class='disabled'><a href=\"$self&$strGet\">Previous</a></li>"; // we're on page one, don't show 'previous' link
			$first = "<li class='disabled'><a href=\"$self&$strGet\">First</a></li>"; // nor 'first page' link
		}
	
		// print 'next' link only if we're not
		// on the last page
		if ($pageNumber < $totalPages) {
			$page = $pageNumber + 1;
			$next = "<li><a href=\"$self&page=$page&$strGet\">Next</a></li> ";
			$last = "<li> <a href=\"$self&page=$totalPages&$strGet\">Last</a></li> ";
		} else {
			$next = "<li class='disabled'><a href=\"$self&page=$page&$strGet\">Next</a></li>"; // we're on the last page, don't show 'next' link
			$last = "<li class='disabled'><a href=\"$self&page=$page&$strGet\">Last</a></li>"; // nor 'last page' link
		}

		$start = $pageNumber - ($pageNumber % $numLinks) + 1;
		$end   = $start + $numLinks - 1;		
		
		$end   = min($totalPages, $end);
		
		//onChange="window.location.href='index.php';"
		$pagingLink = '';
		$pagingLink = "<select name='cboPanging' onChange='pagingOption(\"$self\",\"$strGet\");' style='width:50px'>";
		for($page = 1; $page <= $totalPages; $page++)	{
			if ($page == $pageNumber) {
				$pagingLink = $pagingLink . "<option value=\"$pageNumber\" selected>$pageNumber </option>";   // no need to create a link to current page
			} else {
				/*if ($page == 1) {
					$pagingLink[] = " <a href=\"$self?$strGet\">$page</a> ";
				} else {	
					$pagingLink[] = " <a href=\"$self?page=$page&$strGet\">$page</a> ";
				}*/
				$pagingLink = $pagingLink . "<option value=\"$page\">$page</option>";
			}
	
		}
		
		$pagingLink = $pagingLink . "</select>";
		// return the page navigation link
		$pagingLink = "&nbsp;" . $first . "&nbsp;" . $prev . "&nbsp;Page&nbsp;" . $pagingLink . "&nbsp;of&nbsp;" .$totalPages . "&nbsp;" 
		. $next . "&nbsp;" . $last . "&nbsp;&nbsp;Total Records <strong>" . $totalResults ."</strong>";
		$pagingLink = "<form name='frmPaging' method='POST' width='210' height='30px'><ul class='pager'>" . $pagingLink . "</ul></form>";


$pagingLink=$pagingLink;
	}
	
	return $pagingLink;
}
























/***********START***********/























 /**********END *******/



function recordSaved()
{
?>
	
<div class="alert alert-success">  
<a class="close" data-dismiss="alert">X</a>  
<strong>Record Saved!</strong>  
</div> 
	
<?php
	
}


function mafanikio($w)
{
?>
	
<div class="alert alert-success">  
<a class="close" data-dismiss="alert">X</a>  
<strong>
<?php
echo $w;

?>	
	
</strong>  
</div> 
	
<?php
	
}


function noma($w)
{
?>
	
<div class="alert alert-warning">  
<a class="close" data-dismiss="alert">X</a>  
<strong>
<?php
echo $w;

?>	
	
</strong>  
</div> 
	
<?php
	
}


function savingError()
{
?>
<div class="alert alert-warning" >  
  <a class="close" data-dismiss="alert">X</a>  
  <strong>Error on Saving!</strong> - This is a fatal error.  
</div>


<?php	
}


function existError()
{
?>
<div class="alert alert-warning" >  
  <a class="close" data-dismiss="alert">X</a>  
  <strong>Data Already exist!</strong> - This is a fatal error.  
</div> 	
<?php	
}


function noexistError()
{
?>
<div class="alert alert-warning" >  
  <a class="close" data-dismiss="alert">X</a>  
  <strong>Information you are trying to update does not exist!</strong> - This is a fatal error.  
</div> 	
<?php	
}

function fillerror ()
{
?>
<div class="alert alert-warning" >  
  <a class="close" data-dismiss="alert">X</a>  
  <strong>Fill all fields!</strong> - This is a fatal error.  
</div> 	
<?php
}



function duration($x)
{
$scale=mysql_query("select * from tbl_file where Id='$x'");
$r= mysql_fetch_array ($scale);
$n=mysql_num_rows($scale);
if($n==1)
{
$x=$r['Duration'];	
}	
echo "<select name='Duration'><option> ------ Select Duration ----- </option>";
for($c=1;$c<=20;$c++)
{
if ($x==$c)
echo "<option value='$x' selected>$x Years</option>";
echo "<option value='$c'>$c Years</option>";
}
echo "</select>";
}


function left_menu()
{
    
    //DASHBOARD DEFAULT
    ?>
    
        <li><a class="active" href="./index.php?q=<?php echo MD5('mido'); ?>">
	<i class="fa fa-dashboard fa-fw"></i> Dashboard</a>
	</li>
        
    <?php
    //$mod    = //dbQuery("SELECT m.ID, m.ModuleName, m.Iconsmall, m.ModuleClass 
					   //FROM tbl_module m, tbl_privilege p 
					   //WHERE m.ID = p.ModuleID AND p.priorityId = '{$_SESSION['user_category']}'  ORDER BY m.torder");//user_category
    $mod= mysql_query("select DISTINCT m.ID, m.ModuleName, m.Iconsmall, m.ModuleClass from tbl_staff s, tbl_user_priority u, tbl_privilege p, tbl_module m where s.priorityId=u.id and  u.id=p.priorityId and p.ModuleID=m.ID
		      and m.Category='Backend'");
    
    while($modrow = dbFetchAssoc($mod))
    {
        $modId = $modrow['ID'];
        $modname = $modrow['ModuleName'];
        $modicon = $modrow['Iconsmall'];
        $mod_class = $modrow['ModuleClass'];
        //$module_icon =$modrow['css_icon'];
        $submod = dbQuery("SELECT s.submodName, s.submodTitle, s.subUrl FROM tbl_submodule s WHERE s.parentID = '$modId' AND s.Shows=1");
		
        $n=mysql_num_rows($submod);
 $closer="</li>";
	if($n>0)
	{
	$kifungio="</ul>";
	?>
	 <li><a href="#" title="<?php echo $modname; ?>">
	  <i class="<?php echo $mod_class;?>"></i>&nbsp;&nbsp;<?php echo $modname; ?><span class="fa arrow"></span></a>
	  
	  <ul class="nav nav-second-level">
	  <!--Side Drop Down Menu -->
	<?php
	}else {
	?>
	<li><a href="#" title="<?php echo $modname; ?>"><i class="<?php echo $mod_class;?>"></i>
	<?php echo $modname; ?></a></li>
	<?php
	}
    	while($submodrow = dbFetchAssoc($submod))
		{
	?>
	<li><a href="./index.php?q=<?php echo $submodrow['subUrl']; ?>" title="<?php echo $submodrow['submodTitle']; ?>">
	<?php echo $submodrow['submodName']; ?></a></li>
	<?php
		}
		echo @$kifungio.$closer;
		
	}

}

function menu_list()
{
?>
        <li>
		<a class="active" href="./index.php?q=<?php echo MD5('mido'); ?>"><i class="fa fa-dashboard fa-fw"></i> Dashboard</a> </li>
        

			<!--Left menus from database  -->
			<?php
			$mod    = dbQuery("SELECT m.ID, m.ModuleName, m.Iconsmall, m.ModuleClass  FROM tbl_module m, tbl_privilege p WHERE m.ID = p.ModuleID AND p.user_id = '{$_SESSION['cive_user_id']}' AND m.leftdisplay = '1' ORDER BY m.torder") or die (mysql_error());
			while($modrow = dbFetchAssoc($mod)){
			$modId = $modrow['ID'];
			$modname = $modrow['ModuleName'];
			$modicon = $modrow['Iconsmall'];
			$mod_class = $modrow['ModuleClass'];
			$submod = dbQuery("SELECT s.submodName, s.submodTitle, s.subUrl
							   FROM tbl_submodule s
							   WHERE s.parentID = '$modId' and s.Shows=1");
			?>

				<li>
					<a class="menuitem submenuheader"  title="<?php echo $modname; ?>" >
						<i class="<?php echo $mod_class;?>"></i>
						<?php echo $modname; ?> 
					</a>
					
					<ul class="list-group">
					<div class="submenu">
					<?php
					while($submodrow = dbFetchAssoc($submod))
					{
					?>
					<li class="list-group-item"><a href="<?php echo WEB_ROOT;?>index.php?q=<?php echo MD5($submodrow['subUrl']); ?>" 
					title="<?php echo $submodrow['submodTitle']; ?>">
					<?php echo $submodrow['submodName']; ?></a>
					<?php
					}
					?>
					</div>
					</ul>
					</li>

			<?php
			}
			?>


<?php
}

function side_menu()
{
?>
				<div class="accordion" id="leftMenu">
                    <div class="accordion-group">
                        <div class="accordion-heading">
                            <a class="accordion-toggle" data-toggle="collapse" data-parent="#leftMenu" 
							href="index.php?q=<?php echo MD5('mido'); ?>">
                                  <i class="icon-home"></i> Dashboard
                            </a>
                        </div>
					</div>

                    <?php
$mod    = dbQuery("SELECT m.ID, m.ModuleName, m.Iconsmall, m.ModuleClass  FROM tbl_module m, tbl_privilege p WHERE m.ID = p.ModuleID AND p.user_id = '{$_SESSION['cive_user_id']}' AND m.leftdisplay = '1' ORDER BY m.torder") or die (mysql_error());
			while($modrow = dbFetchAssoc($mod)){
			$modId = $modrow['ID'];
			$modname = $modrow['ModuleName'];
			$modicon = $modrow['Iconsmall'];
			$mod_class = $modrow['ModuleClass'];
			$submod = dbQuery("SELECT s.submodName, s.submodTitle, s.subUrl
							   FROM tbl_submodule s
							   WHERE s.parentID = '$modId'");
					?>
					<div class="accordion-group">
                        
						<div class="accordion-heading">
                            <a class="accordion-toggle" data-toggle="collapse" data-parent="#leftMenu" href="#collapseTwo">
                                <i class="<?php echo $mod_class;?>">
								</i> <?php echo $modname; ?>
                            </a>
                        </div>
                        <div id="collapseTwo" class="accordion-body collapse" style="height: 0px; ">
                            <div class="accordion-inner">
                                <ul>
								<?php
								while ($submodrow = dbFetchAssoc($submod))
								{
								?>
                  <li><a href="<?php echo WEB_ROOT;?>index.php?q=<?php echo MD5($submodrow['subUrl']); ?>" 
					title="<?php echo $submodrow['submodTitle']; ?>">
					<?php echo $submodrow['submodName']; ?></a></li>
                                <?php
								}
								?>
                                </ul>
                            </div>
                        </div>
                    </div>
					<?php
					}
					
					?>
					</div>
					
                    
<?php
}

	
	function file_group($x)
	{
	$scale=mysql_query("select * from tbl_fileCategory where Id='$x'");
	$r= mysql_fetch_array ($scale);
	return $r['Name'];
	}

	
	function file_expiry($date1,$date2)
	{
	$date1a = explode("/",$date1);
	$date2b = explode ("/",$date12);
 if ($date1a[0]>$date2a[0])
 {
  //Deadline 
 }elseif($date1a[1]>$date2a[1])
 {
   //Deadline 
 
 }elseif($date1a[2]>$date2a[2])
 {
 //Deadline 
 
 }
	return @$diff;
	}
	
	
	
	
	
	
	function file_expire_id ($FileNo,$k)
	{
	
	$today=date('d-m-Y');
	
	$dates=mysql_query("SELECT * FROM tbl_file_lend where FileNo = '$FileNo'")or die(mysql_error());
	
	$r=mysql_fetch_array($dates);
	
	$expdate=strtotime($r['ReturnDate']);

	$stamp_leo = strtotime($today);
	$stamp_jana = $expdate;
	
	$diff = $stamp_leo - $stamp_jana;
		@define('DAY11',60*60*24, true);
		@define('MONTH11',DAY11*30, true);
		@define('YEAR11',DAY11*365, true);

	$years = floor($diff / (YEAR11));
	$months = floor(($diff - $years * YEAR11) / (MONTH11));
	$days = floor(($diff - $years * YEAR11 - $months*MONTH11 ) / (DAY11));
	
	
	if($stamp_leo>=$stamp_jana)
	{
	$status=1;
	
	}else
	{
	$status=0;
	
	}
	

	
	switch($k)
	{
	case 1: return $status;break;
	case 2: return $days;break;
	default: return $today;
	}
	
	}
	
	
	
function nrc_key()
{
$p=date('Y');
$y=$p."-"."DEOSYS"."-";
$f=rand(0,9);
$s=rand(0,9);
$t=rand(0,9);
$ft=rand(1,9);
$id="$y"."$f"."$s"."$t"."$ft";
return $id;
}
	
		
	function malipo ($k,$w)
	{
	
	
	
	if ($w==1)//new license
	{
	$today=date('d-m-Y');
	$add_days = $k;
	$date = date('Y-m-d',strtotime($today) + (24*3600*$add_days));
	$key=nrc_key();
	$mute=mysql_query("delete from tbl_malipo") or die (mysql_error());
	
	$dates=mysql_query("INSERT INTO tbl_malipo (SN,Expire, Status) values ('$key','$date',0)") or die (mysql_error());
	
	}
	
	if ($w==2) //block
	{
	//$mute=mysql_query("delete from tbl_malipo") or die (mysql_error());
	
	$dates=mysql_query("UPDATE tbl_malipo SET Status=1") or die (mysql_error());
	
	}
	
	if ($w==3) //unblock
	{
	//$mute=mysql_query("delete from tbl_malipo") or die (mysql_error());
	
	$dates=mysql_query("UPDATE tbl_malipo SET Status=0") or die (mysql_error());
	
	}
	

	
	}
	
	
	
	function license_expire()
	{
	
	$today=date('d-m-Y');
	
	$dates=mysql_query("SELECT * FROM tbl_malipo where Status =0")or die(mysql_error());
	
	$r=mysql_fetch_array($dates);
	
	$expdate=strtotime($r['Expire']);

	$stamp_leo = strtotime($today);
	$stamp_jana = $expdate;
	
	$diff = $stamp_leo - $stamp_jana;
		@define('DAY11',60*60*24, true);
		@define('MONTH11',DAY11*30, true);
		@define('YEAR11',DAY11*365, true);

	$years = floor($diff / (YEAR11));
	$months = floor(($diff - $years * YEAR11) / (MONTH11));
	$days = floor(($diff - $years * YEAR11 - $months*MONTH11 ) / (DAY11));
	
	
	if($stamp_leo>=$stamp_jana)
	{
	$status=1;
	$lock=mysql_query("update tbl_malipo set Status=1");
	
	}else
	{
	$status=0;
	
	}
	
	return $status;

	
	}
	
	
	
	
	
	 function upload_file_sucess()
{
?>
	
<div class="alert alert-success">  
<a class="close" data-dismiss="alert">x</a>  
<strong>File records import was done successfully</strong>  
</div> 
	
<?php
	
}


function uploadError($x)
{
?>
<div class="alert alert-warning" >  
  <a class="close" data-dismiss="alert">x</a>  
  <strong>Error: <?php echo $x;?></strong> 
</div> 	
<?php	
}



function file_cats($x)
{
//echo"<select name='CategoryID'><option value=''>::Select File Category::</option>";
$scale=mysql_query("select * from tbl_fileCategory where Id='$x'");
$r=mysql_fetch_array($scale);
return $r['Name'];
}





function data_log($FileNo,$Action)
{
    
    $StaffID=$_SESSION['cive_user_id'];
    $data=mysql_query("INSERT INTO tbl_log (FileNo,StaffID,Action,logDate) values ('$FileNo','$StaffID',
		      '$Action',now())")or die (mysql_error());
    
    
}



function file_log($Id,$Action)
{
    
  $UserID=$_SESSION['cive_user_id'];
    
    $chota=mysql_query("select * from tbl_file where Id='$Id'");
    $r=mysql_fetch_array($chota);
    
$FileNo   =  $r['FileNo'];
$FileTitle    = $r['FileTitle'];
$ArrivalDate      =  $r['ArrivalDate'];
$Duration  =  $r['Duration'];
$DisposalDate      =  $r['DisposalDate'];
$BoxNumber      =  $r['BoxNumber'];
$CategoryID      =  $r['CategoryID'];
$InstitutionID      =  $r['InstitutionID'];
$FromDate      =  $r['FromDate'];
$ToDate      =  $r['ToDate'];
$Shelf      =  $r['Shelf'];
$RoomNo      =  $r['RoomNo'];
$ColumnNo      =  $r['ColumnNo'];
$PartNo      =  $r['PartNo'];
$BlockNo      =  $r['BlockNo'];
$ActionCategory      =  $r['ActionCategory'];
$ActionDate     =  $r['ActionDate'];
$ConsignmentNumber =  $r['ConsignmentNumber'];
$Registrar =  $r['Registrar'];

$Location= $BlockNo.' / '.$RoomNo.' / '.$Shelf .' / '.$ColumnNo.' / '.$PartNo;
  
  $plug="insert into tbl_file_log (RoomNo,FileTitle, FileNo,ArrivalDate,Duration,DisposalDate,BoxNumber,
	CategoryID,InstitutionID, Location, ActionCategory, ActionDate, BlockNo, ColumnNo,
	PartNo, Shelf, ToDate, FromDate,Registrar, ConsignmentNumber,Action,UserID,ActDate)
        values('$RoomNo','$FileTitle','$FileNo','$ArrivalDate','$Duration','$DisposalDate','$BoxNumber',
	'$CategoryID','$InstitutionID', '$Location', '$ActionCategory', '$ActionDate', '$BlockNo',
	'$ColumnNo', '$PartNo', '$Shelf', '$ToDate', '$FromDate','$Registrar','$ConsignmentNumber','$Action','$UserID',now())";
	
  $db=mysql_query($plug) or die (mysql_error());
       
}


function file_recover($Id)
{
    
    $Registrar=$_SESSION['cive_user_id'];
    
    $chota=mysql_query("select * from tbl_file_log where Id='$Id'");
    $r=mysql_fetch_array($chota);
    
$FileNo   =  $r['FileNo'];
$FileTitle    = $r['FileTitle'];
$ArrivalDate      =  $r['ArrivalDate'];
$Duration  =  $r['Duration'];
$DisposalDate      =  $r['DisposalDate'];
$BoxNumber      =  $r['BoxNumber'];
$CategoryID      =  $r['CategoryID'];
$InstitutionID      =  $r['InstitutionID'];
$FromDate      =  $r['FromDate'];
$ToDate      =  $r['ToDate'];
$Shelf      =  $r['Shelf'];
$RoomNo      =  $r['RoomNo'];
$ColumnNo      =  $r['ColumnNo'];
$PartNo      =  $r['PartNo'];
$BlockNo      =  $r['BlockNo'];
$ActionCategory      =  $r['ActionCategory'];
$ActionDate     =  $r['ActionDate'];
$ConsignmentNumber =  $r['ConsignmentNumber'];
 $Location= $BlockNo.' / '.$RoomNo.' / '.$Shelf .' / '.$ColumnNo.' / '.$PartNo;
 $Registrar= $_SESSION['cive_user_id'];
 $Action="restore";
  
  $plug="insert into tbl_file(RoomNo,FileTitle, FileNo,ArrivalDate,Duration,DisposalDate,BoxNumber,
	CategoryID,InstitutionID, Location, ActionCategory, ActionDate, BlockNo, ColumnNo,
	PartNo, Shelf, ToDate, FromDate,Registrar, ConsignmentNumber)
        values('$RoomNo','$FileTitle','$FileNo','$ArrivalDate','$Duration','$DisposalDate','$BoxNumber',
	'$CategoryID','$InstitutionID', '$Location', '$ActionCategory', '$ActionDate', '$BlockNo',
	'$ColumnNo', '$PartNo', '$Shelf', '$ToDate', '$FromDate','$Registrar','$ConsignmentNumber')";
	
  $db=mysql_query($plug) or die (mysql_error());
  
  $toa=mysql_query("delete from tbl_file_log where Id='$Id'")or die(mysql_error());
       
}



function backup_ifo()
{
	
$resultdb = dbQuery(" SELECT backupDate FROM tbl_database_backup ORDER BY backupId DESC");
$rowdb	= dbFetchAssoc($resultdb);
if($rowdb)
{
 $dated = $rowdb['backupDate'];
}else
{
 $dated = '0000-00-00';
}
?>
<div align="right">
<span class="label label-info">LAST BACKUP DATE:
<?php
echo dateFormat($dated);
?>
</span>
</div> 
<?php	
}

function user_delete()
{
$user_id=$_SESSION['cive_user_id'];
$name=mysql_query ("select * from tbl_user where user_id='$user_id'");
$n=mysql_fetch_array($name);
$right=$n['Admin'];
return $right;
}


function file_data($x,$k)
{
   $fd=mysql_query("select * from  tbl_file where FileNo='$x'");
   $r=mysql_fetch_array($fd);
   $FileTitle=$r['FileTitle'];
   
   switch ($k)
   {
	case 1: $y=$FitleTitle; break;
	default : $y=$FitleTitle; break;
	
   }
   
 return $y;	
}



########## CIVE WEBSITE FUNCTIONS ########
//Notification popup
function notification($message,$type){
	if($type == "success"){
		$alert="success";
	}else if($type == "error"){
		$alert="danger";
	}else if($type == "warning"){
		$alert="warning";
	}
	?>
	<div class="alert alert-<?php echo $alert; ?>">  
	<a class="close" data-dismiss="alert">x</a>  
	<?php
		echo $message;
	?>	 
	</div> 	
	<?php
}
















function publication_category()
{
echo "<select name='Category'  id='pCategory'>";
echo "<option value=''>::Select Category::</option>";
echo "<option value='1'>Journal Article</option>";
echo "<option value='2'>Conference Article</option>";
echo "<option value='3'>Report</option>";
echo "</select>";
}






function department($id=0)
{
$ct=mysql_query("SELECT * FROM tbl_department");
$num=mysql_num_rows($ct);
echo "<select name='DepartmentID' id='accountDepartment'><option>--Select Department--</option>";

while ($ctr=mysql_fetch_array($ct))
{
	($ctr[Id]==$id)? $active="selected='selected'":$active='';
	echo "<option value='$ctr[Id]' $active>$ctr[Name]</option>";
}
echo "</select>";
}



function school()
{
	$ct=mysql_query("SELECT * FROM tbl_school");
	$num=mysql_num_rows($ct);
	echo "<select name='SchoolID' id='accountDepartment'><option>--Select School--</option>";
	for($c=1;$c<=$num;$c++)
	{
	$ctr=mysql_fetch_array($ct);
	echo "<option value='$ctr[Id]'>$ctr[Name]</option>";
	}
	echo "</select>";
}


function staff()
{
$ct=mysql_query("SELECT * FROM tbl_staff") or die (mysql_error());
$num=mysql_num_rows($ct);
echo "<select name='StaffID' id='StaffID'><option>--Select Staff--</option>";
for($c=1;$c<=$num;$c++)
{
$ctr=mysql_fetch_array($ct);
echo "<option value='$ctr[staffId]'>$ctr[firstName] $ctr[lastName]</option>";
}
echo "</select>";
}


function delete_qualification ($StaffID,$Id)
{
	$del=mysql_query("delete from tbl_academic_profile where StaffID ='$StaffID' and Id='$Id'") or die (mysql_error());
	
}


function delete_school ($CollegeID,$Id)
{
	$del=mysql_query("delete from tbl_school where CollegeID ='$CollegeID' and Id='$Id'") or die (mysql_error());
	
}


function delete_department ($Id)
{
	$del=mysql_query("delete from tbl_department where Id='$Id'") or die (mysql_error());
	
}


function delete_programe ($Id)
{
	$del=mysql_query("delete from tbl_program where Id='$Id'") or die (mysql_error());
	
}


function delete_publication ($Id)
{
	$del=mysql_query("delete from tbl_publication where Id='$Id'") or die (mysql_error());
	
}

function academic_profile($StaffID)
{
	
	global $self;
	
	
	$data=mysql_query("select * from tbl_academic_profile where StaffID ='$StaffID' order by GraduationDate ASC") or die (mysql_error());
	while ($r=mysql_fetch_array($data))
	{
		
		$Level=$r['Level'];
		$Name=$r['Name'];
		$Institution=$r['Institution'];
		$GraduationDate=$r['GraduationDate'];
		$Id=$r['Id'];
	   ?>
	   <ol class="widget-list list-dotted">
              <li class="media"> <span class="pull-left"><span class="fontello-icon-graduation-cap"></span></span>
                <div class="media-body"> <span class="date"><?php echo $Level;?></span> <span class="quick-menu-icon pull-right">
			<a class="state fontello-icon-trash-1" href="<?php echo $self ."&del=".$Id;?>"></a> </span>
			<p class="note"><?php echo $Name;?></p> <?php echo $Institution;?> (<?php echo $GraduationDate;?>)
                </div>
              </li>
           </ol>
	<?php	
	}
	
}






function school_list($CollegeID)
{
	
	global $self;
	
	
	$data=mysql_query("select * from tbl_school where CollegeID ='$CollegeID' order by Name ASC") or die (mysql_error());
	while ($r=mysql_fetch_array($data))
	{
		
		$Name=$r['Name'];
		$Description=$r['Description'];
		$Dean=$r['Dean'];
	
		$Id=$r['Id'];
	   ?>
	   <ol class="widget-list list-dotted">
              <li class="media"> <span class="pull-left"><span class="fontello-icon-graduation-cap"></span></span>
                <div class="media-body"> <span class="date"><?php echo $Name;?></span> <span class="quick-menu-icon pull-right">
			<a class="state fontello-icon-trash-1" href="<?php echo $self ."&del=".$Id;?>"></a> </span>
			<p class="note"><?php echo $Description;?></p>
                </div>
              </li>
           </ol>
	<?php	
	}
	
}





function department_list($CollegeID)
{
	
	global $self;
	
	
	$data=mysql_query("select d.Id as dId, s.Name as sName, d.Name as dName , d.Description as dDescription from tbl_school s, tbl_department d where s.ID=d.SchoolID and s.CollegeID ='$CollegeID' order by dName ASC") or die (mysql_error());
	while ($r=mysql_fetch_array($data))
	{
		
		$dName=$r['dName'];
		$dDescription=$r['dDescription'];
		$sName=$r['sName'];
	
		$Id=$r['dId'];
	   ?>
	   <ol class="widget-list list-dotted">
              <li class="media"> <span class="pull-left"><span class="fontello-icon-graduation-cap"></span></span>
                <div class="media-body"> <span class="date"><?php echo $dName;?></span> <span class="quick-menu-icon pull-right">
			<a class="state fontello-icon-trash-1" href="<?php echo $self ."&del=".$Id;?>"></a> </span>
			<p class="note"><?php echo $dDescription;?></p> - <?php echo $sName;?>
                </div>
              </li>
           </ol>
	<?php	
	}
	
}



function programe_list($CollegeID)
{
	
	global $self;
	
	
	$data=mysql_query("select p.Id as dId, p.Name as pName, d.Name as dName , p.Description as pDescription
			  from tbl_program p, tbl_department d where p.DepartmentID=d.Id order by dName ASC") or die (mysql_error());
	while ($r=mysql_fetch_array($data))
	{
		
		$dName=$r['dName'];
		$pDescription=$r['pDescription'];
		$pName=$r['pName'];
	
		$Id=$r['dId'];
	   ?>
	   <ol class="widget-list list-dotted">
              <li class="media"> <span class="pull-left"><span class="fontello-icon-graduation-cap"></span></span>
                <div class="media-body"> <span class="date"><?php echo $pName;?></span> <span class="quick-menu-icon pull-right">
			<a class="state fontello-icon-trash-1" href="<?php echo $self ."&del=".$Id;?>"></a> </span>
			<p class="note"><?php echo $pDescription;?></p> - <?php echo $dName;?>
                </div>
              </li>
           </ol>
	<?php	
	}
	
}



function programe_list_public($DepartmentID)
{
	
	global $self;
	
	
	$data=mysql_query("select p.Id as dId, p.Name as pName, d.Name as dName , p.Description as pDescription
			  from tbl_program p, tbl_department d where p.DepartmentID=d.Id and d.Id='$DepartmentID' order by dName ASC") or die (mysql_error());
	while ($r=mysql_fetch_array($data))
	{
		
		$dName=$r['dName'];
		$pDescription=$r['pDescription'];
		$pName=$r['pName'];
	
		$Id=$r['dId'];
	   ?>
	   <ol class="widget-list list-dotted">
              <li class="media"> <span class="pull-left"><span class="fontello-icon-graduation-cap"></span></span>
                <div class="media-body"> <span class="date"><?php echo $pName;?></span> <span class="quick-menu-icon pull-right">
			<a class="state fontello-icon-trash-1" href="<?php echo $self ."&del=".$Id;?>"></a> </span>
			<p class="note"><?php echo $pDescription;?></p> - <?php echo $dName;?>
                </div>
              </li>
           </ol>
	<?php	
	}
	
}



function publication_list($StaffID)
{	
	global $self;	
?>
<p class="lebo">List of Publications &nbsp;&nbsp;
&nbsp;&nbsp;&nbsp; | &nbsp;&nbsp;&nbsp;
<a href="<?php echo $self;?>&new_publication=<?php echo $StaffID; ?>"><span class='label label-success'>New Publication</span></a>
</p>
	
<table class="table table-striped table-bordered bootstrap-datatable datatable" id='dataTables-example'>
<thead>
<tr>
						
<th>S/N</th>
<th>Title</th>
<th>Publisher</th>
<th>Authors</th>
<th>Year</th>
<th>Volume (Issue)</th>

<th>Page</th>

</tr>
</thead>   
<tbody>
	
	
	<?php
	
	
	
	global $self;
	
	$k=1;
	$data=mysql_query("select * from tbl_publication p, tbl_staff s where s.StaffId=p.StaffID and p.StaffID='$StaffID' order by p.Year ASC") or die (mysql_error());
	while ($r=mysql_fetch_array($data))
	{
		
		$Title=$r['Title'];
		$Publisher=$r['Publisher'];
		$Year=$r['Year'];
		$Id=$r['Id'];
		$Author=$r['CAuthor'];
		$Page=$r['Page'];
		$Volume=$r['Volume'];
		$Issue=$r['Issue'];
		$Author=$r['CAuthor'];
		$Name=$r['lastName'].",".$r['firstName'];
		
		$Author = "<b>".$Name."</b> ,".$Author;
	
	   
	   echo"<tr>
	    <td>$k</td>
	    <td>$Title</td>
	    <td>$Publisher</td>
	    <td>$Author</td>
	     <td>$Year</td>
	      <td>$Volume ($Issue)</td>
	       
	        <td>$Page</td>
	   </tr>";
	   
	   ?>
	   
	   
	   <!--
	   <ol class="widget-list list-dotted">
              <li class="media"> <span class="pull-left"><span class="fontello-icon-graduation-cap"></span></span>
                <div class="media-body"> <span class="date"><?php echo $Title ;?></span> <span class="quick-menu-icon pull-right">
			<a class="state fontello-icon-trash-1" href="<?php echo $self ."&del=".$Id;?>"></a> </span>
			<p class="note"><?php echo $Author;?></p><?php echo $Publisher;?>(<?php echo $Volume;?>), <?php echo $Page;?>
                </div>
              </li>
           </ol>
	   -->
	<?php
	$k++;
	}
	?>
</tbody></table>
	<?php
	
}







function upload()
 {
 global $StaffID,$self;
$action="$self&StaffID=$StaffID&Pic=1";
$mi="$self&StaffID=$StaffID";
 ?>
 <form name= "profile" action="<?php echo $self;?>" enctype="multipart/form-data" method="POST">
 <input type="hidden" name="MAX_FILE_SIZE" value="5120000000">
                <label for="accountPrefix2" class="control-label">Picture</label>
                <div class="controls">
		 <input type="file" name="fileupload" id="fileupload" id="accountPrefix2" class="span6">
                </div>
 <input type="hidden" name="StaffID" value="<?php echo $StaffID;?>"/>
 <button type="submit" name="image" class="btn btn-blue"> Upload </button>
 </form>

<?php
}

 function uploader()
 {
 global $StaffID,$self;
 $mi="$self&EMPID=$StaffD";
 $Picture=keygen("PC");
 $file = "employeePicture";
 $test=mysql_query("SELECT * FROM tbl_staff WHERE StaffID='$StaffID'");
 $n=mysql_num_rows($test);
 if($n==1)
 {
 foreach($_FILES as $file_name => $file_array) 
 {
     if (is_uploaded_file($file_array['tmp_name'])) 
	 {
	 $Type=$file_array['type'];
	 if(($Type=="image/jpeg")||($Type=="image/gif")||($Type=="image/bmp"))
	 {
         move_uploaded_file($file_array['tmp_name'],"$file/$Picture.jpg") or die ("Couldn't copy");
		 $upd=mysql_query("update tbl_staff set Picture='$Picture'  where  StaffID='$StaffID'");
         //echo "Picture Successfuly Uploaded<br><br>";
		 ?>	
		<meta http-equiv="refresh" content="0; url=<?php echo $mi;?>">
		<?
	 }else
	 {
	 echo"<p class='message'>Image type Not Supported</p>";
	 }
     }
}
}
else
{
echo "<p class='message'>INVALID StaffID</p>";
}
}

//other version
function uploadFile($picFile,$folder,$type){
	global $imageFile, $dataFile;
	$isuploaded = false;
	if(!empty($_FILES[$picFile]))
	{	//print_r( $_FILES );
		#constants
		$fileavailable=0;
		
		#check file extension
		$file = $_FILES[$picFile]['name'];
		//The original name of the file on the client machine. 
		$filetype = $_FILES[$picFile]['type'];
		//The mime type of the file, if the browser provided this information. An example would be "image/gif". 
		$filesize = $_FILES[$picFile]['size'];
		//The size, in bytes, of the uploaded file. 
		$filetmp  = $_FILES[$picFile]['tmp_name'];
		//The temporary filename of the file in which the uploaded file was stored on the server. 
		$filetype_error = $_FILES[$picFile]['error'];
		// In PHP earlier then 4.1.0, $HTTP_POST_FILES  should be used instead of $_FILES.
		$errorArray = array();
		
		if (is_uploaded_file($filetmp))
		{
			$filename= getSalt(15) . $file;
			copy($filetmp, "$folder/$filename");
			move_uploaded_file($filetmp, "$folder/$filename");
			$str = $filename;
			$i = strrpos($str,".");
			$l = strlen($str) - $i;
			$ext = substr($str,$i+1,$l);
			$pext = strtolower($ext);
			if($type == "image"){
				if (!in_array($pext,$imageFile))
				{
					unlink("$folder/$filename");
				} else {
					$isuploaded = true;
					return $filename;
				}
			}elseif($type == "data"){
				if (!in_array($pext,$dataFile))
				{
					unlink("$folder/$filename");
				} else {
					$isuploaded = true;
					return $filename;
				}
			}
		}
		else 
		{
			return $isuploaded;
		}
	}else{
		return $isuploaded;
	}
}
##########UPLOADING#############




function article_list()
        {
            if(isset($_GET['id']))
            {
                $did=$_GET['id'];
                $article=mysql_query("select * from tbl_article where Id !='$did' order by PostDate DESC") or die (mysql_error());
                
            }else{
                
                 $article=mysql_query("select * from tbl_article order by PostDate DESC") or die (mysql_error());
            }
           echo "<table id='article-list' class='display' cellspacing='0' width='100%'>";
           echo"<thead>
	   <tr>
	   <th>S/N</th>
	   <th>Title</th>
	   <th>Category</th>
	   <th>Option</th>
	   </tr>
	   </thead>
	   <tbody>";
		$w=1;
                while ( $r=mysql_fetch_array($article))
                {
                    $content=$r['Content'];
                    $n=500;
                    $content= substr($content,0,$n);
                    $title=$r['Title'];
                    $id=$r['Id'];
                    $blog="blog.php?id=".$id;
                    $link="<a class='btn btn-blog pull-right moretag' href='$blog'>Read more</a>";
                    $content= $content. "....". $link;
                    $PostDate=$r['PostDate'];
                    $Category=$r['Category'];
		    
                    $image=$r['Image'];

		
	echo"<tr>
	   <td>$w</td>
	   <td>$title</td>
	   <td>$Category</td>
	   <td> </td>
	   </tr>";

                 $w++;
                 
                }
                ?>

		
              </tbody></table>
            <?php
        }
	




/************* CIVE WEBISTE FUNCTIONS ********************/



function quick_links ()
{
    $link=mysql_query("select * from tbl_link where Category='Quick' and Status=1");
     echo "<ul class='list-unstyled'>";
    while ($r=mysql_fetch_array($link))
    {
     $url=$r['Url'];
     $name=$r['Name'];
      echo"<li><a href='$url' target='_blank'>$name</a></li>"; 
    }
    echo"</ul>";
}



function principal_message ($CollegeID,$n)
{
    $link=mysql_query("select * from tbl_college where Id='$CollegeID'");
    $blog="blog.php?id=p";
    while ($r=mysql_fetch_array($link))
    {
        
    if (!$n)
    {
     $message="<p>".$r['Principal_Message']."</p>";
     
    }else
    {
     $message= substr($r['Principal_Message'],0,$n);
     //$read="<a href='principal_message'>Read more >>";
     $read= "</p><p align='right'><a class='btn' href='$blog'>Read more</a></p>";
     $message="<p>".$message.$read;
    }
      echo $message; 
    }
}
    
    
    
function entry_requirement ($n)
{
    $link=mysql_query("select * from tbl_cordinator where Id='$n'");

    while ($r=mysql_fetch_array($link))
    {
        

     $message="<p>".$r['EntryRequirement']."</p>";
     

      echo $message; 
    }
    
    
    
    
 
}    
 






function staff_biography ($id)
{

      
      ?>
            <div class="leadership-wrapper"><!-- leadership single wrap -->
                            	
                <?php
		 $image=staff_name($id,3);
                if(!$image)
                {
                    
                }else{
                ?>
                <figure class="leadership-photo">	
                   
                        <img src="<?php echo $image;?>" alt="" class="img-responsive pull-left" align="left" />
                 
                </figure>
                 <?php
                }
                ?>
                                    
                                    <div class="leadership-meta clearfix">
                                    	<h1 class="leadership-function title-median"><?php echo staff_name($id,1);?><small><?php staff_name($id,4);?></small></h1>
                                        <div class="leadership-position"><?php echo staff_name($id,4);?></div>
                                        <p class="leadership-bio">
<?php echo staff_name($id,2);?>
                                        </p>
                                    </div>
                                
                                </div>
      <?php
      
}



function school_biography ($id)
{
 
      ?>
      
      
                  <div class="leadership-wrapper"><!-- leadership single wrap -->
                            	
                <?php
		 $image=staff_name(school_name($id,3),3);
                if(!$image)
                {
                    
                }else{
                ?>
                <figure class="leadership-photo">	
                   
                        <img src="<?php echo $image;?>" alt="" class="img-responsive pull-left" align="left" />
                 
                </figure>
                 <?php
                }
                ?>
                                    
                                    <div class="leadership-meta clearfix">
                                    	<h1 class="leadership-function title-median"><?php echo school_name($id,1);?><small><?php staff_name(school_name($id,3),1);?></small></h1>
                                        <div class="leadership-position">Dean : <?php echo staff_name(school_name($id,3),1);?></div>
                                        <p class="leadership-bio">
<?php echo school_name($id,2);?>
                                        </p>
                                    </div>
                                
                                </div>
      
      
      
      
      <?php
      
      
      
      
      
      
}





function about_college ($id)
{
    $college=mysql_query("select * from tbl_college where Id='$id'");

  $r=mysql_fetch_array($college);
  $bio=$r['Biography'];
 echo $bio; 
}



function facilities_default ()
{
    $college=mysql_query("select * from tbl_article where Category='6' limit 0,1");

  $r=mysql_fetch_array($college);
  $bio=$r['Content'];
  $image= "images/articlePictures/".$r['Image'];
  $title=$r['Title'];
    $bio= substr($bio,0,100);
 ?>
 <p>
<img src="<?php echo $image;?>" alt="" class="aligncenter" />
<h1><?php echo $title; ?></h1>
<?php  echo $bio; ?>
</p>
<p>
<a href="facility.php" class="btn btn-success" title="button">&nbsp; See all</a>
</p>
<?php
}



function campuslife_default ()
{
    $college=mysql_query("select * from tbl_article where Category='7' limit 0,1");

  $r=mysql_fetch_array($college);
  $bio=$r['Content'];
  $image= "images/articlePictures/".$r['Image'];
  $title=$r['Title'];
  $bio= substr($bio,0,100);
 ?>
 <p>
<img src="<?php echo $image;?>" alt="" class="aligncenter" />
<h1><?php echo $title; ?></h1>
<?php  echo $bio; ?>
</p>
<p>
<a href="campuslife.php" class="btn btn-success" title="button">&nbsp; See all</a>
</p>
<?php
}


function our_students ()
{
    $college=mysql_query("select * from tbl_alumn limit 0,1");

  $r=mysql_fetch_array($college);
  $bio=$r['Biography'];
  $image= "alumn/".$r['Image'];
  $title=$r['Name'];
  $bio= substr($bio,0,100);
 ?>
 <p>
<img src="<?php echo $image;?>" alt="" class="aligncenter" width='250' height='90' />
<h1><?php echo $title; ?></h1>
<?php  echo $bio; ?>
</p>
<p>
<a href="alumn.php" class="btn btn-success" title="button">&nbsp; See all</a>
</p>
<?php
}



function other_facilities()
        {
            if(isset($_GET['id']))
            {
                $did=$_GET['id'];
                $article=mysql_query("select * from tbl_article where Category='6' and Id !='$did' order by Id DESC") or die (mysql_error());
                
            }else{
                
                 $article=mysql_query("select * from tbl_article where Category='6' order by Id DESC") or die (mysql_error());
            }
           echo "<table id='dataTables-example'>";
           echo"<thead><tr><td></td></tr></thead><tbody>";
		$w=1;
                while ( $r=mysql_fetch_array($article))
                {
                    $content=$r['Content'];
                    $n=500;
                    $content= substr($content,0,$n);
                    $title=$r['Title'];
                    $id=$r['Id'];
                    $blog="facility.php?id=".$id;
                    $link="<a class='btn btn-blog pull-right moretag' href='$blog'>Read more</a>";
                    $content= $content. "....". $link;
                    //$PostDate=$r['PostDate'];
                    $Category=$r['Category'];
                    $image=$r['Image'];
                    $image="images/articlePictures/".$image;
echo "<tr><td>";
?>    
                  <div class="leadership-wrapper">
		     <div class="clearfix">
		      <h1 class="title-median"><?php echo $title;?></h1>
                       <div class="news-meta">
                
                      <hr/>
		      </div>
                     </div> 
                                    <p>
                                    <img src="<?php echo $image;?>" alt="" class="alignleft" />
                                <?php echo $content;?>
                                    </p>
                        <div class="separator-dashed"></div>
                    </div>
             
               
                <?php 
                 $w++;
                 echo"</td></tr>";
                }
                ?>

		
              </tbody></table>
            <?php
        }




function current_facility($did)
        {

            $article=mysql_query("select * from tbl_article where Category='6' and Id='$did'") or die (mysql_error());
            $r=mysql_fetch_array($article);
            $content=$r['Content'];
            $title=$r['Title'];
            $date1=$r['PostDate'];
            $date = date("d-M-Y",$date1);
            $image=$r['Image'];
            $image="images/articlePictures/".$image;
            
         ?>   
		<div class="leadership-wrapper">    

		      <h1 class="title-median"><?php echo $title;?></h1>
                                    <p>
                                    <img src="<?php echo $image;?>" alt="" class="aligncenter" />
                                <?php echo $content;?>
                                    </p>
  </div>
                                            
        <?php
        }






function other_alumn()
        {
            if(isset($_GET['id']))
            {
                $did=$_GET['id'];
                $article=mysql_query("select * from tbl_alumn where Id !='$did' order by Id DESC") or die (mysql_error());
                
            }else{
                
                 $article=mysql_query("select * from tbl_alumn order by Id DESC") or die (mysql_error());
            }
           echo "<table id='dataTables-example'>";
           echo"<thead><tr><td></td></tr></thead><tbody>";
		$w=1;
                while ( $r=mysql_fetch_array($article))
                {
                    $content=$r['Biography'];
                    $n=500;
                    $content= substr($content,0,$n);
                    $title=$r['Name'];
                    $id=$r['Id'];
                    $blog="alumn.php?id=".$id;
                    $link="<a class='btn btn-blog pull-right moretag' href='$blog'>Read more</a>";
                    $content= $content. "....". $link;
                    //$PostDate=$r['PostDate'];
                    $Category=$r['Category'];
                    $image=$r['Image'];
                    $image="alumn/".$image;
echo "<tr><td>";
?>    
                  <div class="leadership-wrapper">
		     <div class="clearfix">
		      <h1 class="title-median"><?php echo $title;?></h1>
                       <div class="news-meta">
                
                      <hr/>
		      </div>
                     </div> 
                                    <p>
                                    <img src="<?php echo $image;?>" alt="" class="alignleft" />
                                <?php echo $content;?>
                                    </p>
                        
                    </div>
             
               <div class="separator-dashed"></div>
                <?php 
                 $w++;
                 echo"</td></tr>";
                }
                ?>

		
              </tbody></table>
            <?php
        }




function current_alumn($did)
        {

            $article=mysql_query("select * from tbl_alumn where Id='$did'") or die (mysql_error());
            $r=mysql_fetch_array($article);
            $content=$r['Biography'];
            $title=$r['Name'];
            $date1=$r['PostDate'];
            $date = date("d-M-Y",$date1);
            $image=$r['Image'];
            $image="alumn/".$image;
            $programme=$r['Programme'];
            
         ?>   
		<div class="leadership-wrapper">    

                                        
		      <h1 class="leadership-function title-median"><?php echo $title;?></small></h1>
                                       <div class="leadership-position"><?php echo $programme;?></div>
                                        
                                    <p class="leadership-bio">
                                    <img src="<?php echo $image;?>" alt="" class="alignleft" />
                                <?php echo $content;?>
                                    </p>
  </div>
                                            
        <?php
        }













function other_campuslife()
        {
            if(isset($_GET['id']))
            {
                $did=$_GET['id'];
                $article=mysql_query("select * from tbl_article where Category='7' and Id !='$did' order by Id DESC") or die (mysql_error());
                
            }else{
                
                 $article=mysql_query("select * from tbl_article where Category='7' order by Id DESC") or die (mysql_error());
            }
           echo "<table id='dataTables-example'>";
           echo"<thead><tr><td></td></tr></thead><tbody>";
		$w=1;
                while ( $r=mysql_fetch_array($article))
                {
                    $content=$r['Content'];
                    $n=500;
                    $content= substr($content,0,$n);
                    $title=$r['Title'];
                    $id=$r['Id'];
                    $blog="campuslife.php?id=".$id;
                    $link="<a class='btn btn-blog pull-right moretag' href='$blog'>Read more</a>";
                    $content= $content. "....". $link;
                    //$PostDate=$r['PostDate'];
                    $Category=$r['Category'];
                    $image=$r['Image'];
                    $image="images/articlePictures/".$image;
echo "<tr><td>";
?>    
                  <div class="leadership-wrapper">
		     <div class="clearfix">
		      <h1 class="title-median"><?php echo $title;?></h1>
                       <div class="news-meta">
                
                      <hr/>
		      </div>
                     </div> 
                                    <p>
                                    <img src="<?php echo $image;?>" alt="" class="alignleft" />
                                <?php echo $content;?>
                                    </p>
                        
                    </div>
             <div class="separator-dashed"></div>
               
                <?php 
                 $w++;
                 echo"</td></tr>";
                }
                ?>

		
              </tbody></table>
            <?php
        }




function current_campuslife($did)
        {

            $article=mysql_query("select * from tbl_article where Category='6' and  Id='$did'") or die (mysql_error());
            $r=mysql_fetch_array($article);
            $content=$r['Content'];
            $title=$r['Title'];
            $date1=$r['PostDate'];
            $date = date("d-M-Y",$date1);
            $image=$r['Image'];
            $image="images/articlePictures/".$image;
            
         ?>   
		<div class="leadership-wrapper">    

		      <h1 class="title-median"><?php echo $title;?></h1>
                                    <p>
                                    <img src="<?php echo $image;?>" alt="" class="aligncenter" />
                                <?php echo $content;?>
                                    </p>
  </div>
                                            
        <?php
        }









function cordinator_biography ($n)
{
    $link=mysql_query("select * from tbl_cordinator where Id='$n'");

  $r=mysql_fetch_array($link);

      $name=$r['Description']; //." ".$r['MiddleName'];
      echo "<h1 class='page-title'>$name</h1>";

     $message= $r['Biography'];
     //$read="<a href='principal_message'>Read more >>";
     //$read= "</p><p><a class='btn' href='#'>$read �</a></p>";
     $message="<p>".$message ."</p>";
    
      echo $message;
      
      if($n==1)
      {
      echo "<a href='docs/no_degree.pdf' target='_blank' class='btn btn-success'><i class='icon-cloud-download'></i> &nbsp; Download Forms</a></li>";
      
      }elseif($n==2){
        echo "<a href='docs/bachelor_degree.pdf' target='_blank' class='btn btn-success'><i class='icon-cloud-download'></i> &nbsp; Download Instructions</a></li>";
      }elseif($n==3){
        echo "<a href='docs/master_degree.pdf' target='_blank' class='btn btn-success'><i class='icon-cloud-download'></i> &nbsp; Download Forms</a></li>";
      }
}


function research_biography ($n)
{
    $link=mysql_query("select * from tbl_college where Id='$n'");

  $r=mysql_fetch_array($link);

      $name="Our Research Focus"; //." ".$r['MiddleName'];
      echo "<h1 class='page-title'>$name</h1>";

     $message= $r['Research_Biography'];
     //$read="<a href='principal_message'>Read more >>";
     //$read= "</p><p><a class='btn' href='#'>$read �</a></p>";
     $message="<p>".$message ."</p>";
    
      echo $message;   
}


function department_biography ($id)
{
?>
      
            
                  <div class="leadership-wrapper"><!-- leadership single wrap -->
                            	
                <?php
		 $image=staff_name(department_name($id,3),3);
                if(!$image)
                {
                    
                }else{
                ?>
                <figure class="leadership-photo">	
                   
                        <img src="<?php echo $image;?>" alt="" class="img-responsive pull-left" align="left" />
                 
                </figure>
                 <?php
                }
                ?>
                                    
                                    <div class="leadership-meta clearfix">
                                    	<h1 class="leadership-function title-median"><?php echo department_name($id,1);?><small><?php staff_name(department_name($id,3),1);?></small></h1>
                                        <div class="leadership-position">HOD : <?php echo staff_name(department_name($id,3),1);?></div>
                                        
                                        <p class="leadership-bio">
<?php echo department_name($id,2);?>
                                        </p>
                                    </div>
                                
                                </div>
      
      
      
      <?php
      
      
}



function program_biography ($n)
{
    $link=mysql_query("select * from tbl_program where Id='$n'");

  $r=mysql_fetch_array($link);

      $name=$r['Name']; //." ".$r['MiddleName'];
      echo "<h1 class='page-title'>$name</h1>";

     $message= $r['Description'];
     //$read="<a href='principal_message'>Read more >>";
     //$read= "</p><p><a class='btn' href='#'>$read �</a></p>";
     $message="<p>".$message ."</p>";
    
      echo $message;   
}



function theme_biography ($n)
{
    $link=mysql_query("select * from tbl_research_theme where Id='$n'");

  $r=mysql_fetch_array($link);

      $name=$r['Name']; //." ".$r['MiddleName'];
      echo "<h2 class='page-title'>$name</h2>";

     $message= $r['Biography'];
     //$read="<a href='principal_message'>Read more >>";
     //$read= "</p><p><a class='btn' href='#'>$read �</a></p>";
     $message="<p>".$message ."</p>";
    
      echo $message;   
}


function school_contacts ($n)
{
    $link=mysql_query("select * from tbl_school where Id='$n'");

    while ($r=mysql_fetch_array($link))
    {
        
$Email=$r['Email'];
$Phone=$r['MobileNo1'];   

      
      echo"<address>
        <abbr title='Email'>eMail: </abbr> $Email | <abbr title='Phone'>Mobile: </abbr> $Phone 
      </address>";

    }    
}




function staff_contacts ($n)
{
    $link=mysql_query("select * from tbl_staff where StaffId='$n'");

    while ($r=mysql_fetch_array($link))
    {
        
$Email=$r['Email'];
$Phone=$r['MobileNo1'];   

      
      echo"<address>
        <abbr title='Email'>eMail: </abbr> $Email | <abbr title='Phone'>Mobile: </abbr> $Phone 
      </address>";

    }    
}


function news ($n)
{
    $link=mysql_query("select * from tbl_article where Status=1 and Category='1' order by PostDate DESC limit 0, 2");
 echo "<div class='col-padded'>";
 
 ?>
<ul class="list-unstyled clear-margins"><!-- widgets -->                       
<li class="widget-container widget_recent_news"><!-- widgets list -->                    
<ul class="list-unstyled">
 
 <?php
    while ($r=mysql_fetch_array($link))
    {
        
        $id=$r['Id'];
        $link2="blog.php?id=".$id;
        
    if (!$n)
    {
     $message="<p>".$r['Conent']."</p>";
     
    }else
    {
     $message= substr($r['Content'],0,$n);
     $read="</br><a href='$link2' align='right moretag'>Read more</a>";
     $message=$message.$read;
     $title=$r['Title'];
     $PostDate=$r['PostDate'];

    }
    ?>
    <li class="recent-news-wrap">
    <h1 class="title-median">
        <a href="<?php echo $link2; ?>" title=""><?php echo $title; ?></a>
    </h1>
                                        
    <div class="recent-news-meta">
    <div class="recent-news-date">Posted <?php echo   $PostDate;?></div>
    </div>
    
    
       <?php
     $image=$r['Image'];
     
       if($image=='article.jpg'|| $image=="")
       {
        echo"<div class='recent-news-content clearfix'><div>";
         
       echo $message;
       echo "</div></div>";
       }else{
        echo "<div class='recent-news-content clearfix'>";
        $image="images/articlePictures/".$image;
        
        ?>
        <div class="recent-news-content clearfix">
    <figure class="recent-news-thumb">
    <a href="#" title="">
    <img src="<?php echo $image;?>"
    class="attachment-thumbnail wp-post-image" alt="" /></a>
    </figure>
        <div class="recent-news-text">
         
        <?php echo $message;?>
   </div>
         </div>
        <?php
       }
    ?>

   
    </li>
      <?php
    }
    echo "</ul>";
    echo "</li></ul>";
    echo "</div>";
}




function display_picture ($img)
{
   $img="principal.jpg";
   $pic= "<img class='imgl' width='120' height='150' src='employeePicture/".$img."'>";
   
   return $pic;
    
}


function announcement ()
{
    $link=mysql_query("select * from tbl_article where Category='2' and Status=1 order by Id DESC limit 0, 3");
    echo "<div class='col-padded col-shaded'>";
    echo"<ul class='list-unstyled clear-margins'>";
    echo "<li class='widget-container widget_up_events' >";
         echo"<ul class='list-unstyled'>";           
    while ($r=mysql_fetch_array($link))
    {
        $id=$r['Id'];
     $url="press.php?id=".$id;
     
     $name=$r['Content'];
     $name=substr($r['Content'],0,70);
     $title=$r['Title'];
     //$format=$r['Format'];
     
     $PostDate=$r['PostDate'];
     $blog="press.php?id=".$id;

     $l="<a href='$url'>pdf</a>";
     if(!$title)
     {
    }else{
?>

                                
<li class="up-event-wrap">   
<a href="<?php echo $blog;?>">
<h1 class="title-median"><?php echo $title;?></h1>
</a>
<?php //echo $name;?>

<div class="up-event-meta clearfix">
<div class="up-event-date">Posted <?php echo $PostDate;?></div>
</div>
<p>
 <?php echo $name;?>
</p>
</li>
<?php
     }
}
echo "</ul></li></ul>";
echo "</div>";
 
}









########## CIVE WEBSITE FUNCTIONS ########







function studyLevel($id=0)
{
	$sqlp = "SELECT id, levelName FROM tbl_study_level";
	$resultp = dbQuery($sqlp);
	?>
	<select name='Level' id='Level'>
		<option value=''>:Select:</option>
		<?php
			while($prows = dbFetchAssoc($resultp)){
				if($prows['id'] == $id){
					$active = "selected='selected'";
				}else
					$active = '';
		?>
		<option value='<? echo $prows['id']; ?>' <?php echo $active; ?>><? echo $prows['levelName']; ?></option>
		<?php
		}
		?>
	</select>
	<?php
}




function studyLevel_name($l)
{
	switch($l)
	{
		case 0: $level="Certificate"; break;
		case 1: $level="Diploma"; break;
		case 2: $level="Bachelor"; break;
		case 3: $level="Postgraduate"; break;
		default : $level="Degree"; break;
	}
	
return $level;
}




























function theme_list()
{
    $sem=mysql_query("select * from tbl_research_theme");

echo "<table class='table table-condensed table-striped table-bordered' width='100%'>
    <thead>
        <tr>
            <th>#</th><th>Name</th><th></th>
        </tr>        
    </thead><tbody>";
    $k=1;
        while ($y=mysql_fetch_array($sem))
        {
        $Name=$y['Name'];
        $profile="<a href='theme_profile.php?p=".$y['Id']."'>View Profile </a>";
        echo"<tr>
            <td>
                $k 
            </td>
            <td>
                $Name
            </td>
            <td>
                $profile
            </td>
            
        </tr>";
        $k++;
        }
    echo"</tbody>";
echo"</table>";
           

	}
        
        
function research_group_list()
{
    $sem=mysql_query("select * from tbl_research_group");

echo "<table class='table table-condensed table-striped table-bordered' width='100%'>
    <thead>
        <tr>
            <th>#</th><th>Name</th><th></th>
        </tr>        
    </thead><tbody>";
    $k=1;
        while ($y=mysql_fetch_array($sem))
        {
        $Name=$y['Name'];
        $profile="<a href='theme_profile.php?p=".$y['Id']."'>View Profile </a>";
        echo"<tr>
            <td>
                $k 
            </td>
            <td>
                $Name
            </td>
            <td>
                $profile
            </td>
            
        </tr>";
        $k++;
        }
    echo"</tbody>";
echo"</table>";
           

	}

        
        
function working_group($n)
{
    $sem=mysql_query("select * from tbl_working_group where ThemeID='$n'");

echo "<table class='table table-condensed table-striped table-bordered' width='100%'>
    <thead>
        <tr>
            <th>#</th><th>Name</th><th>Description</th>
        </tr>        
    </thead><tbody>";
    $k=1;
        while ($y=mysql_fetch_array($sem))
        {
        $Name=$y['Name'];
        $profile= $y['Description'];  //"<a href='theme_profile.php?p=".$y['Id']."'>View Profile </a>";
        echo"<tr>
            <td>
                $k 
            </td>
            <td>
                $Name
            </td>
            <td>
                $profile
            </td>
            
        </tr>";
        $k++;
        }
    echo"</tbody>";
echo"</table>";
           

}
 
        
        





function cord_programe_list($sem1)
{

    
    $sem=mysql_query("select * from tbl_program p, tbl_study_level s where s.id=p.StudyLevel and
		     s.levelName='$sem1'");

echo "<table class='table table-condensed table-striped table-bordered' width='100%'>
    <thead>
        <tr>
            <th>#</th><th>Name</th><th></th>
        </tr>        
    </thead><tbody>";
    $k=1;
        while ($y=mysql_fetch_array($sem))
        {
        $Name=$y['Name'];
        $profile="<a href='programme_profile.php?p=".$y['Id']."'>View Profile </a>";
        echo"<tr>
            <td>
                $k 
            </td>
            <td>
                $Name
            </td>
            <td>
                $profile
            </td>
            
        </tr>";
        $k++;
        }
    echo"</tbody>";
echo"</table>";
           

	}
        








function department_list_pub($sem1)
{
    //$sem=mysql_query("select * from tbl_program where DepartmentID='$sem1' order by StudyLevel");

	$data=mysql_query("select d.Id as dId, s.Name as sName, d.Name as dName , d.Description as dDescription from tbl_school s,
                          tbl_department d where s.ID=d.SchoolID and
                          s.Id ='$sem1' order by dName ASC") or die (mysql_error());
	
echo "<table class='table table-condensed table-striped table-bordered' width='100%'>
    <thead>
        <tr>
            <th>#</th><th>Name</th><th></th>
        </tr>        
    </thead><tbody>";
    $k=1;
        while ($y=mysql_fetch_array($data))
        {
        $Name=$y['dName'];
        $profile="<a href='department_profile.php?d=".$y['dId']."'>View Profile </a>";
        echo"<tr>
            <td>
                $k 
            </td>
            <td>
                $Name
            </td>
            <td>
                $profile
            </td>
            
        </tr>";
        $k++;
        }
    echo"</tbody>";
echo"</table>";
           

	}











function publication_list_all($r)
{
	
	global $self;
	
	$pub1=mysql_query("select MIN(Year) as Mn from tbl_publication");
        $Mn=mysql_fetch_array($pub1);
        $Mn=$Mn['Mn'];
        
        $pub2=mysql_query("select MAX(Year) as Mx from tbl_publication");
        //$Mx=mysql_fetch_array($pub2);
        //$Mx=$Mx['Mx'];
        $Mx=date('Y');
        $YearDiff=$Mx-$Mn;
        //$r=$YearDiff;
    ?>
           
            			<div class="tabbable tab-pane" id="tabs-783341">
				<ul class="nav nav-tabs">
				<?php
                                $M=$Mn;
                                $k=$M;
                                
				while($M<=$Mx)
				{
				$yd=$M;
                                $c=$yd+$r;
                               $cy=date('Y');
                                if($c>$cy)
                                 {
                                    $c=$cy;
                                    $class1="<li class='active'>";
                                    $class2="tab-pane active";
                                }else
                                {
                                    $class1="<li>";
                                    $class2="tab-pane";
                                }
                           
                           
                                ?>
					<?php echo $class1?> 
						<a href="#panel-<?php echo $yd; ?>" data-toggle="tab">
						  <?php echo $yd ." - ". $c; ?>
						</a>
					</li>
				<?php
                                $M=$yd+$r+1;
				}
				?>	
				</ul>
				<div class="tab-content">
				<?php
				
                                $N=$Mn;
				$w=$Mn;
                                
                                while($N<=$Mx)
				{
                                    $yk=$N;
                                    $x=$yk;
                                    $y=$yk+$r;
                                $cy=date('Y');
                                if($y>$cy)
                                 {
                                    $y=$cy;
                                 }
				?>
				<div class="<?php echo $class2;?>" id="panel-<?php echo $x; ?>">
				<?php pub_list($x,$y);?>
				</div>
				
				<?php
                                 $N=$yk+$r+1;
				}
				?>
				</div>
			</div>
 
           
           
           
           <?php
           
            
            
            
        }
        
        


function pub_list($x,$y)
{
    $cy=date('Y');
    if($y>$cy)
    {
        $y=$cy;
    }
    
$data=mysql_query("select * from tbl_publication p, tbl_staff s where s.StaffId=p.StaffID and p.Year between '$x' and '$y' order by p.Year DESC") or die (mysql_error());

$nums=mysql_num_rows($data);

if($nums>0)
{
    //$v=0;
    echo "<ol>";
        while ($r=mysql_fetch_array($data))
	{
		
		$Title=$r['Title'];
		$Publisher=$r['Publisher'];
		$Year=$r['Year'];
		$Id=$r['Id'];
		$Author=$r['CAuthor'];
		$Page=$r['Page'];
		$Volume=$r['Volume'];
		$Issue=$r['Issue'];
		$Author=$r['CAuthor'];
		$Name=$r['lastName'].",".$r['firstName'];
		
		$Author = "<b>".$Name."</b>,".$Author;
            //$v++;
            
                
	   ?>
              <li>
			<p class="note"><?php echo $Author;?>,(<?php echo $Year;?>) <?php echo $Title ;?>,<i><?php echo $Publisher;?></i>(<?php echo $Volume;?>), <?php echo $Page;?></p>
            
            </li>
	<?php	
	}
        echo "</ul>";
    
}
else{
    
    echo "No publication in this range of years";
}

}






 










function programe_course($P,$y)
{
	
	
	$data=mysql_query("select * from tbl_curiculum  where ProgramID='$P' and  YearOfStudy ='$y'") or die (mysql_error());
	while ($r=mysql_fetch_array($data))
	{
		
		$sem1=$r['Semister'];
		$mwaka=$r['YearOfStudy'];
	   
           
$sem=mysql_query("select * from tbl_course c, tbl_curiculum s where s.CourseID=c.Id and Semister='$sem1'");

echo "Year :". $mwaka . " Semister : ".$sem1;

echo "<table class='table table-condensed table-striped table-bordered' width='100%'>
    <thead>
        <tr>
            <th>Code</th><th>Name</th><th>Units</th>
        </tr>        
    </thead><tbody>";
        while ($y=mysql_fetch_array($sem))
        {
        $Code=$y['Name'];
        $Name=$y['Description'];
        $Unit=$y['Unit'];
        echo"<tr>
            <td>
                $Code 
            </td>
            <td>
                $Name
            </td>
            <td>
                $Unit
            </td>
            
        </tr>";
        }
    echo"</tbody>";
echo"</table>";
           

	}
	
}



function staff_list($sem1)
{
    $sem=mysql_query("select * from tbl_staff where DepartmentID='$sem1' order by LastName");

echo "<table class='table table-condensed table-striped table-bordered' width='100%'>
    <thead>
        <tr>
            <th>#</th><th>Name</th><th></th>
        </tr>        
    </thead><tbody>";
    $k=1;
        while ($y=mysql_fetch_array($sem))
        {
        $Name=$y['FirstName'] . $y['LastName'];
        $profile="<a href='staff_profile.php?s=".$y['Id']."'>View Profile </a>";
        echo"<tr>
            <td>
                $k 
            </td>
            <td>
                $Name
            </td>
            <td>
                $profile
            </td>
            
        </tr>";
        $k++;
        }
    echo"</tbody>";
echo"</table>";
           

	}
    

    
    





 function course_name ($CourseID, $k)
 {
    $course=mysql_query("select * from tbl_course where Id='$CourseID'");
    $r=mysql_fetch_array($course);
    switch ($k)
    {
    case 1 : $x= $r['Name']; break;
    case 2 : $x= $r['Unit']; break;
    default : $x= $r['Name']; break;
    }   
 return $x;
 }


	function application_widget()
        {
         //list_of_departments();
         ?>
         <li class="widget-container widget_nav_menu"><!-- widget -->

		      <h1 class="title-widget">Apply for our programmes</h1>

			<ul class="list-unstyled">
                            <li>
                                <a href="degree.php?d=1">Non- degree</a>  
                                
                            <li>
                                <a href="degree.php?d=2" >degree</a>                 
                            <li>
                                <a href="degree.php?d=3" >Postgraduate</a>                 
                        </ul>
         </li>
        <?
        }
        
        
        function sidebar()
        {
            ?>
                                <div class="col-padded col-shaded"><!-- inner custom column -->
                    
                        
                        <ul class="list-unstyled clear-margins"><!-- widgets -->
        <li class="widget-container widget_nav_menu">
        <?php
	search_form();
	?>
        </li>
                        	
				
		<li class="widget-container widget_nav_menu"><!-- widget -->
                    
                <h1 class="title-widget">Academic Units</h1>				
				
		<?php
		list_of_departments();
		?>
                </li>
				

                    			<?php
application_widget();
 ?>
 		        	<li class="widget-container widget_recent_news"><!-- widgets list -->

 <?php
student_projects();


?>
               
                            </li><!-- widgets list end -->
                            
                            <li class="widget-container widget_sofa_twitter"><!-- widget -->
                            <?php //student_story();?>
                            	
                            
                            </li><!-- widget end -->
                            
                        </ul><!-- widgets end -->
                    
                    </div><!-- inner custom column end -->
            
            
            <?php
            
        }
        
        
        
        
        
        
    function list_of_departments()
        {
           $school=mysql_query("select * from tbl_school");
        //echo "<h1 class='title-widget'>Academic Departments</h1>";
           
           echo "<ul class='list-unstyled'>";
           while ($sc=mysql_fetch_array($school))
           {
                $s=$sc['Id'];
                $sName=$sc['Description'];
                //$Name=$r['Name'];
                $Id=$sc['Id'];
                $link="school_profile.php?s=".$Id;
                $link="<a href='".$link."'>".$sName."</a>";
                echo "<li>".$link."</li>";
                
                $dept=mysql_query("select * from tbl_department where SchoolID='$s'");
                
                while ($r=mysql_fetch_array($dept))
                {
                $Name=$r['Name'];
                $Id=$r['Id'];
                $link="department_profile.php?d=".$Id;
                $link="<a href='".$link."'>".$Name."</a>";
                //echo "<li>".$link."</li>";
                }
             
           }
           echo "</ul>";
        }
        
        
        
        
        function student_story()
        {
         ?>   
		  <div class="panel panel-default">
		      <div class="panel-heading">
		      <h1 class="panel-title">Our Students story</h1>
		      </div>
		      <div class="panel-body">
			
                        Grayson julius - "I am proud to be CIVE graduate"
                        
                        
		      </div>
		  </div>
        <?
        }
        
        
        
        
        function student_projects()
        {
         ?>
         <!--
		  <div class="panel panel-default">
		      <div class="panel-heading">
		      <h1 class="panel-title">Our Students Projects</h1>
		      </div>
		      <div class="panel-body">
			-->
                <?php
                        
    $article=mysql_query("select * from tbl_fypgroup order by PostDate DESC limit 0,2") or die (mysql_error());
 //$article=array_rand( $article);
                while ( $r=mysql_fetch_array($article))
                {
                    
                    $content=$r['description'];
                    $n=100;
                    $content= substr($content,0,$n);
                    $title=$r['prjTitle'];
                    $id=$r['id'];
                    $blog="student_projects.php?d=".$id;
                    $link="<a class='btn btn-blog pull-right moretag' href='$blog'>See more</a></br><hr>";
                    $content= $content .".....".$link;
                    //echo "<h6>$title</h6>";
                    //echo $content;
                  }      
                ?>
                        
                        
                    <!--    
                        
		      </div>
		  </div>
                  -->
                  
                  
                  
                            <div>
                                    
                                    <a href="student_projects.php" class="custom-button cb-gray" title="">
                                        <i class="custom-button-icon fa  fa-graduation-cap"></i>
                                        
                                        <span class="custom-button-wrap">
                                            <span class="custom-button-title">Student Projects</span>
                                            <span class="custom-button-tagline">
                                                Final Year Students Projects
                                            
                                            </span>
                                        </span>
                                        <em></em>
                                    </a>
                                </div>
                  
                  
                  
        <?
        }
        
        
      
function search_form()
{
?>
<form action="search_engine.php" name='Search' method='POST'>
<input type='text' name='SearchItem' class='search'>
<button type='submit' name='Search' class='btn btn-primary'>
<i class='fa fa-search'></i>Search</button>
</form>
<?php
}  
        
        
    function current_article($did)
        {
            if($did=='p')
            {
                
            $article=mysql_query("select * from tbl_college order by PostDate DESC") or die (mysql_error());
            $r=mysql_fetch_array($article);
            $content= $r['Principal_Message'];
            $title="Principal Message";
            $date1=$r['PostDate'];
            $date = date("d-M-Y",$date1);
	    
	    
             $image=staff_name(1,3);   
                
                
            }else{
            $article=mysql_query("select * from tbl_article where Id='$did'") or die (mysql_error());
            $r=mysql_fetch_array($article);
            $content=$r['Content'];
            //$content=htmlspecialchars ($content);
            $title=$r['Title'];
            $date1=$r['PostDate'];
            $date = date("d-M-Y",$date1);
            $image=$r['Image'];
	    
	    $File1=$r['File1'];
	    $File2=$r['File2'];
	    $File3=$r['File3'];
	    //Attachments
		    if($File1)
		    {
			$url="images/articleFiles/".$File1;
			$link1="<a href='$url' target='_blank'>| Download Attachment 1</a>";
		    }else{
			$link1="";
		    }
		    
		    if($File2)
		    {
			$url="images/articleFiles/".$File2;
			$link2="<a href='$url' target='_blank'>| Download Attachment 2</a>";
		    }else{
			$link2="";
		    }
		    
		    if($File3)
		    {
			$url="images/articleFiles/".$File3;
			$link3="<a href='$url' target='_blank'>| Download Attachment 3</a>";
		    }else{
			$link3="";
		    }
		    
	    
	    
            
                                if($image=="article.jpg")
                {
                    $image="";
                }else{
                    $image="images/articlePictures/".$image;
                }
            
            }
         ?>   
		<div class="leadership-wrapper">
                
                <?php
                if(!$image)
                {
                    
                }else{
                ?>
                <!--
                <figure class="leadership-photo">	
                   
                        <img src="<?php echo $image;?>" alt="" class="img-responsive aligncenter" align="left" />
                 
                </figure>
                -->
                 <?php
                }
                ?>     
                
		     <div class="clearfix">
		      <h1 class="title-median"><?php echo $title;?></h1>
                      
                       <div class="news-meta">
                         <span class="news-meta-category">Posted <?php echo $date1;?></span>
                        <hr/>
		      </div>
                     </div> 
   
                                    
                                    <p>
                                    <img src="<?php echo $image;?>" alt="" class="alignleft" />
                                <?php echo $content;?>
                                    
                                    </p>
                             
                     		    <p>
					<?php
					
					echo $link1. " ".$link2. " ".$link3;
					?>
				    </p>
  </div>
                                            <div class="separator-dashed"></div>
        <?
        }
        
       
     function other_article()
        {
            if(isset($_GET['id']))
            {
                $did=$_GET['id'];
                $article=mysql_query("select * from tbl_article where Category='1' and Id !='$did' order by PostDate DESC") or die (mysql_error());
                
            }else{
                
                 $article=mysql_query("select * from tbl_article where Category='1' order by PostDate DESC") or die (mysql_error());
            }
           echo "<table id='dataTables-example'>";
           echo"<thead><tr><td></td></tr></thead><tbody>";
		$w=1;
                while ( $r=mysql_fetch_array($article))
                {
                    $content=$r['Content'];
                    $n=500;
                    $content= substr($content,0,$n);
                    $title=$r['Title'];
                    $id=$r['Id'];
                    $blog="blog.php?id=".$id;
                    $link="<a class='btn btn-blog pull-right moretag' href='$blog'>Read more</a>";
                    $content= $content. "....". $link;
                    $PostDate=$r['PostDate'];
                    $Category=$r['Category'];
		    
                    $image=$r['Image'];
                    
		    
		    
		    if($image=="article.jpg")
                {
                    $image="";
                }else{
                    $image="images/articlePictures/".$image;
                }
echo "<tr><td>";
                if(!$image)
                {
                    
                }else{
                ?>
                <!--
                <figure class="leadership-photo">	
                   
                        <img src="<?php echo $image;?>" alt="" class="img-responsive" />
                 
                </figure>
                                    -->
                 <?php
                }
                ?>     
                  <div class="leadership-wrapper">
		     <div class="clearfix">
		      <h1 class="title-median"><?php echo $title;?></h1>
                       <div class="news-meta">
                      <span class="news-meta-category">Posted <?php echo $PostDate;?></span>
                      <hr/>
		      </div>
                     </div> 
                                    <p>
                                        <?php
                                                        if(!$image )
                {
                    
                }else{
                                        
                                        ?>
                                    <img src="<?php echo $image;?>" alt="" class="alignleft" />
                                    
                                <?php } echo $content;?>
                                    </p>

                        <div class="separator-dashed"></div>
                    </div>
             
               
                <?php 
                 $w++;
                 echo"</td></tr>";
                }
                ?>

		
              </tbody></table>
            <?php
        }
        
	
	
	
	
	
	
	

        
       
       
       
       
       function current_project($did)
        {

            $article=mysql_query("select * from tbl_fypgroup where id='$did'") or die (mysql_error());
            $r=mysql_fetch_array($article);
            $content=$r['description'];
            $title=$r['prjTitle'];
            $date1=$r['PostDate'];
            $date = date("d-M-Y",$date1);
            $image=$r['Image'];
            $image="article/".$image;
            $members=$r['members'];
            
         ?>   
		<div class="news">
                
     
                
		     <div class="clearfix">
		      <h1 class="title-median"><?php echo $title;?></h1>
                       <div class="news-meta">
                         <span class="news-meta-category">Posted <?php echo $date1;?></span>
                        <hr/>
		      </div>
                     </div> 

			<p>
                   <?php echo $content;?>
                        
                        
		      </p>
                </div>
                
                			<div class="tabbable" id="tabs-7833413">
				<ul class="nav nav-tabs">
					<li>
						<a href="#panel-268554" data-toggle="tab">Members</a>
					</li>

				</ul>
				<div class="tab-content">
					<div class="tab-pane" id="panel-268554">
<?php echo "".$members; ?>
					</div>

				</div>
			</div>
                
                
        <?
        }
        
       
       
       
       
        
        
        
        
             function other_project()
        {
            if(isset($_GET['d']))
            {
                $did=$_GET['d'];
                $article=mysql_query("select * from tbl_fypgroup where id !='$did' order by PostDate DESC") or die (mysql_error());
                
            }else{
                
                 $article=mysql_query("select * from tbl_fypgroup order by PostDate DESC") or die (mysql_error());
            }
                      echo "<table id='dataTables-example'>";
           echo"<thead><tr><td></td></tr></thead><tbody>";
     
        ?>
                
                        
        	
	
        
        
        
        <?php
		$w=1;
                while ( $r=mysql_fetch_array($article))
                {
                    $content=$r['description'];
                    $n=100;
                    $content= substr($content,0,$n);
                    $title=$r['prjTitle'];
                    $id=$r['id'];
                    $blog="student_projects.php?d=".$id;
                    $link="<a class='btn btn-blog pull-right moretag' href='$blog'>Read more</a>";
                    $content= $content. "....". $link;
                    $PostDate=$r['PostDate'];
                    //$Category=$r['Category'];
                    //$image=$r['Image'];
                    //$image="article/".$image
                    echo "<tr><td>";
                ?>
           
                    
            
                
   
                  <div class="news col-padded col-shaded">
		     <div class="clearfix">
		      <h1 class="leadership-function title-median"><?php echo $title;?></h1>
                       <div class="news-meta">
                      <span class="news-meta-category">Posted <?php echo $PostDate;?></span>
                      <hr/>
		      </div>
                     </div> 
                    
                        <p>	
                    <?php
                        echo $content;
                        //echo $link;
                     ?>   
                    </p>
                        
                    </div>
              
               
                <?php 
                 $w++;
                    echo"</td></tr>";
                }
                ?>

		</tbody></table>
              
            <?php
        }
        
        
        
        
        
        



function current_press($did)
        {
    
            $article=mysql_query("select * from tbl_article where Category='2' and Id='$did'") or die (mysql_error());
            $r=mysql_fetch_array($article);
            $content=$r['Content'];
            $title=$r['Title'];
            $date1=$r['PostDate'];
            $date = date("d-M-Y",$date1);
            	    $File1=$r['File1'];
	    $File2=$r['File2'];
	    $File3=$r['File3'];
	    //Attachments
		    if($File1)
		    {
			$url="images/articleFiles/".$File1;
			$link1="<a href='$url' target='_blank'>| Download Attachment 1</a>";
		    }else{
			$link1="";
		    }
		    
		    if($File2)
		    {
			$url="images/articleFiles/".$File2;
			$link2="<a href='$url' target='_blank'>| Download Attachment 2</a>";
		    }else{
			$link2="";
		    }
		    
		    if($File3)
		    {
			$url="images/articleFiles/".$File3;
			$link3="<a href='$url' target='_blank'>| Download Attachment 3</a>";
		    }else{
			$link3="";
		    }
		    
            //$link="<a href='document/$url'>Download Attachment</a>";
         ?>   
		 <article>
                
                    <div class="clearfix">
		      <h1 class="title-median"><?php echo $title;?></h1>
                       <div class="news-meta">
                         <span class="news-meta-category">Posted <?php echo $date1;?></span>
                        <hr/>
		      </div>
                     </div>

		      <div class="category-description">
		      <p>
                   <?php echo $content;?>
		      </p>
		  
		                    <p class='btn btn-blog pull-right'>
					<?php
					
					echo $link1. " ".$link2. " ".$link3;
					?>
				    </p>
		      </div>
                   
		  </article>
        <?php
        }
        
       
     function other_press()
        {
            if(isset($_GET['id']))
            {
                $did=$_GET['id'];
                $article=mysql_query("select * from tbl_article where Category='2' and Id !='$did' order by PostDate DESC") or die (mysql_error());
                
            }else{
                
                 $article=mysql_query("select * from tbl_article where Category='2' order by PostDate DESC") or die (mysql_error());
            }
           
            echo "<table id='dataTables-example'>";
           echo"<thead><tr><td></td></tr></thead><tbody>";
        ?>
                
                    	
        <?php
		$w=1;
                while ( $r=mysql_fetch_array($article))
                {
                    $content=$r['Content'];
                    $n=100;
                    $content= substr($content,0,$n);
                    $title=$r['Title'];
                    $id=$r['Id'];
                    $blog="press.php?id=".$id;
                    $link="<a class='btn btn-blog pull-right moretag' href='$blog'>Read more</a>";
                    $content= $content. "....". $link;
                    $date1=$r['PostDate'];
                    echo "<tr><td>";
                ?>
		
           	    <article>
                    <div class="clearfix">
		      <h1 class="title-median"><?php echo $title;?></h1>
                       <div class="news-meta">
                         <span class="news-meta-category">Posted <?php echo $date1;?></span>
                        <hr/>
		      </div>
                     </div>
  		      <div class="category-description">
                   <?php echo $content;?>
                   <hr/>
		      </div>
		  </article>
                <?php 
                 $w++;
                 echo"</td></tr>";
                }
                echo "</tbody></table>";
        }




function application_procedure($did)
{
    $link=mysql_query("select * from tbl_cordinator where Id='$did'");

    while ($r=mysql_fetch_array($link))
    {
        

     $message="<p>".$r['Application']."</p>";
     

      echo $message; 
    }
    
    
}



function fee_struture($did)
{
    $link=mysql_query("select * from tbl_cordinator where Id='$did'");

    while ($r=mysql_fetch_array($link))
    {
        

     $message="<p>".$r['Fee_structure']."</p>";
     

      //echo $message;
    $content=$r['Fee_structure'];
    
      echo htmlspecialchars($content);
    }
    
    
}




function staff_name($id,$k)
{
$name=mysql_query("select * from tbl_staff where StaffId='$id'");
$r=mysql_fetch_array($name);
$FirstName=$r['firstName'];
$LastName=$r['lastName'];
$MiddleName=$r['middleName'];
$design=getDesignation($r['Designation']);

$name= $design.". ".$LastName.", ".$FirstName." ".$MiddleName;

$name="<a href='index.php?q=staff_profile&sid=".$id."' title='Open Profile'>".$name."</a>";
$bio=$r['Biography'];
$pic="employeePics/".$r['Picture'];
$Position =$r['Position'];

switch ($k)
{
    case 1: $x=$name;break;
    case 2: $x=$bio; break;
    case 3: $x=$pic; break;
    case 4: $x=$Position; break;
    default : $x=$name;break;    
    
}

return $x;
    
}




function school_name($id,$k)
{
$name=mysql_query("select * from tbl_school where Id='$id'");
$r=mysql_fetch_array($name);
$Name=$r['Name'];
$LongName=$r['Description'];
$Dean=$r['Dean'];


$name= $LongName;

$bio=$r['Biography'];
$Contacts = $r['Email']. "| ".$r['MobileNo'];

switch ($k)
{
    case 1: $x=$name;break;
    case 2: $x=$bio; break;
    case 3: $x=$Dean; break;
    case 4: $x=$Contacts; break;
    default : $x=$name;break;    
    
}

return $x;
    
}



function department_id($name)
{
$x=mysql_query("select * from tbl_department where Name='$name'");
$r=mysql_fetch_array($x);
$Id=$r['Id'];
return $Id;
}



function department_name($id,$k)
{
$name=mysql_query("select * from tbl_department where Id='$id'");
$r=mysql_fetch_array($name);
$Name=$r['Name'];
$LongName=$r['Description'];
$Dean=$r['HOD'];


$name= $LongName;

$bio=$r['Biography'];
//$Contacts =$r['Email']. "| ".$r['Mobile'];

switch ($k)
{
    case 1: $x=$name;break;
    case 2: $x=$bio; break;
    case 3: $x=$Dean; break;
    //case 4: $x=$Contacts; break;
    default : $x=$name;break;    
    
}

return $x;
    
}


function retrieve_courseid($id)  
{
$name=mysql_query("select * from tbl_course where Name='$id'");
$r=mysql_fetch_array($name);
$CourseID=$r['Id'];
return $CourseID;
}


function retrieve_programid($id)
{
$name=mysql_query("select * from tbl_program where acronym='$id'");
$r=mysql_fetch_array($name);
$ProgramID=$r['Id'];
return $ProgramID;
}


function campus_tour ()
{
    ?>
    <div>
     
        <div class="video-container">
<iframe width="420" height="315" src="//www.youtube.com/embed/jEx7f3tU7oA" frameborder="0" allowfullscreen></iframe>
        </div>
    </div>
    
    <?php
    
}



function staffAvailability($x)
{
	$staff=mysql_query("select * from tbl_staff where staffId='$x'") or die (mysql_error());
	$r=mysql_fetch_array($staff);
	$Status=$r['Status'];
	if($Status==1)
	{
		$Status_value="Available";
	}else{
		
	$Status_value="On Study Leave";	
	}
	$n=mysql_num_rows($staff);
	
	echo "<select name='Status'>";
	
	
		echo "<option value='$Status' selected>$Status_value</option>";
		
	
		
	echo "<option value='1'>Available</option>";
	echo "<option value='0'>On Study Leave</option>";
		
	
	echo "</select>";
	
	
	
}



function department_x($x)
{
	$staff=mysql_query("select * from tbl_department") or die (mysql_error());
	
	echo "<select name='DepartmentID'>";
	
	while ($r=mysql_fetch_array($staff))
	{
		
	$Status=$r['DepartmentID'];
	//echo $Status ."XXXX";
	
	if($x==$r['Id'])
	{
		
		echo "<option value='$r[Id]' selected>$r[Name] </option>";
	}else{
		
		echo "<option value='$r[Id]'>$r[Name]</option>";
		
	}
	}
		
	echo "</select>";
	
	
	
}



function add_leader()
{
    $self=$_SERVER['PHP_SELF'];
    $Position=$_POST['Position'];
    $StaffID=$_POST['StaffID'];
    
    echo $Position;
?>
    <form method="POST" action="<?php echo $self;?>">
        <table>
            <tr><td>Staff </td>
                <td><?php staff(); ?> </td>
            </tr>
            <tr><td>Position </td>
                <td><?php leader_position(); ?> </td>
            </tr>
            <tr><td></td>
                <td>
                    <button name="save" type="submit" class="btn btn-success">Save</button>
                </td>
            </tr>
        </table>
    </form>
    
<?php
}



function leader_position()
{
	$leader= array ("Principal","Dean","HOD");
	echo "<select name='Position'><option>--Select Position--</option>";
	for($c=0;$c<=2;$c++)
	{
	$ctr=mysql_fetch_array($ct);
	echo "<option value='$leader[$c]'>$leader[$c]</option>";

	}
	echo "</select>";
}


function study_duration($id=0)
{

	echo "<select name='Duration'><option>--Study Duration--</option>";
	for($c=1;$c<=4;$c++)
	{
		($c==$id)? $active="selected='selected'" : $active='';
		echo "<option value='$c' $active >$c Year</option>";
	}
	echo "</select>";
}





function article_pic($id)
{

$name=$id;

 $Picture=$name;
 $file = "article";

$fileData = pathinfo(basename($_FILES["photo"]["name"]));

$Picture=$name . '.' . $fileData['extension'];
foreach($_FILES as $file_name => $file_array) 
 {
        

	 $Type=$fileData['extension'];
	 if(($Type=="jpeg")||($Type=="gif")||($Type=="bmp")||($Type=="jpg")||($Type=="png"))
	 {
         move_uploaded_file($file_array['tmp_name'],"$file/$Picture") or die ("Couldn't copy");
		 $upd=mysql_query("update tbl_article set Image='$Picture'  where  Id='$id'");
         //echo "Picture Successfuly Uploaded<br><br>";
	 }else
	 {
	 echo"<p class='message'>Image type Not Supported</p>";
	 }

 }
 

 
 ?>
<!--
<form enctype='multipart/form-data' method='POST' action='upload.php' >
        <table>
<tr><td>
<input type=hidden name=MAX_FILE_SIZE value=100050000>
Please choose photo: </td><td><input type='file' name='photo'><br>
</td></tr><tr><td>
</td><td><input type='submit' name='picha' value='upload'>

</td></tr>
</table>
</form>
-->
<?php
}








function course_unit()
{

echo "<select name='Unit'><option>--Course Credits--</option>";
$units=array(7, 7.5,10,15);
for($c=0;$c<=3;$c++)
{
echo "<option value='$units[$c]'>$units[$c] Credits</option>";

}
echo "</select>";
}


function semester()
{

echo "<select name='Semester'><option>--Semester--</option>";
$units=array("xx","First Semester","Second Semester","Third");
for($c=1;$c<=3;$c++)
{
echo "<option value='$c'>$units[$c]</option>";

}
echo "</select>";
}


function core_courses()
{

echo "<select name='Core'><option value='1'>Core</option>";
echo "<option value='0'>Optional</option>";
echo "</select>";
}





function yearOfStudy()
{

echo "<select name='YearOfStudy'><option>--Year Of Study--</option>";
$units=array("xx","First Year","Second Year","Third Year","Fourth Year");
for($c=1;$c<=5;$c++)
{
echo "<option value='$c'>$units[$c]</option>";

}
echo "</select>";
}


function programe()
{
	$ct=mysql_query("SELECT * FROM tbl_program") or die (mysql_error());
	$num=mysql_num_rows($ct);
	echo "<select name='ProgramID' id='Id'><option>--Select Program--</option>";
	for($c=1;$c<=$num;$c++)
	{
	$ctr=mysql_fetch_array($ct);
	echo "<option value='$ctr[Id]'>$ctr[Name]</option>";
	}
	echo "</select>";
}


function staff_position_select($id,$disabled)
{
	$post = mysql_query("SELECT * FROM tbl_staff_position");
	
	echo"<select name='Position' $disabled>";
	while ($r=mysql_fetch_array($post))
	{
		$position=$r['position'];
		$id_db=$r['id'];
		if(($id_db == $id) )
		{
			echo "<option value='$id_db' selected>$position</option>";
		}elseif(($id_db == $id)){
			echo "<option value='$id_db' selected>$position</option>";
		}else{
			
			echo "<option value='$id_db' >$position</option>";
		}
		
	}		
}


function staff_designation_select($id,$disabled)
{
	$post = mysql_query("SELECT * FROM tbl_designation");
	echo"<select name='Designation' $disabled>";
	while ($r=mysql_fetch_array($post))
	{
		$position=$r['designation'];
		$id_db=$r['id'];
		if(($id_db == $id))
		{
			echo "<option value='$id_db' selected>$position</option>";
		}elseif(($id_db == $id)){
			echo "<option value='$id_db' selected disabled>$position</option>";
		}else{
			
			echo "<option value='$id_db' >$position</option>";
		}
		
	}		
}




function getPosition($id){
	$sqlp = "SELECT position FROM tbl_staff_position WHERE id='$id'";
	$resultp = dbQuery($sqlp);
	$row = dbFetchAssoc($resultp);
	return $row['position'];
}

function getDesignation($id){
	$sql = "SELECT id, designation FROM tbl_designation WHERE id='$id'";
	$resultp = dbQuery($sql);
	$row = dbFetchAssoc($resultp);
	return $row['designation'];
}

//get current semester
function getCurrentSemester(){
	$sql = "SELECT semester FROM tbl_academic_year WHERE status='1'";
	$resultp = dbQuery($sql);
	$row = dbFetchAssoc($resultp);
	return $row['semester'];
}

//get staff Position ID
function getStaffPositionID($staffId){
	$sql = "SELECT Position FROM tbl_staff WHERE staffId='$staffId'";
	$resultp = dbQuery($sql);
	$row = dbFetchAssoc($resultp);
	return $row['Position'];
}
//get max course for a staff;
function getMaxCourseForStaff($staffId,$isChief){
	$position = getStaffPositionID($staffId);
	$sql = "SELECT semester FROM tbl_staff_position WHERE status='1'";
	$resultp = dbQuery($sql);
	$row = dbFetchAssoc($resultp);
	return $row['semester'];
}

//get work load settings;
function getworkloadSetting($name){
	$sql = "SELECT status FROM tbl_workload_setting WHERE name='$name'";
	$resultp = dbQuery($sql);
	$row = dbFetchAssoc($resultp);
	return $row['status'];
}

//get current academic year
function getCurrentAcademic(){
	$sql = "SELECT id FROM tbl_academic_year WHERE status='1'";
	$resultp = dbQuery($sql);
	$row = dbFetchAssoc($resultp);
	return $row['id'];
}

//Check if staff can choose a course
function checkIfCanChooseACourse($staffId,$academic,$isChief){
	$check      = dbQuery("SELECT id FROM tbl_course_selection WHERE staffID='$staffId' AND academicID='$academic' AND status='$isChief'");
	$selectedCourses = dbNumRows($check);
	$allowExtra = getworkloadSetting("ALLOWEXTRAWORKLOAD");
	$position   = getStaffPositionID($staffId);
	if($isChief == '1') { //course for chief instructor
		if($allowExtra == '1'){
			$sqlmax      = "SELECT maxCourse FROM tbl_work_policy WHERE positionID='$position'";
			$resultmax   = dbQuery($sqlmax);
			$rowmax      = dbFetchAssoc($resultmax);
			$maxCourse   = $rowmax['maxCourse'];
			if($selectedCourses < $maxCourse)
				return true;
			else
				return false;
		}else{
			if($position == '1'){
				//check of assist
				$checkAssist = dbQuery("SELECT id FROM tbl_course_selection WHERE staffID='$staffId' AND academicID='$academic' AND status='2'");
				$numAssist = dbNumRows($checkAssist);
				$sql       = "SELECT minCourse FROM tbl_work_policy WHERE positionID='$position' AND assCourse='$numAssist'";
				$resultp   = dbQuery($sql);
				$row       = dbFetchAssoc($resultp);
				$minCourse = $row['minCourse'];
				if($selectedCourses < $minCourse)
					return true;
				else
					return false;
			}else{
				$sqlnotP    = "SELECT minCourse FROM tbl_work_policy WHERE positionID='$position'";
				$resultnotP = dbQuery($sqlnotP);
				$rownotP    = dbFetchAssoc($resultnotP);
				$minCourse = $rownotP['minCourse'];
				if($selectedCourses < $minCourse)
					return true;
				else
					return false;
			}
		}
	}else{
		return false;
	}
	
}

//Check if the course selected
function isCourseSelected($course,$academic,$iscore){
	$sql = "SELECT s.staffId as staffId,s.firstName as fName FROM tbl_course_selection cs, tbl_staff s 
			WHERE s.staffId=cs.staffID AND cs.courseID='$course' AND cs.academicID='$academic' AND cs.status='$iscore'";
	$resultp = dbQuery($sql);
	$m=0;$name="";
	if(dbNumRows($resultp )>0){
		
		while($row = dbFetchAssoc($resultp)){
			if($m == 0){
				$name[0] = $row['staffId'];
				$name[1] = $row['fName'];
			}else{
				$name[0] .= ", " .$row['staffId'];
				$name[1] .= ", " . $row['fName'];
			}
			$m++;
		}
		return $name;
	}else{
		return $name;
	}
}

//Store the errors when uploading student list
function errorExcelFile($errorArray)
{
	$filename = "listOfRejectedRecords.xls";
	$file = "temp/$filename";
	$file1 = "../temp/$filename";
	$fp=fopen($file1,"w");
	$data = "#" . "\t" .  "Course" . "\t" . "Programme" ."\t" . "Comments" ."\n"; 
	fwrite($fp,$data);	
	$countArr = count($errorArray);
	for($i=0; $i<$countArr; $i++)
	{
		$data = ($i+1) . "\t" . $errorArray[$i]['CourseID'] . "\t" . $errorArray[$i]['ProgramID']. "\t" . $errorArray[$i]['Comment'] ."\n";
		fwrite($fp,$data);
	}
	
	fclose($fp);
	
	return $file;
}

function errorExcelFileCourse($errorArray)
{
	$filename = "listOfRejectedCourses.xls";
	$file = "temp/$filename";
	$file1 = "../temp/$filename";
	$fp=fopen($file1,"w");
	$data = "#" . "\t" .  "Name" . "\t" . "Description" ."\t" . "Comments" ."\n"; 
	fwrite($fp,$data);	
	$countArr = count($errorArray);
	for($i=0; $i<$countArr; $i++)
	{
		$data = ($i+1) . "\t" . $errorArray[$i]['Name'] . "\t" . $errorArray[$i]['Description']. "\t" . $errorArray[$i]['Comment'] ."\n";
		fwrite($fp,$data);
	}
	
	fclose($fp);
	
	return $file;
}


///uchakaramu mpya
function fyp_student()
{
$ct=mysql_query("SELECT * FROM tbl_fyp_student") or die (mysql_error());
$num=mysql_num_rows($ct);
echo "<select name='StudentID' id='StudentID'><option>--Select Student--</option>";
for($c=1;$c<=$num;$c++)
{
$ctr=mysql_fetch_array($ct);
echo "<option value='$ctr[Id]'>$ctr[Name] ($ctr[RegNo])</option>";
}
echo "</select>";
}


function fyp_student_name($id,$k)
{
$name=mysql_query("select * from tbl_fyp_student where Id='$id'");
$r=mysql_fetch_array($name);
$FirstName=$r['Name'];
$RegNo=$r['RegNo'];


$name= $FirstName;


switch ($k)
{
    case 1: $x=$name;break;
    case 2: $x=$RegNo; break;
    default : $x=$name;break;    
    
}

return $x;
    
}


	      function alumn_pic($sd){
		$p=mysql_query("select * from tbl_alumn where Id='$sd'");
		$r=mysql_fetch_array($p);
		$pics=$r['Image'];
		
		if(!$pics)
		{
		$pics="default.jpg";
		}
		
		return $pics;
		
	      }




?>
        



