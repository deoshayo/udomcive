
window.setTimeout(function () { $('#mydiv').hide(); }, 500);
			
//Paging Functions
function getXMLHTTP() { //fuction to return the xml http object
	var xmlhttp=false;	
	try{
		xmlhttp=new XMLHttpRequest();
	}
	catch(e)	{		
		try{			
			xmlhttp= new ActiveXObject("Microsoft.XMLHTTP");
		}
		catch(e){
			try{
				xmlhttp = new ActiveXObject("Msxml2.XMLHTTP");
			}
			catch(e1){
				xmlhttp=false;
			}
		}
	}
		 	
	return xmlhttp;
}

function onflyload(form,target,id,Div) {
	
	var strURL="onflychange.php?target=" + target + "&id=" + id;
	
	var req = getXMLHTTP();	
	
	if (req) {
		
		req.onreadystatechange = function() {
			if (req.readyState == 4) {
					// only if "OK"
					if (req.status == 200) {						
						document.getElementById(Div).innerHTML=req.responseText;						
					} else {
						alert("There was a problem while loading the page:\n" + req.statusText);
					}
				}				
			}			
			req.open("GET", strURL, true);
			req.send(null);
		}
	
}

function onflyloadStaff(form,target,Div,dep) {
	var req = getXMLHTTP();	
	var s = document.getElementById("accountDepartment");
	
	var sValue = s.options[s.selectedIndex].value; 
	
	var strURL="includes/onflychange.php?target=" + target +"&dep=" + dep + "&id=" + sValue;
	
	if (req) {
		req.onreadystatechange = function() {
			if (req.readyState == 4) {
					// only if "OK"
					if (req.status == 200) {						
						document.getElementById(Div).innerHTML=req.responseText;						
					} else {
						alert("There was a problem while loading the page:\n" + req.statusText);
					}
				}				
			}			
			req.open("GET", strURL, true);
			req.send(null);
		}
	
}

function getAvailablePrivilege(form,target) {
	var c = document.getElementById("cboUserId");
	var cValue = c.options[c.selectedIndex].value;

	var strURL="includes/onflychange.php?target=" + target + "&id=" + cValue;
	var req = getXMLHTTP();
	document.getElementById("ProcessAppldiv").innerHTML="<span class='loading' align='center'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span>";
	
	if (req) {
		
		req.onreadystatechange = function() {
			if (req.readyState == 4) {
					// only if "OK"
					if (req.status == 200) {						
						document.getElementById('ProcessAppldiv').innerHTML=req.responseText;						
					} else {
						alert("There was a problem while loading the page:\n" + req.statusText);
					}
				}				
			}			
			req.open("GET", strURL, true);
			req.send(null);
		}
}

function assignPrivilege(form,target,Div,modId,action) {

	var c       = document.getElementById("cboUserId");
	var cValue  = c.options[c.selectedIndex].value;
	var actionV = document.getElementById(action).value;
	if(actionV == 1)
		document.getElementById(action).value = 0;
	else
		document.getElementById(action).value = 1;

	var strURL="includes/onflychange.php?target=" + target + "&groupId=" + cValue + "&modId=" + modId + "&action=" + actionV;
	var req = getXMLHTTP();
	document.getElementById(Div).innerHTML="<span class='loading' align='center'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span>";
	
	if (req) {
		
		req.onreadystatechange = function() {
			if (req.readyState == 4) {
					// only if "OK"
					if (req.status == 200) {						
						document.getElementById(Div).innerHTML=req.responseText;						
					} else {
						alert("There was a problem while loading the page:\n" + req.statusText);
					}
				}				
			}			
			req.open("GET", strURL, true);
			req.send(null);
		}
	
}

//function to upload the file

function fileUpload(form, action_url, div_id) {
    // Create the iframe...
    var iframe = document.createElement("iframe");
    iframe.setAttribute("id", "upload_iframe");
    iframe.setAttribute("name", "upload_iframe");
    iframe.setAttribute("width", "0");
    iframe.setAttribute("height", "0");
    iframe.setAttribute("border", "0");
    iframe.setAttribute("style", "width: 0; height: 0; border: none;");
 
    // Add to document...
    form.parentNode.appendChild(iframe);
    window.frames['upload_iframe'].name = "upload_iframe";
 
    iframeId = document.getElementById("upload_iframe");
 
    // Add event...
    var eventHandler = function () {
 
            if (iframeId.detachEvent) iframeId.detachEvent("onload", eventHandler);
            else iframeId.removeEventListener("load", eventHandler, false);
 
            // Message from server...
            if (iframeId.contentDocument) {
                content = iframeId.contentDocument.body.innerHTML;
            } else if (iframeId.contentWindow) {
                content = iframeId.contentWindow.document.body.innerHTML;
            } else if (iframeId.document) {
                content = iframeId.document.body.innerHTML;
            }
 
            document.getElementById(div_id).innerHTML = content;
 
            // Del the iframe...
            setTimeout('iframeId.parentNode.removeChild(iframeId)', 250);
        }
 
    if (iframeId.addEventListener) iframeId.addEventListener("load", eventHandler, true);
    if (iframeId.attachEvent) iframeId.attachEvent("onload", eventHandler);
 
    // Set properties of form...
    form.setAttribute("target", "upload_iframe");
    form.setAttribute("action", action_url);
    form.setAttribute("method", "post");
    form.setAttribute("enctype", "multipart/form-data");
    form.setAttribute("encoding", "multipart/form-data");
 
    // Submit the form...
    form.submit();
 
    document.getElementById(div_id).innerHTML = "<span style='color:blue;'>Uploading, Please wait &nbsp;&nbsp;&nbsp;</span><span class='loading' align='center'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span>";
}

/*
	Course Selection
*/

function courseSelection(form,target,Div,courseId,academic,staffId,isChief, action,checkName) {
	
	var actionV = document.getElementById(action).value;
	if(actionV == 1)
		document.getElementById(action).value = 0;
	else
		document.getElementById(action).value = 1;
		
	var strURL="includes/onflychange.php?target=" + target + "&courseId=" + courseId + "&academic=" + academic + "&staffId=" + staffId + "&isChief=" + isChief + "&action=" + actionV + "&checkDiv=" + Div + "&checkName=" + checkName + "&checkHidden=" + action;
	var req = getXMLHTTP();
	document.getElementById(Div).innerHTML="<span class='loading' align='center'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span>";
	
	if (req) {
		
		req.onreadystatechange = function() {
			if (req.readyState == 4) {
					// only if "OK"
					if (req.status == 200) {						
						document.getElementById(Div).innerHTML=req.responseText;						
					} else {
						alert("There was a problem while loading the page:\n" + req.statusText);
					}
				}				
			}			
			req.open("GET", strURL, true);
			req.send(null);
		}
	
}

//uncheck the box
function jsuncheckCheckbox(name){
	document.getElementById(name).checked = false;
}