<?php
/**
 *NJIWA WEB APPLICATION FRAMEWORK 2013
 * DEVELOPED BY DEO SHAO
 */
 
require_once 'library/config.php';

require_once 'library/functions.php';


checkUser();

require_once 'includes/pageengine.php';
require_once 'includes/lang.php';

require_once loadtemplate();

dbClose($dbConn);
?>