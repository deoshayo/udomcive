<?php
if(isset($_GET['s']))
{
  $id=$_GET['s'];
  
}
?>

    	<div class="container"><!-- container -->
        
        	<div class="row"><!-- row -->
            
                <div id="k-top-search" class="col-lg-12 clearfix"><!-- top search -->
                

                </div><!-- top search end -->
            
            	<div class="k-breadcrumbs col-lg-12 clearfix"><!-- breadcrumbs -->
                

                    
                </div><!-- breadcrumbs end -->               
                
            </div><!-- row end -->
            
            <div class="row no-gutter"><!-- row -->
                
                <div class="col-lg-8 col-md-8"><!-- doc body wrapper -->
                	
                    <div class="col-padded"><!-- inner custom column -->
                    
                    	<div class="row gutter"><!-- row -->
                        
                        	<div class="col-lg-12 col-md-12">

<?php staff_biography ($id);?>

			<p>
  <?php staff_contacts ($id);?>
                            </div>
                        
                        </div><!-- row end -->
                    
                    	<div class="row gutter"><!-- row -->
                        
                        	<div class="col-lg-12 col-md-12">
                                

			<div class="tabbable" id="tabs-783341">
				<ul class="nav nav-tabs">
					<li>
						<a href="#panel-26855" data-toggle="tab">Profession</a>
					</li>
					<li class="active">
						<a href="#panel-669580" data-toggle="tab">Publications</a>
					</li>
				</ul>
				<div class="tab-content">
					<div class="tab-pane" id="panel-26855">
<?php academic_profile($id);?>
					</div>
					<div class="tab-pane active" id="panel-669580">
<?php publication_list($id); ?>
					</div>
				</div>
			</div>




                            </div>
                        
                        </div><!-- row end --> 
                        
                        <div class="row gutter"><!-- row -->
                        
                        	<div class="col-lg-12">
                        
   
                            
                            </div>
                            
                        </div><!-- row end -->                 
                    
                    </div><!-- inner custom column end -->
                    
                </div><!-- doc body wrapper end -->
                
                <div id="k-sidebar" class="col-lg-4 col-md-4"><!-- sidebar wrapper -->
                	
<?php sidebar(); ?>
                    
                </div><!-- sidebar wrapper end -->
            
            </div><!-- row end -->
        
        </div><!-- container end -->
    
