<?php
require_once 'config.php';



/*
Load template
*/

function loadtemplate()
{
	$rowtmp='cive';
	$temp = 'templates/' . $rowtmp. '/index.php'; 
	return $temp;

}







//menu
function top_menu ()
{
?>

                                        <!-- Home -->
		<ul class="nav navbar-nav">		
                    <li class="dropdown active">
                        <a href="javascript:void(0);" class="dropdown-toggle" data-toggle="dropdown">
                          <?php echo portal_label ("Home"); ?>
                        </a>
                        <ul class="dropdown-menu">
                            <li class="active"><a href="index.php"><?php echo portal_label ("AboutUs"); ?></a></li>
                         
                            
                        </ul>
                    </li>
                    <!-- End Home -->

                    <!-- Pages -->                        
                    <li class="dropdown">
                        <a href="javascript:void(0);" class="dropdown-toggle" data-toggle="dropdown">
                         <?php echo portal_label ("Service"); ?>
                        </a>
                        <ul class="dropdown-menu">
 
                                    <li><a href="index.php?q=services"><?php echo portal_label ("Overview"); ?></a></li>
                                    <li><a href="index.php?q=verification"><?php echo portal_label ("Verification"); ?></a></li>
                                    <li><a href="index.php?q=facility"><?php echo portal_label ("FacilitySearch"); ?></a></li>
                                    <li><a href="index.php?q=analytics"><?php echo portal_label ("DataAnalytics"); ?></a></li>
				    
				    <li><a href="index.php?q=service_profile&action=4"><?php echo portal_label ("MedicalDrugProfile"); ?></a></li>
                     
			</ul>
                    </li>
                    <!-- End Pages -->

                    <!-- Blog -->
                    <li class="dropdown">
                        <a href="javascript:void(0);" class="dropdown-toggle" data-toggle="dropdown">
                         <?php echo portal_label ("Blog"); ?>
                        </a>
                        <ul class="dropdown-menu">
 
                                    <li><a href="index.php?q=blog"><?php echo portal_label ("FeaturedNews"); ?></a></li>
                                    
                        </ul>
                    </li>
                    <!-- End Blog -->


                    <!-- Contact -->
                    <li class="dropdown">
                        <a href="javascript:void(0);" class="dropdown-toggle" data-toggle="dropdown">
                          <?php echo portal_label ("ReportIssue"); ?>
                        </a>
                        <ul class="dropdown-menu">
 
                                    <li><a href="index.php?q=contact"><?php echo portal_label ("ReportIssue"); ?></a></li>
                                    
                        </ul>
                    </li>
                    <!-- End Contact -->


 
                    <!-- Verify -->
                    <li><a href="index.php?q=verification"><?php echo portal_label ("Verify"); ?></a></li>
                    <!-- End Verify -->

					
		    
		      <!-- Search Block -->
		    
		    <li>
                        <i class="search fa fa-search search-btn"></i>
                        <div class="search-open">
				<form action="index.php?q=facility&gosearch=1" name='Search' method='POST'>
                            <div class="input-group animated fadeInDown">
                                <input type="text" class="form-control" name='SearchItem'  placeholder="<?php echo portal_label ("Search"); ?>">
                                <span class="input-group-btn">
                                    <button class="btn-u" type="submit" name="search"><?php echo portal_label ("Search"); ?></button>
                                </span>
                            </div>
				</form>
                        </div>    
                    </li>
		   </ul> 
		       <!-- End Search Block -->
    
    

<?php    
}

 


//function to query web content by section
function portal_label ($Section)
{
   $lang=$_SESSION['lang_session'];
    
    $h_content=mysql_query("select * from tbl_portal_label where LabelName='$Section' and Status='1'") or die (mysql_error());
    
    $content_data=mysql_fetch_array($h_content);
    $Title_sw=$content_data['Title_sw'];
    $Title_en=$content_data['Title_en'];
    if($lang==1)
    {
        $contents= $Title_en;
    return $contents;
    }else{
         $contents= $Title_sw;
    return $contents;
    }
}



function home_content ($Section)
{
   $lang=$_SESSION['lang_session'];
    
    $h_content=mysql_query("select * from tbl_portal_content where SectionName='$Section' and Status='1'") or die (mysql_error());
    
    $content_data=mysql_fetch_array($h_content);
    $Title_sw=$content_data['Title_sw'];
    $Title_en=$content_data['Title_en'];
    $content_sw=$content_data['Content_sw'];
    $content_en=$content_data['Content_en'];
    if($lang==1)
    {
        $contents= "<h3>".$Title_en."</h3><p>".$content_en."</p>";
    return $contents;
    }else{
         $contents= "<h3>".$Title_sw."</h3><p>".$content_sw."</p>";
    return $contents;
    }
}


//function to query web content by section
function dynamic_content ($Section,$k)
{
    $lang=$_SESSION['lang_session'];
    
    $h_content=mysql_query("select * from tbl_portal_content where SectionName='$Section' and Status='1'") or die (mysql_error());
    
    $content_data=mysql_fetch_array($h_content);
    $Title_sw=$content_data['Title_sw'];
    $Title_en=$content_data['Title_en'];
    $content_sw=$content_data['Content_sw'];
    $content_en=$content_data['Content_en'];
    
    if($lang==1)
    {
        $contents     = "<h3>".$Title_en."</h3><p>".$content_en."</p>";
        $title        =$Title_en;
        $content_data =$content_en;
    }else
    {
        $contents     = "<h3>".$Title_sw."</h3><p>".$content_sw."</p>";
        $title        =$Title_sw;
        $content_data =$content_sw;
    }
    
    //decide parameter
    switch ($k)
    {
        case 1 : $x=$title; break;
        case 2 : $x=$content_data; break;
        default : $x=$contents; break;
    }
    
    //$x=htmlspecialchars($x,ENT_QUOTES);
   
   return $x; 
}




function dynamic_content_less($Section,$k,$n)
{
    $lang=$_SESSION['lang_session'];
    
    $h_content=mysql_query("select * from tbl_portal_content where SectionName='$Section' and Status='1'") or die (mysql_error());
    
    $content_data=mysql_fetch_array($h_content);
    $Title_sw=$content_data['Title_sw'];
    $Title_en=$content_data['Title_en'];
    $content_sw=$content_data['Content_sw'];
    $content_en=$content_data['Content_en'];
    $id=$content_data['Id'];
    
    if($lang==1)
    {
        $contents     = "<h3>".$Title_en."</h3><p>".$content_en."</p>";
        $title        =$Title_en;
        $content_data =$content_en;
    }else
    {
        $contents     = "<h3>".$Title_sw."</h3><p>".$content_sw."</p>";
        $title        =$Title_sw;
        $content_data =$content_sw;
    }
    
    //decide parameter
    switch ($k)
    {
        case 1 : $x=$title; break;
        case 2 : $x=$content_data; break;
        default : $x=$contents; break;
    }
    
    //$x=htmlspecialchars($x,ENT_QUOTES);
  $x= substr($x,0,$n);
    $link2="index.php?q=blog_item&action=".$id;
        
    $read="</br><a href='$link2' align='right moretag' class='pull-right'>Read more</a>";
    $x=$x."....".$read;
   return $x; 
}



function announcement ()
{
    $link=mysql_query("select * from tbl_article where Category='2' and Status=1 order by Id DESC limit 0, 2");
    echo "<div class='col-padded col-shaded'>";
    echo"<ul class='list-unstyled clear-margins'>";
    echo "<li class='widget-container widget_up_events' >";
         echo"<ul class='list-unstyled'>";           
    while ($r=mysql_fetch_array($link))
    {
        $id=$r['Id'];
     $url="index.php?q=blog_item&id=".$id;
     
     $name=$r['Content'];
     $name=substr($r['Content'],0,70);
     $title=$r['Title'];
     //$format=$r['Format'];
     
     $PostDate=$r['PostDate'];
     $blog="index.php?q=blog_item&id=".$id;

     $l="<a href='$url'>pdf</a>";
     if(!$title)
     {
    }else{
?>

                                
<li class="up-event-wrap">   
<a href="<?php echo $blog;?>">
<h1 class="title-median"><?php echo $title;?></h1>
</a>
<?php //echo $name;?>

<div class="up-event-meta clearfix">
<div class="up-event-date">Posted <?php echo $PostDate;?></div>
</div>
<p>
 <?php echo $name;?>
</p>
</li>
<?php
     }
}
echo "</ul></li></ul>";
echo "</div>";
 
}


function news ($n)
{
    $link=mysql_query("select * from tbl_article where Status=1 order by PostDate DESC limit 0, 3");
 echo "<div class='col-padded'>";
 
 ?>
<ul class="list-unstyled clear-margins"><!-- widgets -->                       
<li class="widget-container widget_recent_news"><!-- widgets list -->                    
<ul class="list-unstyled">
 
 <?php
    while ($r=mysql_fetch_array($link))
    {
        
        $id=$r['Id'];
        $link2="index.php?q=blog_item&id=".$id;
        
    if (!$n)
    {
     $message="<p>".$r['Content']."</p>";
     
    }else
    {
     $message= substr($r['Content'],0,$n);
     $read="</br><a href='$link2' align='right moretag'>Read more</a>";
     $message=$message.$read;
     $title=$r['Title'];
     $PostDate=$r['PostDate'];

    }
    ?>
    <li class="recent-news-wrap">
    <h1 class="title-median">
        <a href="<?php echo $link2; ?>" title=""><?php echo $title; ?></a>
    </h1>
                                        
    <div class="recent-news-meta">
    <div class="recent-news-date">Posted <?php echo   $PostDate;?></div>
    </div>
    
    
       <?php
     $image=$r['Image'];
     
       if($image=='article.jpg'|| $image=="")
       {
        echo"<div class='recent-news-content clearfix'><div>";
         
       echo $message;
       echo "</div></div>";
       }else{
        echo "<div class='recent-news-content clearfix'>";
        $image="images/articlePictures/".$image;
        
        ?>
        <div class="recent-news-content clearfix">
    <figure class="recent-news-thumb">
    <a href="#" title="">
    <img src="<?php echo $image;?>"
    class="attachment-thumbnail wp-post-image" alt="" /></a>
    </figure>
        <div class="recent-news-text">
         
        <?php echo $message;?>
   </div>
         </div>
        <?php
       }
    ?>

   
    </li>
      <?php
    }
    echo "</ul>";
    echo "</li></ul>";
    echo "</div>";
}



function sidebar()
        {
            ?>
                                <div class="col-padded col-shaded"><!-- inner custom column -->
                    
                        
                        <ul class="list-unstyled clear-margins"><!-- widgets -->
        <li class="widget-container widget_nav_menu">
        <?php
	search_form();
	?>
        </li>
                        	
				
		<li class="widget-container widget_nav_menu"><!-- widget -->
                    
                <h1 class="title-widget">Academic Units</h1>				
				
		<?php
		list_of_departments();
		?>
                </li>
				

                    			<?php
application_widget();
 ?>
 		        	<li class="widget-container widget_recent_news"><!-- widgets list -->

 <?php
student_projects();


?>
               
                            </li><!-- widgets list end -->
                            
                            <li class="widget-container widget_sofa_twitter"><!-- widget -->
                            <?php //student_story();?>
                            	
                            
                            </li><!-- widget end -->
                            
                        </ul><!-- widgets end -->
                    
                    </div><!-- inner custom column end -->
            
            
            <?php
            
        }

function search_form()
{
?>
<form action="index.php?q=search_engine" name='Search' method='POST'>
<input type='text' name='SearchItem' class='search'>
<button type='submit' name='Search' class='btn btn-primary'>
<i class='fa fa-search'></i>Search</button>
</form>
<?php
}



    function list_of_departments()
        {
           $school=mysql_query("select * from tbl_school")or die (mysql_error());
        //echo "<h1 class='title-widget'>Academic Departments</h1>";
           
           echo "<ul class='list-unstyled'>";
           while ($sc=mysql_fetch_array($school))
           {
                $s=$sc['Id'];
                $sName=$sc['Description'];
                //$Name=$r['Name'];
                $Id=$sc['Id'];
                $link="index.php?q=school_profile&s=".$Id;
                $link="<a href='".$link."'>".$sName."</a>";
                echo "<li>".$link."</li>";
                
                $dept=mysql_query("select * from tbl_department where SchoolID='$s'");
                
                while ($r=mysql_fetch_array($dept))
                {
                $Name=$r['Name'];
                $Id=$r['Id'];
                $link="index.php?q=department_profile&d=".$Id;
                $link="<a href='".$link."'>".$Name."</a>";
                //echo "<li>".$link."</li>";
                }
             
           }
           echo "</ul>";
        }

	
	
	function application_procedure($did)
{
    $link=mysql_query("select * from tbl_cordinator where Id='$did'");

    while ($r=mysql_fetch_array($link))
    {
        

     $message="<p>".$r['Application']."</p>";
     

      echo $message; 
    }
    
    
}



function fee_struture($did)
{
    $link=mysql_query("select * from tbl_cordinator where Id='$did'");

    while ($r=mysql_fetch_array($link))
    {
        

     $message="<p>".$r['Fee_structure']."</p>";
     

      //echo $message;
    $content=$r['Fee_structure'];
    
      echo htmlspecialchars($content);
    }
    
    
}


function quick_links ()
{
    $link=mysql_query("select * from tbl_link where Category='Quick' and Status=1");
     echo "<ul class='list-unstyled'>";
    while ($r=mysql_fetch_array($link))
    {
     $url=$r['Url'];
     $name=$r['Name'];
      echo"<li><a href='$url' target='_blank'>$name</a></li>"; 
    }
    echo"</ul>";
}


function campus_tour ()
{
    ?>
    <div>
     
        <div class="video-container">
<iframe width="420" height="315" src="//www.youtube.com/embed/jEx7f3tU7oA" frameborder="0" allowfullscreen></iframe>
        </div>
    </div>
    
    <?php
    
}



function staff_name($id,$k)
{
$name=mysql_query("select * from tbl_staff where StaffId='$id'");
$r=mysql_fetch_array($name);
$FirstName=$r['firstName'];
$LastName=$r['lastName'];
$MiddleName=$r['middleName'];
$design=$r['Designation'];

$name= $design.". ".$LastName.", ".$FirstName." ".$MiddleName;

$name="<a href='index.php?q=staff_profile&s=".$id."' title='Open Profile'>".$name."</a>";
$bio=$r['Biography'];
$pic="employeePics/".$r['Picture'];
$Position =$r['Position'];

switch ($k)
{
    case 1: $x=$name;break;
    case 2: $x=$bio; break;
    case 3: $x=$pic; break;
    case 4: $x=$Position; break;
    default : $x=$name;break;    
    
}

return $x;
    
}




function school_name($id,$k)
{
$name=mysql_query("select * from tbl_school where Id='$id'");
$r=mysql_fetch_array($name);
$Name=$r['Name'];
$LongName=$r['Description'];
$Dean=$r['Dean'];


$name= $LongName;

$bio=$r['Biography'];
$Contacts = $r['Email']. "| ".$r['MobileNo'];

switch ($k)
{
    case 1: $x=$name;break;
    case 2: $x=$bio; break;
    case 3: $x=$Dean; break;
    case 4: $x=$Contacts; break;
    default : $x=$name;break;    
    
}

return $x;
    
}





function department_name($id,$k)
{
$name=mysql_query("select * from tbl_department where Id='$id'");
$r=mysql_fetch_array($name);
$Name=$r['Name'];
$LongName=$r['Description'];
$Dean=$r['HOD'];


$name= $LongName;

$bio=$r['Biography'];
//$Contacts =$r['Email']. "| ".$r['Mobile'];

switch ($k)
{
    case 1: $x=$name;break;
    case 2: $x=$bio; break;
    case 3: $x=$Dean; break;
    //case 4: $x=$Contacts; break;
    default : $x=$name;break;    
    
}

return $x;
    
}




	function application_widget()
        {
         //list_of_departments();
         ?>
         <li class="widget-container widget_nav_menu"><!-- widget -->

		      <h1 class="title-widget">Apply for our programmes</h1>

			<ul class="list-unstyled">
                            <li>
                                <a href="index.php?q=degree&d=1">Non- degree</a>  
                                
                            <li>
                                <a href="index.php?q=degree&d=2" >degree</a>                 
                            <li>
                                <a href="index.php?q=degree&d=3" >Postgraduate</a>                 
                        </ul>
         </li>
        <?
        }
	
	
	
	
	 function student_story()
        {
         ?>   
		  <div class="panel panel-default">
		      <div class="panel-heading">
		      <h1 class="panel-title">Our Students story</h1>
		      </div>
		      <div class="panel-body">
			
                        Grayson julius - "I am proud to be CIVE graduate"
                        
                        
		      </div>
		  </div>
        <?
        }
        
        
        
        
        function student_projects()
        {
         ?>
         <!--
		  <div class="panel panel-default">
		      <div class="panel-heading">
		      <h1 class="panel-title">Our Students Projects</h1>
		      </div>
		      <div class="panel-body">
			-->
                <?php
                        
    $article=mysql_query("select * from tbl_fypgroup order by PostDate DESC limit 0,2") or die (mysql_error());
 //$article=array_rand( $article);
                while ( $r=mysql_fetch_array($article))
                {
                    
                    $content=$r['description'];
                    $n=100;
                    $content= substr($content,0,$n);
                    $title=$r['prjTitle'];
                    $id=$r['id'];
                    $blog="index.php?q=student_projects&d=".$id;
                    $link="<a class='btn btn-blog pull-right moretag' href='$blog'>See more</a></br><hr>";
                    $content= $content .".....".$link;
                    //echo "<h6>$title</h6>";
                    //echo $content;
                  }      
                ?>
                        
                        
                    <!--    
                        
		      </div>
		  </div>
                  -->
                  
                  
                  
                            <div>
                                    
                                    <a href="index.php?q=student_projects" class="custom-button cb-gray" title="">
                                        <i class="custom-button-icon fa  fa-graduation-cap"></i>
                                        
                                        <span class="custom-button-wrap">
                                            <span class="custom-button-title">Student Projects</span>
                                            <span class="custom-button-tagline">
                                                Final Year Students Projects
                                            
                                            </span>
                                        </span>
                                        <em></em>
                                    </a>
                                </div>
                  
                  
                  
        <?
        }
        
	

//FEATURED NEWS

function news_old ()
{
    $n=150;
    $link=mysql_query("select * from tbl_article where Status=1 order by PostDate DESC limit 0, 4");

 ?>
        <div class="">
        <div class="headline"><h2><?php echo portal_label ("FeaturedNews"); ?></h2></div>
        <div class="row margin-bottom-20">
 
 <?php
    while ($r=mysql_fetch_array($link))
    {
        
        $id=$r['Id'];
        $link2="index.php?q=blog_item&action=".$id;
        

     $message=$r['Content'];

     $message= substr($r['Content'],0,$n);
     $read="<a href='$link2' align='right moretag'>Read more</a>";
     $message=$message;
     $title=$r['Title'];
     $PostDate=$r['PostDate'];


     $image=$r['Image'];
     if($image=="" || $image == "article.jpg") 
     {
        $image="";
	$picha="";
     }else {
     $image=WEB_ROOT."images/articlePictures/".$image;
     $picha="<div class='overflow-hidden'><img class='img-responsive' src='$image' alt='' height='70px' width='90px'></div>";
     }
       ?>
    	    <div class="col-md-3 col-sm-6">
                <div class="thumbnails thumbnail-style thumbnail-kenburn">
                   <div class="thumbnail-img">
                        <?php echo $picha;?>
                        <a class="btn-more hover-effect" href="<?php echo $link2; ?>"><?php echo portal_label("readmore"); ?></a>					
                    </div>
                    <div class="caption">
                        <h3><a class="hover-effect" href="<?php echo $link2; ?>"><?php echo $title; ?></a></h3>
                        <p>
                            <?php echo $message;?>
                        </p>
                    </div>
                </div>
            </div>

      <?php
    }

    echo "</div></div>";
}


?>


<?php





///BLOG  PAGE

function current_article($did)
        {
            if($did=='p')
            {
                
            $article=mysql_query("select * from tbl_college order by PostDate DESC") or die (mysql_error());
            $r=mysql_fetch_array($article);
            $content= $r['Principal_Message'];
            $title="Principal Message";
            $date1=$r['PostDate'];
            $date = date("d-M-Y",$date1);
	    
	    
             $image=staff_name(1,3);   
                
                
            }else{
            $article=mysql_query("select * from tbl_article where Id='$did'") or die (mysql_error());
            $r=mysql_fetch_array($article);
            $content=$r['Content'];
            //$content=htmlspecialchars ($content);
            $title=$r['Title'];
            $date1=$r['PostDate'];
            $date = ""; //date("d-M-Y",$date1);
            $image=$r['Image'];
	    
	    $File1=$r['File1'];
	    $File2=$r['File2'];
	    $File3=$r['File3'];
	    //Attachments
		    if($File1)
		    {
			$url="images/articleFiles/".$File1;
			$link1="<a href='$url' target='_blank'>| Download Attachment 1</a>";
		    }else{
			$link1="";
		    }
		    
		    if($File2)
		    {
			$url=WEB_ROOT."/images/articleFiles/".$File2;
			$link2="<a href='$url' target='_blank'>| Download Attachment 2</a>";
		    }else{
			$link2="";
		    }
		    
		    if($File3)
		    {
			$url=WEB_ROOT."/images/articleFiles/".$File3;
			$link3="<a href='$url' target='_blank'>| Download Attachment 3</a>";
		    }else{
			$link3="";
		    }
		    
	    
	    
            
                if($image=="article.jpg")
                {
                    $image="";
                }else{
                    $image= WEB_ROOT."/images/articlePictures/".$image;
                }
            
            }
         ?>   
    
					    
					    
					    
	
					    
					    
					    
        <?
        }
        
       
            function other_article()
        {
            if(isset($_GET['id']))
            {
                $did=$_GET['id'];
                $article=mysql_query("select * from tbl_article where  Id !='$did' order by PostDate DESC") or die (mysql_error());
                
            }elseif(isset($_GET['cat'])){
                 $cat=$_GET['cat'];
                 $article=mysql_query("select * from tbl_article where Category='$cat' order by PostDate DESC") or die (mysql_error());
            
	    }else{
		 $article=mysql_query("select * from tbl_article order by PostDate DESC") or die (mysql_error());
            
	    }
           echo "<table id='dataTables-example' width='100%'>";
           echo"<thead><tr><td></td></tr></thead><tbody>";
		$w=1;
                while ( $r=mysql_fetch_array($article))
                {
                    $content=$r['Content'];
                    $n=500;
                    $content= substr($content,0,$n);
                    $title=$r['Title'];
                    $id=$r['Id'];
                    $blog="index.php?q=blog_item&action=".$id;
		    $readmore=portal_label("readmore");
                    $link="<a class='btn btn-blog pull-right moretag' href='$blog'>$readmore</a>";
                    $content= $content. "....". $link;
                    $PostDate=$r['PostDate'];
                    $Category=$r['Category'];
		    
                    $image=$r['Image'];
                    
		    
		    
		    if(($image=="article.jpg") || ($image=="")) 
                {
                    $image="";
                }else{
                    $image=WEB_ROOT."images/articlePictures/".$image;
                }
			echo "<tr><td>";
                if(!$image)
                {
                    
                }else{
                ?>
                <!--
                <figure class="leadership-photo">	
                   
                        <img src="<?php //echo $image;?>" alt="" class="img-responsive" />
                 
                </figure>
                                    -->
                 <?php
                }
                ?>
		

                
                
                    <?php
                    if(!$image)
                    {
                    ?>
                 <div class="row">
                    <div class="col-md-12">
                        <h6><?php echo $title;?></h6>
                        <ul class="list-inline">
                            <li><i class="fa fa-calendar color-blue"></i>  <?php echo portal_label("Posted");?> : <?php echo $PostDate;?></li>
                        </ul>
                        <p>
                            <?php echo $content; ?>
                        </p>
			<hr/>
                    </div>
		</div>
                      
                        <?php
                        
                    }else{
                     ?>

			

			
<div class="row">

<h6><?php echo $title;?></h6>			 
<ul class="list-inline">
<li><i class="fa fa-calendar color-blue"></i> <?php echo portal_label("Posted");?> :   <?php echo $PostDate;?></li>
</ul>
<hr/>
<div class="col-md-3">			
<img src="<?php echo $image;?>" class="img-responsive" alt="" />	
</div>
<div class="col-md-1">
</div>
<div class="col-md-8">
                        <p>
                            <?php echo $content; ?>
                        </p>
		


</div>

</div>
			
			
                    
	

                     	
                     <?php   
                    }
                    ?>

             
                

                <?php 
                 $w++;
                 echo"</td></tr>";
                }
                ?>

		
              </tbody></table>
            <?php
        }
       
       
       
       
       
     function recent_article()
        {
            if(isset($_GET['id']) || isset($_GET['action']))
            {
                $did=@$_GET['id'];
		$act=@$_GET['action'];
                $article=mysql_query("select * from tbl_article where Id !='$act' order by PostDate DESC limit 0, 3") or die (mysql_error());
                
            }else{
                
                 $article=mysql_query("select * from tbl_article order by PostDate DESC  limit 0, 3") or die (mysql_error());
            }
  ?>
                  <div>
                    <div><h2><?php echo portal_label("RecentPost");?></h2></div>
  <?php
		$w=1;
                while ( $r=mysql_fetch_array($article))
                {
                    $content=$r['Content'];
                    $n=200;
                    $content= substr($content,0,$n);
                    $title=$r['Title'];
                    $id=$r['Id'];
                    $blog="index.php?q=blog_item&action=".$id;
		    $readmore=portal_label("readmore");
                    $link="<a class='btn btn-blog pull-right moretag' href='$blog'>$readmore</a>";
                    $content= "<b>".$title."</b></br>".$content. "....". $link;
                    $PostDate=$r['PostDate'];
                    $Category=$r['Category'];
		    
                    $image=$r['Image'];
                    
		    
		    
		    if(($image=="article.jpg") || ($image=="")) 
                {
                    $image="";
                }else{
                    $image=WEB_ROOT."images/articlePictures/".$image;
                }
	?>
		
	
			<?php
			if(!$image)
			{
				echo "<dl>";
			}else
			{
                        ?>
			<dl>
                        <dt><a href="#"><img src="<?php echo $image;?>" class="img-responsive hover-effect" alt="" /></a></dt>
			<?php
			}
			?>
			
                        <dd>
                            <p><?php echo $content; ?></p> 
                        </dd>
                    </dl>
		
            <?php
        }
	
	 $blog_all="index.php?q=blog";
?>
  <div>
  <a href="<?php echo $blog_all; ?>" class="btn-u btn-u-primary btn-u-sm btn-block"><?php echo portal_label("SeeAllPosts"); ?></a>
  </div>
		  </div>
		
		 
<?php
	}
        

        
	
	
	
	
        
        function verification_counts ()
        {
           $count=mysql_query("select  * from tbl_verifications where Status='1'");
           $n=mysql_num_rows($count);
           
           $count_mobile=mysql_query("select  * from tbl_verifications where Status='1' and Type='Mobile'");
           $n_mobile=mysql_num_rows($count_mobile);  
           
           $count_web=mysql_query("select  * from tbl_verifications where Status='1' and Type='Web'");
           $n_mobile=mysql_num_rows($count_web);
           
           
           $count_sms=mysql_query("select  * from tbl_verifications where Status='1' and Type='SMS'");
           $n_sms=mysql_num_rows($count_sms);
           
           
           $val=$n;
           
           //$val="893839383938";
           
           
								
	echo $val;
								
            
            
        }
	
	
	
    function verification_counts_stat ($p)
        {
           $count=mysql_query("select  * from tbl_verifications where Status='1'");
           $n=mysql_num_rows($count);
           
           $count_mobile=mysql_query("select  * from tbl_verifications where Status='1' and Type='Mobile'");
           $n_mobile=mysql_num_rows($count_mobile);  
           
           $count_web=mysql_query("select  * from tbl_verifications where Status='1' and Type='Web'");
           $n_web=mysql_num_rows($count_web);
           
           
           $count_sms=mysql_query("select  * from tbl_verifications where Status='1' and Type='SMS'");
           $n_sms=mysql_num_rows($count_sms);
           
           
           $val=$n;
           
           $n_mobile1=($n_mobile/$n) * 100;
	   
           $n_web1=($n_web/$n) * 100;
           $n_sms1=($n_sms/$n) * 100;
			$n_mobile1=round($n_mobile1);
			$n_sms1=round($n_sms1);
			$n_web1=round($n_web1);
	switch ($p)
	{
	case 1: $x=$n_mobile1;break;
	case 2 :$x=$n_web1;break;
	case 3 :$x=$n_sms1;break;
	case 11: $x=$n_mobile;break;
	case 22 :$x=$n_web;break;
	case 33 :$x=$n_sms;break;
	default:$x=$n;break;
	}
								
            return $x;
            
        }
	
        
        
        
        ///Verification function
        
        
        function web_verification ($scratch)
        {
            $Type="Web";
            $vStatus= 1;
        //Check code status
            $web1=mysql_query("select * from tbl_scratch_code s where s.scratch_number='$scratch'");
            $nv1=mysql_num_rows($web1);
            if($nv1==1)
            {
        //Check activation status
                $n=mysql_fetch_fetch_array($web1);
                $status_code=$n['Status'];
                if($status_code==1)
                {
                    //medical drugs qualifies for verification
                    
                    
                            //Check revalidation
                            $web2=mysql_query("select * from tbl_verifications v, tbl_scratch_code s where s.id=v.StratchID and s.scratch_number='$stratch' and v.Status='1'") or die (mysql_error());
                            $nv2=mysql_num_rows($web2);
                            if($nv2>=1)
                            {
                                //revalidation action
                                  $veryStatus=2;
                            }else{
                                //Okey for validation
                            $web4=mysql_query("insert into tbl_verifications (Type,StratchID,Status) values ('$Type','$StratchID', '$vStatus')") or die (mysql_error());
                                //update stratch
                            $web5=mysql_query("update tbl_scratch_code set Status='0' where Scratch_Number='$stratch'") or die (mysql_error());
                            //success
                            $veryStatus=1;

                            }
                }else{
                    //medical drugs does not qualifies for verification
                      $veryStatus=3; 
                } 
            }else{
        //Code does not exist
        $veryStatus=4; 
                
            }
            
		return $veryStatus;
        }
        
        
     function verification_feedback($stracth)
     {
        
       $k=web_verification ($stracth);
       
       
       //Decide feedback
        $test= mysql_query("select * from tbl_message_template where Type='$k'");
        $r=mysql_fetch_array($test);
        $message=$r['Content_sw'];
        
        return $message;
       
     }
     
     
     
     function verification_form()
     {
	global $self;
        ?>
        
        
        <!-- Login-Form -->
                    
                        <form action="<?php echo $self;?>"  class="sky-form" method="POST">
                            <header><?php echo portal_label("Verification");?></header>
                            
                            <fieldset>                  
                                
                                <section>
                                    <div class="row verify">
                                        <label></label>
                                        <div class="col col-8">
                                            <label class="input">
                                                <i class="icon-append fa fa-barcode"></i>
                                                <input type="text" name="scratch" placeholder="<?php echo portal_label("ScratchText");?>">
                                            </label>

                                        </div>
                                    </div>
                                </section>
				<section>
                            <label class="label"></label>
                            <label class="input input-captcha">
                                <img src="<?php echo WEB_ROOT;?>templates/dvs/assets/plugins/sky-forms-pro/skyforms/captcha/image.php?<?php echo time(); ?>" width="80" height="32" alt="Captcha image" />
                                <input type="text"  name="captcha" id="captcha2" placeholder="<?php echo portal_label("CaptchaText");?>">
                            </label>
                        </section>
                            </fieldset>
                            <footer>
                                <button type="submit" class="btn-u pulse-grow" name="verify"><?php echo portal_label("Verify");?></button>
                            </footer>
                        </form>         
                        
                   
                    <!-- End Login-Form -->
        
        
        
        
        <?php
	

	
	
        
     }
        
        

 
 function doLogin()
{
	// if we found an error save the error message in this variable
	$errorMessage = '';
	
	$userName = addslashes(trim($_POST['txtUserName']));
	$password = addslashes(trim($_POST['txtPassword']));
	//$language = $_POST['language'];
	
	//home url
	//$url = 'https://' . CONFIG_KANNEL_HOST . '/ams/index.php';
	$url = WEB_ROOT.'/admindvs/ndex.php';
	
	// first, make sure the username & password are not empty
	if ($userName == '' or $password == '') {
		$errorMessage = 'You must enter your username and password';
	} else {
		// check the database and see if the username and password combo do match
		$sql = "SELECT user_id, fname
		        FROM tbl_user 
				WHERE username = '$userName' AND password = MD5('$password')";
		$result = dbQuery($sql);
	
		if (dbNumRows($result) == 1) {		
			//check from db if ip is blocked 
			$_SESSION['user_ip'] = $_SERVER['REMOTE_ADDR'];
			$_SESSION ['user_login_iteration'] = 1;
			$resultcheckip = dbQuery("SELECT * FROM tbl_blocked_ip WHERE ip = '{$_SESSION['user_ip']}' AND status = 1");
			if(dbNumRows($resultcheckip) > 0){
			$errorMessage = 'HTTP/1.1 403 Forbidden, Your Account has been banned. Contact Your System Administrator';
			return $errorMessage;
			}
			$row = dbFetchAssoc($result);
			
			//check if user have modules
			$activeUser = dbQuery("SELECT Position FROM tbl_privilege WHERE user_id ='{$row['user_id']}' ");
			if(dbNumRows($activeUser) > 0){
			$_SESSION['sis_user_id'] = $row['user_id'];
			$_SESSION['sis_username']    = $row[ 'fname'];
			//$_SESSION['sms_language']    = $language;
			$_SESSION['last_use']    = time();
			$_SESSION['login_time']  = time();
			
			define('USER_ID',$_SESSION['sis_user_id']);
			
			// log the time when the user last login
			$sql = "UPDATE tbl_user 
			        SET user_last_login = NOW()
					WHERE user_id = '{$row['user_id']}'";
			dbQuery($sql);
			
			// automatic backup after two weeks
			automaticbackup();
			
			$pageNumber = 'login';
			
			//upadate the loghistory
			traceUsers($pageNumber);
			
			// now that the user is verified we move on to the next page
            // if the user had been in the admin pages before we move to
			// the last page visited
			if(isset($_SESSION ['login_return_url'])){
			    //check if user have previlege to view the page
				if($_SESSION['last_user_id'] == $_SESSION['ams_user_id'] ){
				header('Location: ' . $_SESSION ['login_return_url']);
				exit;
				} else {
				
				header('Location:' . $url);
				exit;
				}
			}
			@header('Location:' . $url);
			//header('Location: index.php');
			exit;
			} else $errorMessage = 'Your Account is locked contact your Administrator';
			
		} else {
		    // for wrong username or password
			$ip      = $_SERVER['REMOTE_ADDR'];
			if(!isset($_SESSION ['user_ip'])){
				$_SESSION ['user_ip'] = $ip;
				$_SESSION ['user_login_iteration'] = 1; //check number of iterations for user login
				$errorMessage = 'Wrong username or password';
			} else {
				if($_SESSION ['user_login_iteration'] > 10){
					$errorMessage = 'Brute force attack. Please you are not required to access this system again';
					$resultifipexist = dbQuery("SELECT * FROM tbl_blocked_ip WHERE ip = '{$_SESSION['user_ip']}'");
					if(dbNumRows($resultifipexist) > 0){
						dbQuery("UPDATE tbl_blocked_ip SET status = '1', date = NOW() WHERE ip = '{$_SESSION['user_ip']}'");
					} else {
						dbQuery("INSERT INTO tbl_blocked_ip (ip,status) VALUES('$ip','1')");
					}
					
					$_SESSION ['user_login_iteration'] += 1;
				} else {
					$errorMessage = 'Wrong username or password';
					$_SESSION ['user_login_iteration'] += 1;
				}
			}
			
		
			
		}		
			
	}
	
	return $errorMessage;
}
 
 
 function mafanikio($w)
{
?>
	
<div class="alert alert-success">  
<a class="close" data-dismiss="alert">?</a>  
<strong>
<?php
echo $w;

?>	
	
</strong>  
</div> 
	
<?php
	
}


function noma($w)
{
?>
	
<div class="alert alert-warning">  
<a class="close" data-dismiss="alert">?</a>  
<strong>
<?php
echo $w;

?>	
	
</strong>  
</div> 
	
<?php
	
}
 
 

function school_biography ($id)
{
 
      ?>
      
      
                  <div class="leadership-wrapper"><!-- leadership single wrap -->
                            	
                <?php
		 $image=staff_name(school_name($id,3),3);
                if(!$image)
                {
                    
                }else{
                ?>
                <figure class="leadership-photo">	
                   
                        <img src="<?php echo $image;?>" alt="" class="img-responsive pull-left" align="left" />
                 
                </figure>
                 <?php
                }
                ?>
                                    
                                    <div class="leadership-meta clearfix">
                                    	<h1 class="leadership-function title-median"><?php echo school_name($id,1);?><small><?php staff_name(school_name($id,3),1);?></small></h1>
                                        <div class="leadership-position">Dean : <?php echo staff_name(school_name($id,3),1);?></div>
                                        <p class="leadership-bio">
<?php echo school_name($id,2);?>
                                        </p>
                                    </div>
                                
                                </div>
      
      
      
      
      <?php
      
      
      
      
      
      
}



function school_contacts ($n)
{
    $link=mysql_query("select * from tbl_school where Id='$n'");

    while ($r=mysql_fetch_array($link))
    {
        
$Email=$r['Email'];
$Phone=$r['MobileNo'];   

      
      echo"<address>
        <abbr title='Email'>eMail: </abbr> $Email | <abbr title='Phone'>Mobile: </abbr> $Phone 
      </address>";

    }    
}



function department_biography ($id)
{
?>
      
            
                  <div class="leadership-wrapper"><!-- leadership single wrap -->
                            	
                <?php
		 $image=staff_name(department_name($id,3),3);
                if(!$image)
                {
                    
                }else{
                ?>
                <figure class="leadership-photo">	
                   
                        <img src="<?php echo $image;?>" alt="" class="img-responsive pull-left" align="left" />
                 
                </figure>
                 <?php
                }
                ?>
                                    
                                    <div class="leadership-meta clearfix">
                                    	<h1 class="leadership-function title-median"><?php echo department_name($id,1);?><small><?php staff_name(department_name($id,3),1);?></small></h1>
                                        <div class="leadership-position">HOD : <?php echo staff_name(department_name($id,3),1);?></div>
                                        
                                        <p class="leadership-bio">
<?php echo department_name($id,2);?>
                                        </p>
                                    </div>
                                
                                </div>
      
      
      
      <?php
      
      
}


function staff_list($sem1)
{
    $sem=mysql_query("select * from tbl_staff where DepartmentID='$sem1' order by LastName");

echo "<table class='table table-condensed table-striped table-bordered' width='100%'>
    <thead>
        <tr>
            <th>#</th><th>Name</th><th></th>
        </tr>        
    </thead><tbody>";
    $k=1;
        while ($y=mysql_fetch_array($sem))
        {
        $Name=$y['firstName'] . $y['lastName'];
        $profile="<a href='index.php?q=staff_profile&s=".$y['staffId']."'>View Profile </a>";
        echo"<tr>
            <td>
                $k 
            </td>
            <td>
                $Name
            </td>
            <td>
                $profile
            </td>
            
        </tr>";
        $k++;
        }
    echo"</tbody>";
echo"</table>";
           

	}
	
	
	
	function staff_biography ($id)
{

      
      ?>
            <div class="leadership-wrapper"><!-- leadership single wrap -->
                            	
                <?php
		 $image=staff_name($id,3);
                if(!$image)
                {
                    
                }else{
                ?>
                <figure class="leadership-photo">	
                   
                        <img src="<?php echo $image;?>" alt="" class="img-responsive pull-left" align="left" />
                 
                </figure>
                 <?php
                }
                ?>
                                    
                                    <div class="leadership-meta clearfix">
                                    	<h1 class="leadership-function title-median"><?php echo staff_name($id,1);?><small><?php staff_name($id,4);?></small></h1>
                                        <div class="leadership-position"><?php echo staff_name($id,4);?></div>
                                        <p class="leadership-bio">
<?php echo staff_name($id,2);?>
                                        </p>
                                    </div>
                                
                                </div>
      <?php
      
}



function staff_contacts ($n)
{
    $link=mysql_query("select * from tbl_staff where StaffId='$n'");

    while ($r=mysql_fetch_array($link))
    {
        
$Email=$r['Email'];
$Phone=$r['MobileNo1'];   

      
      echo"<address>
        <abbr title='Email'>eMail: </abbr> $Email | <abbr title='Phone'>Mobile: </abbr> $Phone 
      </address>";

    }    
}
	



function department_list_pub($sem1)
{
    //$sem=mysql_query("select * from tbl_program where DepartmentID='$sem1' order by StudyLevel");

	$data=mysql_query("select d.Id as dId, s.Name as sName, d.Name as dName , d.Description as dDescription from tbl_school s,
                          tbl_department d where s.ID=d.SchoolID and
                          s.Id ='$sem1' order by dName ASC") or die (mysql_error());
	
echo "<table class='table table-condensed table-striped table-bordered'>
    <thead>
        <tr>
            <th>#</th><th>Name</th><th></th>
        </tr>        
    </thead><tbody>";
    $k=1;
        while ($y=mysql_fetch_array($data))
        {
        $Name=$y['dName'];
        $profile="<a href='index.php?q=department_profile&d=".$y['dId']."'>View Profile </a>";
        echo"<tr>
            <td>
                $k 
            </td>
            <td>
                $Name
            </td>
            <td>
                $profile
            </td>
            
        </tr>";
        $k++;
        }
    echo"</tbody>";
echo"</table>";
           

	}
	
	
	
	
	
	function programe_list($sem1)
{
    $sem=mysql_query("select * from tbl_program where DepartmentID='$sem1' order by StudyLevel") or die (mysql_error());

echo "<table class='table table-condensed table-striped table-bordered' width='100%'>
    <thead>
        <tr>
            <th>#</th><th>Name</th><th></th>
        </tr>        
    </thead><tbody>";
    $k=1;
        while ($y=mysql_fetch_array($sem))
        {
        $Name=$y['Name'];
        $profile="<a href='index.php?q=programme_profile&p=".$y['Id']."'>Course List </a>";
        echo"<tr>
            <td>
                $k 
            </td>
            <td>
                $Name
            </td>
            <td>
                $profile
            </td>
            
        </tr>";
        $k++;
        }
    echo"</tbody>";
echo"</table>";
           

	}
	
	
	
	
	function program_biography ($n)
{
    $link=mysql_query("select * from tbl_program where Id='$n'");

  $r=mysql_fetch_array($link);

      $name=$r['Name']; //." ".$r['MiddleName'];
      echo "<h1 class='page-title'>$name</h1>";

     $message= $r['Description'];
     //$read="<a href='principal_message'>Read more >>";
     //$read= "</p><p><a class='btn' href='#'>$read �</a></p>";
     $message="<p>".$message ."</p>";
    
      echo $message;   
}




function programe_course($P,$y)
{
	
	
	$data=mysql_query("select * from tbl_curiculum  where ProgramID='$P' and  YearOfStudy ='$y'") or die (mysql_error());
	while ($r=mysql_fetch_array($data))
	{
		
		$sem1=$r['Semister'];
		$mwaka=$r['YearOfStudy'];
	   
           
$sem=mysql_query("select * from tbl_course c, tbl_curiculum s where s.CourseID=c.Id and s.Semister='$sem1' and s.ProgramID='$P' and  s.YearOfStudy ='$y'") or die (mysql_error());

echo "Year :". $mwaka . " Semister : ".$sem1;

echo "<table class='table table-condensed table-striped table-bordered' width='100%'>
    <thead>
        <tr>
            <th>Code</th><th>Name</th><th>Units</th>
        </tr>        
    </thead><tbody>";
        while ($y=mysql_fetch_array($sem))
        {
        $Code=$y['Name'];
        $Name=$y['Description'];
        $Unit=$y['Unit'];
        echo"<tr>
            <td>
                $Code 
            </td>
            <td>
                $Name
            </td>
            <td>
                $Unit
            </td>
            
        </tr>";
        }
    echo"</tbody>";
echo"</table>";
           

	}
	
}






function publication_list($StaffID)
{
	
	
	?>
	    <!--
<table class="table table-striped table-bordered bootstrap-datatable datatable" id='dataTables-example'>
<thead>
<tr>
								  

 
<th>S/N</th>
<th>Title</th>
<th>Publisher</th>
<th>Authors</th>
<th>Year</th>
<th>Volume</th>
<th>Issue</th>
<th>Page</th>

</tr>
</thead>   
<tbody>
	-->
	
	<?php
	
	
	
	global $self;
	
	
	$data=mysql_query("select * from tbl_publication p, tbl_staff s where s.StaffId=p.StaffID and p.StaffID='$StaffID' order by p.Year ASC") or die (mysql_error());
	while ($r=mysql_fetch_array($data))
	{
		
		$Title=$r['Title'];
		$Publisher=$r['Publisher'];
		$Year=$r['Year'];
		$Id=$r['Id'];
		$Author=$r['CAuthor'];
		$Page=$r['Page'];
		$Volume=$r['Volume'];
		$Issue=$r['Issue'];
		$Author=$r['CAuthor'];
		$Name=$r['lastName'].",".$r['firstName'];
		
		$Author = "<b>".$Name."</b> ,".$Author;
	
	   
	   //echo"<tr>
	    //<td>$k</td>
	    //<td>$Title</td>
	    //<td>$Publisher</td>
	    //<td>$Author</td>
	     //<td>$Year</td>
	      //<td>$Volume</td>
	       //<td>$Issue</td>
	        //<td>$Page</td>
	   //</tr>";
	   
	   ?>
	   
	   
	
	   <ol class="widget-list list-dotted">
              <li class="media"> <span class="pull-left"><span class="fontello-icon-graduation-cap"></span></span>
                <div class="media-body"> <span class="date"><?php echo $Title ;?></span> <span class="quick-menu-icon pull-right">
			<a class="state fontello-icon-trash-1" href="<?php echo $self ."&del=".$Id;?>"></a> </span>
			<p class="note"><?php echo $Author;?></p><?php echo $Publisher;?>(<?php echo $Volume;?>), <?php echo $Page;?>
                </div>
              </li>
           </ol>
	  
	<?php	
	}
	?>
	<!--
</tbody></table>
-->
	<?php
	
}




function academic_profile($StaffID)
{
	
	global $self;
	
	
	$data=mysql_query("select * from tbl_academic_profile where StaffID ='$StaffID' order by GraduationDate ASC") or die (mysql_error());
	while ($r=mysql_fetch_array($data))
	{
		
		$Level=$r['Level'];
		$Name=$r['Name'];
		$Institution=$r['Institution'];
		$GraduationDate=$r['GraduationDate'];
		$Id=$r['Id'];
	   ?>
	   <ol class="widget-list list-dotted">
              <li class="media"> <span class="pull-left"><span class="fontello-icon-graduation-cap"></span></span>
                <div class="media-body"> <span class="date"><?php echo $Level;?></span> <span class="quick-menu-icon pull-right">
			<a class="state fontello-icon-trash-1" href="<?php echo $self ."&del=".$Id;?>"></a> </span>
			<p class="note"><?php echo $Name;?></p> <?php echo $Institution;?> (<?php echo $GraduationDate;?>)
                </div>
              </li>
           </ol>
	<?php	
	}
	
}



function cordinator_biography ($n)
{
    $link=mysql_query("select * from tbl_cordinator where Id='$n'") or die (mysql_error());

  $r=mysql_fetch_array($link);

      $name=$r['Description']; //." ".$r['MiddleName'];
      echo "<h1 class='page-title'>$name</h1>";

     $message= $r['Biography'];
     //$read="<a href='principal_message'>Read more >>";
     //$read= "</p><p><a class='btn' href='#'>$read �</a></p>";
     $message="<p>".$message ."</p>";
    
      echo $message;
      
      if($n==1)
      {
      echo "<a href='docs/no_degree.pdf' target='_blank' class='btn btn-success'><i class='icon-cloud-download'></i> &nbsp; Download Forms</a></li>";
      
      }elseif($n==2){
        echo "<a href='docs/bachelor_degree.pdf' target='_blank' class='btn btn-success'><i class='icon-cloud-download'></i> &nbsp; Download Instructions</a></li>";
      }elseif($n==3){
        echo "<a href='docs/master_degree.pdf' target='_blank' class='btn btn-success'><i class='icon-cloud-download'></i> &nbsp; Download Forms</a></li>";
      }
}

 

function cord_programe_list($sem1)
{

    
    $sem=mysql_query("select * from tbl_program where StudyLevel='$sem1'");

echo "<table class='table table-condensed table-striped table-bordered' width='100%'>
    <thead>
        <tr>
            <th>#</th><th>Name</th><th></th>
        </tr>        
    </thead><tbody>";
    $k=1;
        while ($y=mysql_fetch_array($sem))
        {
        $Name=$y['Name'];
        $profile="<a href='index.php?q=programme_profile&p=".$y['Id']."'>View Profile </a>";
        echo"<tr>
            <td>
                $k 
            </td>
            <td>
                $Name
            </td>
            <td>
                $profile
            </td>
            
        </tr>";
        $k++;
        }
    echo"</tbody>";
echo"</table>";
           

	}
        
	
	
	
	
	function entry_requirement ($n)
{
    $link=mysql_query("select * from tbl_cordinator where Id='$n'");

    while ($r=mysql_fetch_array($link))
    {
        

     $message="<p>".$r['EntryRequirement']."</p>";
     

      echo $message; 
    }
    
    
    
    
 
} 






             function other_project()
        {
            if(isset($_GET['d']))
            {
                $did=$_GET['d'];
                $article=mysql_query("select * from tbl_fypgroup where id !='$did' order by PostDate DESC") or die (mysql_error());
                
            }else{
                
                 $article=mysql_query("select * from tbl_fypgroup order by PostDate DESC") or die (mysql_error());
            }
                      echo "<table id='dataTables-example'>";
           echo"<thead><tr><td></td></tr></thead><tbody>";
     
        ?>
                
                        
        	
	
        
        
        
        <?php
		$w=1;
                while ( $r=mysql_fetch_array($article))
                {
                    $content=$r['description'];
                    $n=100;
                    $content= substr($content,0,$n);
                    $title=$r['prjTitle'];
                    $id=$r['id'];
                    $blog="index.php?q=student_projects&d=".$id;
                    $link="<a class='btn btn-blog pull-right moretag' href='$blog'>Read more</a>";
                    $content= $content. "....". $link;
                    $PostDate=$r['PostDate'];
                    //$Category=$r['Category'];
                    //$image=$r['Image'];
                    //$image="article/".$image
                    echo "<tr><td>";
                ?>
           
                    
            
                
   
                  <div class="news col-padded col-shaded">
		     <div class="clearfix">
		      <h1 class="leadership-function title-median"><?php echo $title;?></h1>
                       <div class="news-meta">
                      <span class="news-meta-category">Posted <?php echo $PostDate;?></span>
                      <hr/>
		      </div>
                     </div> 
                    
                        <p>	
                    <?php
                        echo $content;
                        //echo $link;
                     ?>   
                    </p>
                        
                    </div>
              
               
                <?php 
                 $w++;
                    echo"</td></tr>";
                }
                ?>

		</tbody></table>
              
            <?php
        }







function current_project($did)
        {

            $article=mysql_query("select * from tbl_fypgroup where id='$did'") or die (mysql_error());
            $r=mysql_fetch_array($article);
            $content=$r['description'];
            $title=$r['prjTitle'];
            $date1=$r['PostDate'];
            $date = date($date1);

            $members=$r['members'];
            
         ?>   
		<div class="news">
                
     
                
		     <div class="clearfix">
		      <h1 class="title-median"><?php echo $title;?></h1>
                       <div class="news-meta">
                         <span class="news-meta-category">Posted : <?php echo $date1;?></span>
                        <hr/>
		      </div>
                     </div> 

			<p>
                   <?php echo $content;?>
                        
                        
		      </p>
                </div>
                
                			<div class="tabbable" id="tabs-7833413">
				<ul class="nav nav-tabs">
					<li>
						<a href="#panel-268554" data-toggle="tab">Members</a>
					</li>

				</ul>
				<div class="tab-content">
					<div class="tab-pane" id="panel-268554">
<?php echo "".$members; ?>
					</div>

				</div>
			</div>
                
                
        <?
        }
        

	
	
	function theme_list()
{
    $sem=mysql_query("select * from tbl_research_theme");

echo "<table class='table table-condensed table-striped table-bordered' width='100%'>
    <thead>
        <tr>
            <th>#</th><th>Name</th><th></th>
        </tr>        
    </thead><tbody>";
    $k=1;
        while ($y=mysql_fetch_array($sem))
        {
        $Name=$y['Name'];
        $profile="<a href='index.php?q=theme_profile&p=".$y['Id']."'>View Profile </a>";
        echo"<tr>
            <td>
                $k 
            </td>
            <td>
                $Name
            </td>
            <td>
                $profile
            </td>
            
        </tr>";
        $k++;
        }
    echo"</tbody>";
echo"</table>";
           

	}
	
	
	
	
	
	function theme_biography ($n)
{
    $link=mysql_query("select * from tbl_research_theme where Id='$n'");

  $r=mysql_fetch_array($link);

      $name=$r['Name']; //." ".$r['MiddleName'];
      echo "<h2 class='page-title'>$name</h2>";

     $message= $r['Biography'];
     //$read="<a href='principal_message'>Read more >>";
     //$read= "</p><p><a class='btn' href='#'>$read �</a></p>";
     $message="<p>".$message ."</p>";
    
      echo $message;   
}





function working_group($n)
{
    $sem=mysql_query("select * from tbl_working_group where ThemeID='$n'");

echo "<table class='table table-condensed table-striped table-bordered' width='100%'>
    <thead>
        <tr>
            <th>#</th><th>Name</th><th>Description</th>
        </tr>        
    </thead><tbody>";
    $k=1;
        while ($y=mysql_fetch_array($sem))
        {
        $Name=$y['Name'];
        $profile= $y['Description'];  //"<a href='theme_profile.php?p=".$y['Id']."'>View Profile </a>";
        echo"<tr>
            <td>
                $k 
            </td>
            <td>
                $Name
            </td>
            <td>
                $profile
            </td>
            
        </tr>";
        $k++;
        }
    echo"</tbody>";
echo"</table>";
           

}







function publication_list_all($r)
{
	
	global $self;
	
	$pub1=mysql_query("select MIN(Year) as Mn from tbl_publication");
        $Mn=mysql_fetch_array($pub1);
        $Mn=$Mn['Mn'];
        
        $pub2=mysql_query("select MAX(Year) as Mx from tbl_publication");
        //$Mx=mysql_fetch_array($pub2);
        //$Mx=$Mx['Mx'];
        $Mx=date('Y');
        $YearDiff=$Mx-$Mn;
        //$r=$YearDiff;
    ?>
           
            			<div class="tabbable tab-pane" id="tabs-783341">
				<ul class="nav nav-tabs">
				<?php
                                $M=$Mn;
                                $k=$M;
                                
				while($M<=$Mx)
				{
				$yd=$M;
                                $c=$yd+$r;
                               $cy=date('Y');
                                if($c>$cy)
                                 {
                                    $c=$cy;
                                    $class1="<li class='active'>";
                                    $class2="tab-pane active";
                                }else
                                {
                                    $class1="<li>";
                                    $class2="tab-pane";
                                }
                           
                           
                                ?>
					<?php echo $class1?> 
						<a href="#panel-<?php echo $yd; ?>" data-toggle="tab">
						  <?php echo $yd ." - ". $c; ?>
						</a>
					</li>
				<?php
                                $M=$yd+$r+1;
				}
				?>	
				</ul>
				<div class="tab-content">
				<?php
				
                                $N=$Mn;
				$w=$Mn;
                                
                                while($N<=$Mx)
				{
                                    $yk=$N;
                                    $x=$yk;
                                    $y=$yk+$r;
                                $cy=date('Y');
                                if($y>$cy)
                                 {
                                    $y=$cy;
                                 }
				?>
				<div class="<?php echo $class2;?>" id="panel-<?php echo $x; ?>">
				<?php pub_list($x,$y);?>
				</div>
				
				<?php
                                 $N=$yk+$r+1;
				}
				?>
				</div>
			</div>
 
           
           
           
           <?php
           
            
            
            
        }
        
        


function pub_list($x,$y)
{
    $cy=date('Y');
    if($y>$cy)
    {
        $y=$cy;
    }
    
$data=mysql_query("select * from tbl_publication p, tbl_staff s where s.StaffId=p.StaffID and p.Year between '$x' and '$y' order by p.Year DESC") or die (mysql_error());

$nums=mysql_num_rows($data);

if($nums>0)
{
    //$v=0;
    echo "<ol>";
        while ($r=mysql_fetch_array($data))
	{
		
		$Title=$r['Title'];
		$Publisher=$r['Publisher'];
		$Year=$r['Year'];
		$Id=$r['Id'];
		$Author=$r['CAuthor'];
		$Page=$r['Page'];
		$Volume=$r['Volume'];
		$Issue=$r['Issue'];
		$Author=$r['CAuthor'];
		$Name=$r['lastName'].",".$r['firstName'];
		
		$Author = "<b>".$Name."</b>,".$Author;
            //$v++;
            
                
	   ?>
              <li>
			<p class="note"><?php echo $Author;?>,(<?php echo $Year;?>) <?php echo $Title ;?>,<i><?php echo $Publisher;?></i>(<?php echo $Volume;?>), <?php echo $Page;?></p>
            
            </li>
	<?php	
	}
        echo "</ul>";
    
}
else{
    
    echo "No publication in this range of years";
}

}



function other_facilities()
        {
            if(isset($_GET['id']))
            {
                $did=$_GET['id'];
                $article=mysql_query("select * from tbl_article where Category='6' and Id !='$did' order by Id DESC") or die (mysql_error());
                
            }else{
                
                 $article=mysql_query("select * from tbl_article where Category='6' order by Id DESC") or die (mysql_error());
            }
           echo "<table id='dataTables-example'>";
           echo"<thead><tr><td></td></tr></thead><tbody>";
		$w=1;
                while ( $r=mysql_fetch_array($article))
                {
                    $content=$r['Content'];
                    $n=500;
                    $content= substr($content,0,$n);
                    $title=$r['Title'];
                    $id=$r['Id'];
                    $blog="index.ph?q=facility&id=".$id;
                    $link="<a class='btn btn-blog pull-right moretag' href='$blog'>Read more</a>";
                    $content= $content. "....". $link;
                    //$PostDate=$r['PostDate'];
                    $Category=$r['Category'];
                    $image=$r['Image'];
                    $image="images/articlePictures/".$image;
echo "<tr><td>";
?>    
                  <div class="leadership-wrapper">
		     <div class="clearfix">
		      <h1 class="title-median"><?php echo $title;?></h1>
                       <div class="news-meta">
                
                      <hr/>
		      </div>
                     </div> 
                                    <p>
                                    <img src="<?php echo $image;?>" alt="" class="alignleft" />
                                <?php echo $content;?>
                                    </p>
                        <div class="separator-dashed"></div>
                    </div>
             
               
                <?php 
                 $w++;
                 echo"</td></tr>";
                }
                ?>

		
              </tbody></table>
            <?php
        }




function current_facility($did)
        {

            $article=mysql_query("select * from tbl_article where Category='6' and Id='$did'") or die (mysql_error());
            $r=mysql_fetch_array($article);
            $content=$r['Content'];
            $title=$r['Title'];
            $date1=$r['PostDate'];
            $date = date("d-M-Y",$date1);
            $image=$r['Image'];
            $image="images/articlePictures/".$image;
            
         ?>   
		<div class="leadership-wrapper">    

		      <h1 class="title-median"><?php echo $title;?></h1>
                                    <p>
                                    <img src="<?php echo $image;?>" alt="" class="aligncenter" />
                                <?php echo $content;?>
                                    </p>
  </div>
                                            
        <?php
        }







function other_alumn()
        {
            if(isset($_GET['id']))
            {
                $did=$_GET['id'];
                $article=mysql_query("select * from tbl_alumn where Id !='$did' order by Id DESC") or die (mysql_error());
                
            }else{
                
                 $article=mysql_query("select * from tbl_alumn order by Id DESC") or die (mysql_error());
            }
           echo "<table id='dataTables-example'>";
           echo"<thead><tr><td></td></tr></thead><tbody>";
		$w=1;
                while ( $r=mysql_fetch_array($article))
                {
                    $content=$r['Biography'];
                    $n=500;
                    $content= substr($content,0,$n);
                    $title=$r['Name'];
                    $id=$r['Id'];
                    $blog="index.php?q=alumn&id=".$id;
                    $link="<a class='btn btn-blog pull-right moretag' href='$blog'>Read more</a>";
                    $content= $content. "....". $link;
                    //$PostDate=$r['PostDate'];
                    //$Category=$r['Category'];
                    $image=$r['Image'];
                    $image="alumn/".$image;
echo "<tr><td>";
?>    
                  <div class="leadership-wrapper">
		     <div class="clearfix">
		      <h1 class="title-median"><?php echo $title;?></h1>
                       <div class="news-meta">
                
                      <hr/>
		      </div>
                     </div> 
                                    <p>
                                    <img src="<?php echo $image;?>" alt="" class="alignleft" />
                                <?php echo $content;?>
                                    </p>
                        
                    </div>
             
               <div class="separator-dashed"></div>
                <?php 
                 $w++;
                 echo"</td></tr>";
                }
                ?>

		
              </tbody></table>
            <?php
        }




function current_alumn($did)
        {

            $article=mysql_query("select * from tbl_alumn where Id='$did'") or die (mysql_error());
            $r=mysql_fetch_array($article);
            $content=$r['Biography'];
            $title=$r['Name'];
            $date1=$r['PostDate'];
            $date = date($date1);
            $image=$r['Image'];
            $image="alumn/".$image;
            $programme=$r['Programme'];
            
         ?>   
		<div class="leadership-wrapper">    

                                        
		      <h1 class="leadership-function title-median"><?php echo $title;?></small></h1>
                                       <div class="leadership-position"><?php echo $programme;?></div>
                                        
                                    <p class="leadership-bio">
                                    <img src="<?php echo $image;?>" alt="" class="alignleft" />
                                <?php echo $content;?>
                                    </p>
  </div>
                                            
        <?php
        }













function other_campuslife()
        {
            if(isset($_GET['id']))
            {
                $did=$_GET['id'];
		 $CampusLife="Campus Life";
                $article=mysql_query("select * from tbl_article c, tbl_article_category a where c.Category= a.Id and a.Name='$CampusLife' and c.Id !='$did' order by c.Id DESC") or die (mysql_error());
                
            }else{
                 $CampusLife="Campus Life";
                 $article=mysql_query("select * from tbl_article c, tbl_article_category a where c.Category= a.Id and c.Id and a.Name='$CampusLife'  order by c.Id DESC") or die (mysql_error());
            }
           echo "<table id='dataTables-example'>";
           echo"<thead><tr><td></td></tr></thead><tbody>";
		$w=1;
                while ( $r=mysql_fetch_array($article))
                {
                    $content=$r['Content'];
                    $n=500;
                    $content= substr($content,0,$n);
                    $title=$r['Title'];
                    $id=$r['Id'];
                    $blog="index.php?q=campuslife&id=".$id;
                    $link="<a class='btn btn-blog pull-right moretag' href='$blog'>Read more</a>";
                    $content= $content. "....". $link;
                    //$PostDate=$r['PostDate'];
                    $Category=$r['Category'];
                    $image=$r['Image'];
                    $image="images/articlePictures/".$image;
echo "<tr><td>";
?>    
                  <div class="leadership-wrapper">
		     <div class="clearfix">
		      <h1 class="title-median"><?php echo $title;?></h1>
                       <div class="news-meta">
                
                      <hr/>
		      </div>
                     </div> 
                                    <p>
                                    <img src="<?php echo $image;?>" alt="" class="alignleft" />
                                <?php echo $content;?>
                                    </p>
                        
                    </div>
             <div class="separator-dashed"></div>
               
                <?php 
                 $w++;
                 echo"</td></tr>";
                }
                ?>

		
              </tbody></table>
            <?php
        }




function current_campuslife($did)
        {

            //$article=mysql_query("select * from tbl_article where Category='6' and  Id='$did'") or die (mysql_error());
            $CampusLife="Campus Life";
	    $article=mysql_query("select * from tbl_article c, tbl_article_category a where c.Category= a.Id and a.Name='$CampusLife' and c.Id ='$did'") or die (mysql_error());
                
	    $r=mysql_fetch_array($article);
            $content=$r['Content'];
            $title=$r['Title'];
            $date1=$r['PostDate'];
            $date = date("d-M-Y",$date1);
            $image=$r['Image'];
            $image="images/articlePictures/".$image;
            
         ?>   
		<div class="leadership-wrapper">    

		      <h1 class="title-median"><?php echo $title;?></h1>
                                    <p>
                                    <img src="<?php echo $image;?>" alt="" class="aligncenter" />
                                <?php echo $content;?>
                                    </p>
  </div>
                                            
        <?php
        }






function about_college ($id)
{
    $college=mysql_query("select * from tbl_college where Id='$id'");

  $r=mysql_fetch_array($college);
  $bio=$r['Biography'];
 echo $bio; 
}




function facilities_default ()
{
    $college=mysql_query("select * from tbl_article where Category='6' limit 0,1");

  $r=mysql_fetch_array($college);
  $bio=$r['Content'];
  $image= "images/articlePictures/".$r['Image'];
  $title=$r['Title'];
    $bio= substr($bio,0,100);
 ?>
 <p>
<img src="<?php echo $image;?>" alt="" class="aligncenter" />
<h1><?php echo $title; ?></h1>
<?php  echo $bio; ?>
</p>
<p>
<a href="index.php?q=facility" class="btn btn-success" title="button">&nbsp; See all</a>
</p>
<?php
}



function campuslife_default ()
{
    $college=mysql_query("select * from tbl_article where Category='7' limit 0,1");

  $r=mysql_fetch_array($college);
  $bio=$r['Content'];
  $image= "images/articlePictures/".$r['Image'];
  $title=$r['Title'];
  $bio= substr($bio,0,100);
 ?>
 <p>
<img src="<?php echo $image;?>" alt="" class="aligncenter" />
<h1><?php echo $title; ?></h1>
<?php  echo $bio; ?>
</p>
<p>
<a href="index.php?q=campuslife" class="btn btn-success" title="button">&nbsp; See all</a>
</p>
<?php
}




function our_students ()
{
    $college=mysql_query("select * from tbl_alumn limit 0,1");

  $r=mysql_fetch_array($college);
  $bio=$r['Biography'];
  $image= "alumn/".$r['Image'];
  $title=$r['Name'];
  $bio= substr($bio,0,100);
 ?>
 <p>
<img src="<?php echo $image;?>" alt="" class="aligncenter" width='250' height='90' />
<h1><?php echo $title; ?></h1>
<?php  echo $bio; ?>
</p>
<p>
<a href="index.php?q=alumn" class="btn btn-success" title="button">&nbsp; See all</a>
</p>
<?php
}


function principal_message ($n)
{
    //$link=mysql_query("select * from tbl_college");
        $link=mysql_query("select * from tbl_article where Category='8' order by PostDate DESC");
	      
    $blog="index.php?q=blog_item&action=p";
    while ($r=mysql_fetch_array($link))
    {
        
    if (!$n)
    {
     $message="<p>".$r['Content']."</p>";
     
    }else
    {
     $message= substr($r['Content'],0,$n);
     //$read="<a href='principal_message'>Read more >>";
     $read= "</p><p align='right'><a class='btn' href='$blog'>Read more</a></p>";
     $message="<p>".$message.$read;
    }
      echo $message; 
    }
}


?>